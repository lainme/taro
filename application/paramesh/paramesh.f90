! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2018 lainme <lainme993@gmail.com>
!
! Description:
!
! Partition structured CGNS mesh for MPI.
!
program main
    use iso_fortran_env, only : OUTPUT_UNIT, REAL64
    use class_string
    use class_system
    use class_argparser
    use class_datafmt_cgns
    use class_logging
    use class_paramesh
    implicit none

    type(argparser_class) :: argparser
    type(paramesh_class) :: paramesh
    type(cgns_reader_class) :: reader
    type(cgns_writer_class) :: writer
    type(string), allocatable, dimension(:) :: zonename
    type(string) :: filename
    real(kind=REAL64), allocatable, dimension(:) :: geometry
    integer, allocatable, dimension(:) :: numcells
    integer :: celldims
    integer :: nblocks
    integer :: nprocs
    integer :: funit
    integer :: i, k

    ! Suppress the stdout logging.
    call logger%construct(stdout_level=SUPPRESS)

    ! Construct the argument parser and get the input parameters.
    call argparser%construct(description="Partition structured CGNS mesh for MPI.", version="0.0.1")
    call argparser%add_argument("--nprocs", meta="NPROCS", required=.true., help="Number of target procs.")
    call argparser%add_argument("--nblocks", meta="NBLOCKS", required=.true., help="Number of target blocks.")
    call argparser%add_argument("--filename", meta="FILENAME", required=.true., help="Filename of the CGNS file.")
    call argparser%add_argument("--celldims", content="-1", meta="CELLDIMS", help="Dimension in simple mesh construct.")
    call argparser%add_argument("--geometry", content="-1", meta="GEOMETRY", help="Geometry in simple mesh construct.")
    call argparser%add_argument("--numcells", content="-1", meta="NUMCELLS", help="Numer of cells in simple mesh construct.")
    call argparser%parse()
    call argparser%get("--nprocs", nprocs)
    call argparser%get("--nblocks", nblocks)
    call argparser%get("--filename", filename)
    call argparser%get("--celldims", celldims)
    allocate(geometry(max(1,celldims)))
    allocate(numcells(max(1,celldims)))
    call argparser%get("--geometry", geometry)
    call argparser%get("--numcells", numcells)

    ! Read mesh.
    if (celldims < 0) then
        call reader%construct(filename%s)
    else
        call reader%construct(geometry, numcells+1)
    end if
    call reader%parse()

    ! Partition.
    call paramesh%construct(reader, nprocs, nblocks)
    reader%zonelist = [(i, i=1, reader%nzones)]

    ! Write mesh.
    call writer%construct(system%path%rootname(filename%s)//"_metis.cgns", reader)

    ! Write partition information.
    open(newunit=funit, file=system%path%rootname(filename%s)//"_metis.info", status="unknown", action="write")
    write(OUTPUT_UNIT, "(A)") "ProcessorID, NumberOfBlocks, BlockNames"
    write(OUTPUT_UNIT, "(A)") "------------------------------------------------------------------------------"
    do i = 1, nprocs
        zonename = pack([(string(reader%zones(k)%zonename), k=1, reader%nzones)], paramesh%part==i)
        write(funit, "(I0,1X,I0,1X,*(DT,1X))") i, size(zonename), zonename
        write(OUTPUT_UNIT, "(I0,1X,I0,1X,*(DT,1X))") i, size(zonename), zonename
    end do
    write(OUTPUT_UNIT, "(A)") "------------------------------------------------------------------------------"
    write(OUTPUT_UNIT, "(A,F4.1)") "Balance of Node: ", paramesh%balance(1)
    write(OUTPUT_UNIT, "(A,F4.1)") "Balance of Edge: ", paramesh%balance(2)
    write(OUTPUT_UNIT, "(A,F4.1)") "Balance of Cell: ", paramesh%balance(3)
    close(funit)
end program main

! vim: set ft=fortran ff=unix tw=132:
