! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2018 lainme <lainme993@gmail.com>
!
! Description:
!
! CSV data format.
!
module class_datafmt_csv
    use class_exception
    use class_string
    use class_iostream
    implicit none
    private
    public :: csv_reader_class
    !-------------------------------------------------------------------------------------------------------------------------------
    ! csv structure.
    !-------------------------------------------------------------------------------------------------------------------------------
    type :: csv_structure
        type(string), allocatable, dimension(:) :: line
    end type csv_structure

    !-------------------------------------------------------------------------------------------------------------------------------
    ! csv reader and writer class.
    !-------------------------------------------------------------------------------------------------------------------------------
    type :: csv_reader_class
        private
        type(csv_structure), allocatable, dimension(:), public :: lines
    contains
        procedure :: construct
    end type csv_reader_class
contains
    subroutine construct(self, filename, delimiter)
        class(csv_reader_class), intent(inout) :: self
        character(len=*), intent(in) :: filename
        character(len=*), intent(in), optional :: delimiter
        type(exception_class) :: exception
        type(string), allocatable, dimension(:) :: lines
        character(len=:), allocatable :: delimiter_chosen
        integer :: funit
        integer :: i

        open(newunit=funit, file=filename, status="unknown", action="read", iostat=exception%stat, iomsg=exception%message)
        call exception%raise()
        lines = iostream%readlines(funit)
        close(funit)

        if (present(delimiter)) then
            delimiter_chosen = delimiter
        else
            delimiter_chosen = " "
        end if

        if (allocated(self%lines)) then
            deallocate(self%lines)
        end if
        allocate(self%lines(size(lines)))
        do i = 1, size(lines)
            self%lines(i)%line = str%split(lines(i)%s, delimiter_chosen)
        end do
    end subroutine construct
end module class_datafmt_csv

! vim: set ft=fortran ff=unix tw=132:
