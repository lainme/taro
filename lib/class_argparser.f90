! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
! Description:
!
! A simple argument parser inspired by Python standard argparse library and the FLAP (https://github.com/szaghi/FLAP) code.
!
module class_argparser
    use iso_fortran_env, only : INT32, INT64, REAL32, REAL64, OUTPUT_UNIT
    use class_exception
    use class_string
    use class_hashmap
    implicit none
    private
    public :: argparser_class

    type, extends(hashmap_pair) :: argument_structure
        character(len=:), allocatable :: keyname ! Name of the argument (key).
        character(len=:), allocatable :: content ! Value of the argument.
        character(len=:), allocatable :: help ! Help message, defaults to none.
        character(len=:), allocatable :: meta ! Used in help message, defaults to none.
        logical :: required = .false. ! Defaults to false.
    contains
        procedure :: has_samekey => argument_has_samekey
        procedure :: copy => argument_copy
        procedure :: hash => argument_hash
    end type argument_structure

    interface argument_structure
        procedure :: argument_create_object
    end interface argument_structure

    type :: argparser_class
        private
        character(len=:), allocatable :: description
        character(len=:), allocatable :: version
        character(len=:), allocatable, public :: commands
        type(hashmap_class) :: arguments
    contains
        procedure :: construct => argparser_construct
        procedure :: add_argument => argparser_add_argument
        procedure :: parse => argparser_parse
        generic :: get => argparser_get_scalar, argparser_get_vector
        procedure, private :: argparser_get_scalar
        procedure, private :: argparser_get_vector
        procedure, private :: output_help => argparser_output_help
        procedure, private :: output_version => argparser_output_version
    end type argparser_class
contains
    !-------------------------------------------------------------------------------------------------------------------------------
    ! Argument structure methods.
    !-------------------------------------------------------------------------------------------------------------------------------
    elemental function argument_create_object(keyname) result(argument)
        character(len=*), intent(in) :: keyname
        type(argument_structure) :: argument

        argument%keyname = str%strip(keyname)
    end function argument_create_object

    elemental function argument_has_samekey(self, pair) result(has_samekey)
        class(argument_structure), intent(in) :: self
        class(hashmap_pair), intent(in) :: pair
        logical :: has_samekey

        has_samekey = .false.

        select type(pair)
        class is(argument_structure)
            has_samekey = pair%keyname .eq. self%keyname
        end select
    end function argument_has_samekey

    subroutine argument_copy(self, object)
        class(argument_structure), intent(in) :: self
        class(*), intent(out) :: object

        select type(object)
        class is(argument_structure)
            object%keyname = self%keyname
            object%required = self%required
            if (allocated(self%content)) then
                object%content = self%content
            end if
            if (allocated(self%help)) then
                object%help = self%help
            end if
            if (allocated(self%meta)) then
                object%meta = self%meta
            end if
        class is(string)
            if (allocated(self%content)) then
                object = self%content
            end if
        end select
    end subroutine argument_copy

    elemental function argument_hash(self) result(hash)
        class(argument_structure), intent(in) :: self
        integer :: hash

        hash = str%hash(self%keyname)
    end function argument_hash

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Argparser class methods.
    !-------------------------------------------------------------------------------------------------------------------------------
    subroutine argparser_construct(self, description, version)
        class(argparser_class), intent(inout) :: self
        character(len=*), intent(in), optional :: description
        character(len=*), intent(in), optional :: version
        integer :: stat

        deallocate(self%description, stat=stat)
        deallocate(self%version, stat=stat)
        deallocate(self%commands, stat=stat)

        if (present(description)) then
            self%description = str%strip(description)
        end if

        if (present(version)) then
            self%version = str%strip(version)
        end if

        call self%arguments%construct()
    end subroutine argparser_construct

    subroutine argparser_add_argument(self, keyname, content, help, meta, required)
        class(argparser_class), intent(inout) :: self
        character(len=*), intent(in) :: keyname
        character(len=*), intent(in), optional :: content
        character(len=*), intent(in), optional :: help
        character(len=*), intent(in), optional :: meta
        logical, intent(in), optional :: required
        type(argument_structure) :: argument

        argument%keyname = str%strip(keyname)

        if (present(content)) then
            argument%content = str%strip(content)
        end if

        if (present(help)) then
            argument%help = str%strip(help)
        end if

        if (present(meta)) then
            argument%meta = str%strip(str%upper(meta))
        else
            argument%meta = "VALUE"
        end if

        if (present(required)) then
            argument%required = required
        end if

        call self%arguments%set(argument)
    end subroutine argparser_add_argument

    subroutine argparser_parse(self, commands)
        class(argparser_class), intent(inout) :: self
        character(len=*), intent(in), optional :: commands ! Defaults to obtain from command line.
        type(string), allocatable, target, dimension(:) :: commands_splitted
        type(string), pointer :: command
        class(hashmap_pair), pointer :: argument
        logical :: success
        integer :: charlen
        integer :: i

        ! Get commands.
        if (present(commands)) then
            self%commands = str%strip(commands)
        else
            call get_command(length=charlen)
            allocate(character(len=charlen) :: self%commands)
            call get_command(self%commands)
        end if

        commands_splitted = str%split(self%commands)

        ! Parse commands.
        argument => null()
        do i = 1, size(commands_splitted)
            command => commands_splitted(i)

            ! Termination on help.
            if (command .in. [string("-h"), string("--help")]) then
                call self%output_help()
                stop
            end if

            ! Termination on version.
            if (command .in. [string("-v"), string("--version")]) then
                call self%output_version()
                stop
            end if

            ! Find argument location.
            success = self%arguments%find(argument_structure(command%s), argument)

            if ((.not. success) .and. associated(argument)) then
                select type(argument)
                class is(argument_structure)
                    argument%content = command%s
                end select
            end if
        end do
    end subroutine argparser_parse

    subroutine argparser_get_scalar(self, keyname, content)
        class(argparser_class), intent(in) :: self
        character(len=*), intent(in) :: keyname ! Name to search.
        class(*), intent(out) :: content ! Found content.
        type(exception_class) :: exception
        type(string) :: charvalue
        logical :: success

        ! Find content.
        success = self%arguments%pick(argument_structure(keyname), charvalue)

        ! Get content.
        if (.not. allocated(charvalue%s)) then
            call exception%construct("Required value not found for argument ["//str%strip(keyname)//"].")
        else
            call exception%construct("Data type error occurred for value of argument ["//str%strip(keyname)//"].")
            select type(content)
            type is(integer(kind=INT32))
                read(charvalue%s, *, iostat=exception%stat) content
            type is(integer(kind=INT64))
                read(charvalue%s, *, iostat=exception%stat) content
            type is(real(kind=REAL32))
                read(charvalue%s, *, iostat=exception%stat) content
            type is(real(kind=REAL64))
                read(charvalue%s, *, iostat=exception%stat) content
            type is(logical)
                read(charvalue%s, *, iostat=exception%stat) content
            type is(complex)
                read(charvalue%s, *, iostat=exception%stat) content
            type is(string)
                exception%stat = 0
                content = charvalue
            end select
        end if
        call exception%raise()
    end subroutine argparser_get_scalar

    subroutine argparser_get_vector(self, keyname, content)
        class(argparser_class), intent(in) :: self
        character(len=*), intent(in) :: keyname ! Name to search.
        class(*), dimension(:), intent(out) :: content ! Found content.
        type(exception_class) :: exception
        type(string) :: charvalue
        logical :: success

        ! Find content.
        success = self%arguments%pick(argument_structure(keyname), charvalue)

        ! Get content.
        if (.not. allocated(charvalue%s)) then
            call exception%construct("Required value not found for argument ["//str%strip(keyname)//"].")
        else
            call exception%construct("Data type error occurred for value of argument ["//str%strip(keyname)//"].")
            select type(content)
            type is(integer(kind=INT32))
                read(charvalue%s, *, iostat=exception%stat) content
            type is(integer(kind=INT64))
                read(charvalue%s, *, iostat=exception%stat) content
            type is(real(kind=REAL32))
                read(charvalue%s, *, iostat=exception%stat) content
            type is(real(kind=REAL64))
                read(charvalue%s, *, iostat=exception%stat) content
            type is(logical)
                read(charvalue%s, *, iostat=exception%stat) content
            type is(complex)
                read(charvalue%s, *, iostat=exception%stat) content
            type is(string)
                exception%stat = 0
                content = charvalue
            end select
        end if
        call exception%raise()
    end subroutine argparser_get_vector

    subroutine argparser_output_help(self)
        class(argparser_class), intent(inout) :: self
        character(len=:), allocatable :: exename
        character(len=:), allocatable :: message
        type(argument_structure) :: argument
        integer :: charlen

        ! Usage
        call get_command_argument(0, length=charlen)
        allocate(character(len=charlen) :: exename)
        call get_command_argument(0, exename)
        message = "Usage: "//exename//" [-h] [-v]"

        do while(self%arguments%iterator%pick(argument))
            if (argument%required) then
                message = message//" "//argument%keyname//" "//argument%meta
            else
                message = message//" ["//argument%keyname//" "//argument%meta//"]"
            end if
        end do
        write(OUTPUT_UNIT, "(A,/)") message

        ! Description
        if (allocated(self%description)) then
            write(OUTPUT_UNIT, "(A,/)") self%description
        end if

        ! Options
        write(OUTPUT_UNIT, "(A,/)") "Options:"
        write(OUTPUT_UNIT, "(4X,A,/,8X,A,1X,A,/)") "-h, --help", "Show this help message.", "(Optional)"
        write(OUTPUT_UNIT, "(4X,A,/,8X,A,1X,A,/)") "-v, --version", "Show version information.", "(Optional)"

        do while(self%arguments%iterator%pick(argument))
            write(OUTPUT_UNIT, "(4X,A,2X,A)") argument%keyname, argument%meta

            if (argument%required) then
                message = "(Required)"
            else
                message = "(Optional)"
            end if
            if (allocated(argument%help)) then
                message = argument%help//" "//message
            end if
            write(OUTPUT_UNIT, "(8X,A,/)") message
        end do
    end subroutine argparser_output_help

    subroutine argparser_output_version(self)
        class(argparser_class), intent(in) :: self

        if (allocated(self%version)) then
            write(OUTPUT_UNIT, "(A)") self%version
        end if
    end subroutine argparser_output_version
end module class_argparser

! vim: set ft=fortran ff=unix tw=132:
