! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2018 lainme <lainme993@gmail.com>
!
! Description:
!
! Mesh partition on external format (CGNS).
!
module class_paramesh
    use iso_fortran_env, only : REAL32, REAL64, INT32, INT64
    use class_datafmt_cgns
    use class_configparser
    use class_exception
    use class_logging
    use class_indexmap
    use class_message_passing
    use class_string
    implicit none
    private
    public :: paramesh_class

#if METIS_IDXTYPEWIDTH == 32
    integer, parameter :: METIS_INT = INT32
#else
    integer, parameter :: METIS_INT = INT64
#endif
#if METIS_REALTYPEWIDTH == 32
    integer, parameter :: METIS_REAL = REAL32
#else
    integer, parameter :: METIS_REAL = REAL64
#endif

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Mesh partition class.
    !-------------------------------------------------------------------------------------------------------------------------------
    type :: paramesh_class ! Structured only.
        private
        logical :: autosplit
        integer :: nprocs
        integer :: nblocks
        integer :: maxretry
        real(kind=REAL64) :: stepsize
        real(kind=REAL64), public :: balance(3) ! Node, edge and cell balance.
        integer, allocatable, dimension(:), public :: part ! Partition solution.
        type(cgns_reader_class), pointer :: reader => null()
    contains
        generic :: construct => construct_config, construct_direct
        procedure, private :: construct_config => paramesh_construct_config
        procedure, private :: construct_direct => paramesh_construct_direct
        procedure, private :: construct_common => paramesh_construct_common
        procedure, private :: split => paramesh_split
        procedure, private :: metis => paramesh_metis
        procedure, private :: defaults => paramesh_defaults
    end type paramesh_class
contains
    !-------------------------------------------------------------------------------------------------------------------------------
    ! Mesh partition class.
    !-------------------------------------------------------------------------------------------------------------------------------
    subroutine paramesh_construct_config(self, reader, configparser)
        class(paramesh_class), intent(inout) :: self
        type(cgns_reader_class), intent(inout), target :: reader
        type(configparser_class), intent(in) :: configparser
        integer :: stat

        call self%defaults()

        self%reader => reader
        call configparser%get("paramesh.autosplit", self%autosplit, stat=stat)
        call configparser%get("paramesh.nprocs", self%nprocs, stat=stat)
        call configparser%get("paramesh.nblocks", self%nblocks, stat=stat)
        call configparser%get("paramesh.maxretry", self%maxretry, stat=stat)
        call configparser%get("paramesh.stepsize", self%stepsize, stat=stat)

        call self%construct_common()
    end subroutine paramesh_construct_config

    subroutine paramesh_construct_direct(self, reader, nprocs, nblocks, maxretry, stepsize, autosplit)
        class(paramesh_class), intent(inout) :: self
        type(cgns_reader_class), intent(inout), target :: reader
        integer, intent(in), optional :: nprocs
        integer, intent(in), optional :: nblocks
        integer, intent(in), optional :: maxretry
        real(kind=REAL64), intent(in), optional :: stepsize
        logical, intent(in), optional :: autosplit

        call self%defaults()

        self%reader => reader

        if (present(nprocs)) then
            self%nprocs = nprocs
        end if
        if (present(nblocks)) then
            self%nblocks = nblocks
        end if
        if (present(maxretry)) then
            self%maxretry = maxretry
        end if
        if (present(stepsize)) then
            self%stepsize = stepsize
        end if
        if (present(autosplit)) then
            self%autosplit = autosplit
        end if

        call self%construct_common()
    end subroutine paramesh_construct_direct

    subroutine paramesh_construct_common(self)
        class(paramesh_class), intent(inout) :: self
        type(cgns_zone_structure), allocatable, dimension(:) :: zones
        type(exception_class) :: exception
        integer :: cutratio
        integer, allocatable, dimension(:) :: connmarks
        integer, allocatable, dimension(:) :: ncells
        integer :: avgsize
        integer :: maxlocs
        integer :: divsize
        integer :: stat
        integer :: i, k, m

        if (self%reader%parainit .or. .not. self%autosplit) return
        do i = 1, self%reader%nzones
            if (self%reader%zones(i)%zonetype .ne. STRUCTURED) then
                call exception%construct("Mesh is not structured.")
                call exception%raise()
            end if
        end do

        self%nprocs = max(self%nprocs, mpi%nprocs)
        self%nblocks = max(self%nblocks, self%nprocs*mpi%nsockets, self%reader%nzones)

        allocate(zones(self%nblocks))
        zones(1:self%reader%nzones) = self%reader%zones
        call move_alloc(zones, self%reader%zones)

        allocate(ncells(self%nblocks))
        ncells = 0
        do i = 1, self%reader%nzones
            ncells(i) = product(self%reader%zones(i)%zonesize-1)
        end do
        avgsize = sum(ncells)/self%nblocks

        ! Split zones.
        do while(self%reader%nzones < self%nblocks)
            maxlocs = maxloc(ncells, 1)
            divsize = nint(maxval(ncells)/real(avgsize))
            associate(dims => self%reader%cell_dimension)
            if (mod(divsize, dims) == 0) then
                cutratio = dims
            else if (nint(divsize**(1.0/dims))**dims == divsize) then
                cutratio = nint(divsize**(1.0/dims))
            else
                cutratio = divsize
            end if
            end associate
            cutratio = min(max(cutratio,2), self%nblocks-self%reader%nzones+1)

            call self%split(self%reader%zones(maxlocs), maxlocs, cutratio)

            ncells(maxlocs) = product(self%reader%zones(maxlocs)%zonesize-1)
            ncells(self%reader%nzones) = product(self%reader%zones(self%reader%nzones)%zonesize-1)
        end do
        self%balance(3) = maxval(ncells)/(minval(ncells)+tiny(1.0))

        ! Partition.
        call self%metis()

        ! Fix partition information.
        self%reader%zonelist = pack([(k, k=1,self%nblocks)], self%part==mpi%rank)
        self%reader%parainit = .true.
        do i = 1, self%reader%nzones
            self%reader%zones(i)%zonemark = i
            self%reader%zones(i)%zoneproc = self%part(i)
        end do

        ! Fix connection collections.
        do i = 1, self%reader%nzones
        associate(zone => self%reader%zones(i))
            deallocate(connmarks, stat=stat)
            allocate(connmarks(self%reader%nzones))
            connmarks = 0
            do k = 1, size(zone%connections)
                connmarks(zone%connections(k)%donorzone) = zone%connections(k)%donorzone
            end do
            connmarks = pack(connmarks, connmarks/=0)

            deallocate(self%reader%zones(i)%collections, stat=stat)
            allocate(self%reader%zones(i)%collections(size(connmarks)))
            do k = 1, size(connmarks)
                zone%collections(k)%connlist = pack([(m, m=1, size(zone%connections))], zone%connections%donorzone==connmarks(k))
                zone%collections(k)%localzone = i
                zone%collections(k)%donorzone = connmarks(k)
                zone%collections(k)%donorproc = self%reader%zones(connmarks(k))%zoneproc
            end do
        end associate
        end do

        ! Log message.
        if (mpi%rank .ne. 1) return
        call logger%msg(INFO, "A", "Paramesh partition solution:")
        call logger%msg(INFO, "*(I0,1X)", self%part)
        call logger%msg(INFO, "A", "Paramesh node, edge and cell balance:")
        call logger%msg(INFO, "*(G8.2,1X)", self%balance)
    end subroutine paramesh_construct_common

    subroutine paramesh_split(self, zone, index_zone, cutratio)
        class(paramesh_class), intent(inout) :: self
        type(cgns_zone_structure), intent(inout) :: zone
        integer, intent(in) :: index_zone
        integer, intent(in) :: cutratio
        type(cgns_connection_structure), allocatable, dimension(:) :: newconn
        type(cgns_zone_structure) :: newzone(2)
        character(len=128) :: zonename
        integer, allocatable, dimension(:,:) :: cutrange
        integer, allocatable, dimension(:) :: connmarks
        integer, allocatable, dimension(:) :: connsplit
        integer :: newindex(2)
        integer :: cutplane
        integer :: cursor
        integer :: i, k

        cutplane = maxloc(zone%zonesize, 1)

        allocate(cutrange(self%reader%cell_dimension, 4))
        cutrange(:,1) = 1
        cutrange(:,2) = zone%zonesize
        cutrange(:,3) = 1
        cutrange(:,4) = zone%zonesize
        cutrange(cutplane,2) = zone%zonesize(cutplane)/cutratio+1
        cutrange(cutplane,3) = cutrange(cutplane,2)

        allocate(connmarks(self%reader%nzones)) ! Record index of zones whose connections have been cut by the newly splitted zones.
        allocate(connsplit(self%reader%nzones)) ! Record number of splitted connections for zones in connmarks.
        connmarks = 0
        connsplit = 0

        newindex(1) = index_zone
        newindex(2) = self%reader%nzones+1

        ! General information of the newly splitted zones, also record connmarks and connsplit.
        write(zonename, "(A,I0)") "tarosplit", newindex(1)
        call split_subzone_common(newzone(1), cutrange(:,1:2), str%strip(zonename))
        write(zonename, "(A,I0)") "tarosplit", newindex(2)
        call split_subzone_common(newzone(2), cutrange(:,3:4), str%strip(zonename))

        ! Connection between the newly splitted zones.
        call split_subzone_conninter(newzone(1), newzone(2)%zonename, newindex(2), newzone(2)%zonesize, [cutrange(cutplane,2), 1])
        call split_subzone_conninter(newzone(2), newzone(1)%zonename, newindex(1), newzone(1)%zonesize, [1, cutrange(cutplane,2)])

        ! Rebuild the connection information of zones recorded in connmarks. (if any).
        connsplit = pack(connsplit/2, connmarks/=0) ! Remove duplicate.
        connmarks = pack(connmarks, connmarks/=0)
        do i = 1, size(connmarks)
        associate(oldconn => self%reader%zones(connmarks(i))%connections)
            allocate(newconn(size(oldconn)+connsplit(i))) ! Resize the number of connections.
            cursor = 1
            do k = 1, size(oldconn)
                if (oldconn(k)%donorname == zone%zonename) then
                    ! Rebuild the connection with newly splitted zones, also increase the cursor.
                    call split_subzone_connouter(newconn(cursor), oldconn(k), newzone(1)%zonename, newindex(1),&
                                                 newzone(1)%zonesize, cutrange(:,1:2), cursor)
                    call split_subzone_connouter(newconn(cursor), oldconn(k), newzone(2)%zonename, newindex(2),&
                                                 newzone(2)%zonesize, cutrange(:,3:4), cursor)
                else
                    newconn(cursor) = oldconn(k)
                    cursor = cursor+1
                end if
            end do
            self%reader%zones(connmarks(i))%connections = newconn
            deallocate(newconn)
        end associate
        end do

        self%reader%zones(newindex(1)) = newzone(1)
        self%reader%zones(newindex(2)) = newzone(2)
        self%reader%nzones = self%reader%nzones+1
    contains
        subroutine split_subzone_common(subzone, cutrange, subname)
            type(cgns_zone_structure), intent(inout) :: subzone
            integer, dimension(:,:), intent(in) :: cutrange
            character(len=*), intent(in) :: subname
            logical :: overlap(2)
            integer, allocatable, dimension(:) :: decoded
            integer, allocatable, dimension(:) :: marker
            integer :: encoded
            integer :: cursor
            integer :: nnodes
            integer :: i

            subzone%zonetype = zone%zonetype
            subzone%zonesize = cutrange(:,2)-cutrange(:,1)+1
            subzone%zonename = subname

            allocate(subzone%sections(0))
            allocate(subzone%collections(0))

            nnodes = product(subzone%zonesize)

            allocate(subzone%coords(nnodes, size(zone%coords,2)))
            do i = 1, nnodes
                decoded = indexmap%decode(subzone%zonesize, i)+cutrange(:,1)-1
                encoded = indexmap%encode(zone%zonesize, decoded)
                subzone%coords(i,:) = zone%coords(encoded,:)
            end do

            ! Record the index of bocos belongs to subzone.
            allocate(marker(size(zone%bocos)))
            marker = 0
            do i = 1, size(zone%bocos)
            associate(boco => zone%bocos(i))
                if (any(has_overlap(boco%scope(cutplane,:), cutrange(cutplane,:)) .eqv. .true.)) then
                    marker(i) = i
                end if
            end associate
            end do
            ! Rebuild the bocos of subzone.
            marker = pack(marker, marker/=0)
            allocate(subzone%bocos(size(marker)))
            do i = 1, size(subzone%bocos)
            associate(boco => subzone%bocos(i))
                boco = zone%bocos(marker(i))
                boco%scope(cutplane,:) = min(max(boco%scope(cutplane,:), cutrange(cutplane,1)), cutrange(cutplane,2))
                boco%scope(cutplane,:) = boco%scope(cutplane,:)-cutrange(cutplane,1)+1
            end associate
            end do

            ! Record the index of conns belongs to subzone, also record connmarks and connsplit.
            deallocate(marker)
            allocate(marker(size(zone%connections)))
            marker = 0
            do i = 1, size(zone%connections)
            associate(conn => zone%connections(i))
                overlap = has_overlap(conn%localscope(cutplane,:), cutrange(cutplane,:))
                if (any(overlap .eqv. .true.)) then
                    marker(i) = i
                    connmarks(conn%donorzone) = conn%donorzone
                    ! Increase counter if donorzone is splitted by subzone.
                    connsplit(conn%donorzone) = connsplit(conn%donorzone)+merge(0,1,overlap(1))
                end if
            end associate
            end do
            ! Rebuild the conns of subzone, connection with the other subzone will be constructed later.
            marker = pack(marker, marker/=0)
            allocate(subzone%connections(size(marker)+1))
            do i = 1, size(marker)
            associate(newconn => subzone%connections(i))
            associate(oldconn => zone%connections(marker(i)))
                newconn = oldconn
                newconn%localscope(cutplane,:) = max(newconn%localscope(cutplane,:), cutrange(cutplane,1))
                newconn%localscope(cutplane,:) = min(newconn%localscope(cutplane,:), cutrange(cutplane,2))
                newconn%donorscope(:,1) = matmul(newconn%transform, newconn%localscope(:,1)-oldconn%localscope(:,1))&
                                         +oldconn%donorscope(:,1)
                newconn%donorscope(:,2) = matmul(newconn%transform, newconn%localscope(:,2)-oldconn%localscope(:,1))&
                                         +oldconn%donorscope(:,1)
                newconn%localscope(cutplane,:) = newconn%localscope(cutplane,:)-cutrange(cutplane,1)+1
            end associate
            end associate
            end do
        end subroutine split_subzone_common

        subroutine split_subzone_conninter(subzone, donorname, donorzone, donorsize, scope)
            type(cgns_zone_structure), intent(inout) :: subzone
            character(len=*), intent(in) :: donorname
            integer, intent(in) :: donorzone
            integer, dimension(:), intent(in) :: donorsize
            integer, dimension(:), intent(in) :: scope

            associate(conn => subzone%connections(size(subzone%connections)))
                conn%donorname = donorname
                conn%donorzone = donorzone
                conn%donorsize = donorsize
                allocate(conn%transform(self%reader%cell_dimension, self%reader%cell_dimension))
                allocate(conn%localscope(self%reader%cell_dimension, 2))
                allocate(conn%donorscope(self%reader%cell_dimension, 2))
                conn%transform = 0
                do i = 1, self%reader%cell_dimension
                    conn%transform(i,i) = 1
                end do
                conn%localscope(:,1) = 1
                conn%localscope(:,2) = zone%zonesize
                conn%donorscope(:,1) = 1
                conn%donorscope(:,2) = zone%zonesize
                conn%localscope(cutplane,:) = scope(1)
                conn%donorscope(cutplane,:) = scope(2)
            end associate
        end subroutine split_subzone_conninter

        subroutine split_subzone_connouter(newconn, oldconn, donorname, donorzone, donorsize, cutrange, cursor)
            type(cgns_connection_structure), intent(inout) :: newconn
            type(cgns_connection_structure), intent(in) :: oldconn
            character(len=*), intent(in) :: donorname
            integer, intent(in) :: donorzone
            integer, dimension(:), intent(in) :: donorsize
            integer, dimension(:,:), intent(in) :: cutrange
            integer, intent(inout) :: cursor

            if (any(has_overlap(oldconn%donorscope(cutplane,:), cutrange(cutplane,:)) .eqv. .true.)) then
                newconn = oldconn
                newconn%donorname = donorname
                newconn%donorzone = donorzone
                newconn%donorsize = donorsize
                newconn%donorscope(cutplane,:) = max(newconn%donorscope(cutplane,:), cutrange(cutplane,1))
                newconn%donorscope(cutplane,:) = min(newconn%donorscope(cutplane,:), cutrange(cutplane,2))
                newconn%localscope(:,1) = matmul(newconn%donorscope(:,1)-oldconn%donorscope(:,1), newconn%transform)&
                                         +oldconn%localscope(:,1)
                newconn%localscope(:,2) = matmul(newconn%donorscope(:,2)-oldconn%donorscope(:,1), newconn%transform)&
                                         +oldconn%localscope(:,1)
                newconn%donorscope(cutplane,:) = newconn%donorscope(cutplane,:)-cutrange(cutplane,1)+1
                cursor = cursor+1
            end if
        end subroutine split_subzone_connouter

        function has_overlap(localscope, donorscope)
            integer, dimension(:), intent(in) :: localscope
            integer, dimension(:), intent(in) :: donorscope
            logical :: has_overlap(2)

            has_overlap(1) = all(localscope >= donorscope(1)) .and. &
                             all(localscope <= donorscope(2)) ! Full overlap.
            has_overlap(2) = minval(localscope) < donorscope(2) .and. &
                             maxval(localscope) > donorscope(1) ! Partial overlap.
        end function has_overlap
    end subroutine paramesh_split

    subroutine paramesh_metis(self)
        class(paramesh_class), intent(inout) :: self
        type(exception_class) :: exception
        real(kind=METIS_REAL), allocatable, dimension(:) :: ubvec ! Allowed load imbalance for each constraint.
        real(kind=METIS_REAL), allocatable, dimension(:) :: tpwgts ! Desired weights for each partition and constraint.
        integer(kind=METIS_INT), allocatable, dimension(:) :: xadj ! Index of connection information.
        integer(kind=METIS_INT), allocatable, dimension(:) :: adjncy ! Connection information.
        integer(kind=METIS_INT), allocatable, dimension(:) :: vwgt ! Weight of vertex (number of nodes).
        integer(kind=METIS_INT), allocatable, dimension(:) :: adjwgt ! Weight of edges (number of boundary nodes).
        integer(kind=METIS_INT), allocatable, dimension(:) :: part ! Partition solution.
        integer(kind=METIS_INT), dimension(:), pointer :: ptr => null() ! Dummy.
        integer(kind=METIS_INT) :: nvtxs ! Number of vertex (blocks)
        integer(kind=METIS_INT) :: nconn ! Number of constraints.
        integer(kind=METIS_INT) :: nedge ! Number of 2xedges (connections).
        integer(kind=METIS_INT) :: nparts ! Number of partitions.
        integer(kind=METIS_INT) :: objval ! Object value.
        integer, allocatable, dimension(:) :: balance_node ! Balance info of node(vertex).
        integer, allocatable, dimension(:) :: balance_edge ! Balance info of edge.
        integer :: cursor
        integer :: i, k

        if (self%nblocks < 2 .or. self%nprocs < 2) then
            self%part = [(1, i=1, self%nblocks)]
            self%balance = 1.0
            return
        end if

        nparts = self%nprocs
        nvtxs = self%nblocks
        nconn = 1
        nedge = 0
        do i = 1, nvtxs
            nedge = nedge+size(self%reader%zones(i)%connections)
        end do
        allocate(xadj(nvtxs+1))
        allocate(adjncy(nedge))
        allocate(vwgt(nvtxs*nconn))
        allocate(adjwgt(nedge))
        allocate(tpwgts(nparts*nconn))
        allocate(ubvec(nconn))
        allocate(part(nvtxs))
        cursor = 1
        do i = 1, nvtxs
            ! Connection information.
            xadj(i) = cursor
            do k = 1, size(self%reader%zones(i)%connections)
            associate(conn => self%reader%zones(i)%connections(k))
                adjncy(cursor) = conn%donorzone
                adjwgt(cursor) = product(abs(conn%localscope(:,2)-conn%localscope(:,1))+1)
                cursor = cursor+1
            end associate
            end do
            ! Weight of vertex.
            vwgt(i) = product(self%reader%zones(i)%zonesize)
        end do
        xadj(nvtxs+1) = cursor
        tpwgts = 1.0/(nparts*nconn)
        ubvec = 1.001

        ! C-style numbering.
        xadj = xadj-1
        adjncy = adjncy-1

        do i = 1, self%maxretry
            call METIS_PartGraphRecursive(nvtxs, nconn, xadj, adjncy, vwgt, ptr, adjwgt, nparts, tpwgts, ubvec, ptr, objval, part)
            if (all([(any(part==k-1), k=1, self%nprocs)])) exit ! Exit if all procs are used.
            ubvec = ubvec*self%stepsize ! Increase tolerance.
        end do

        ! Fortran-style numbering.
        part = part+1
        xadj = xadj+1
        adjncy = adjncy+1

        ! Balance check.
        allocate(balance_node(nparts))
        allocate(balance_edge(nparts))
        balance_node = 0
        balance_edge = 0
        do i = 1, nvtxs
            balance_node(part(i)) = balance_node(part(i))+vwgt(i)
            do k = xadj(i), xadj(i+1)-1
                if (part(adjncy(k)) .ne. part(i)) then
                    balance_edge(part(i)) = balance_edge(part(i))+adjwgt(k)
                end if
            end do
        end do

        if (minval(balance_node) == 0) then ! Failed.
            call exception%construct("Paramesh reach maximum retry attempt.")
            call exception%raise()
        else
            self%part = part
            self%balance(1) = maxval(balance_node)/(minval(balance_node)+tiny(1.0))
            self%balance(2) = maxval(balance_edge)/(minval(balance_edge)+tiny(1.0))
        end if
    end subroutine paramesh_metis

    subroutine paramesh_defaults(self)
        class(paramesh_class), intent(inout) :: self
        self%autosplit = .true.
        self%nprocs = 1
        self%nblocks = 1
        self%maxretry = 10
        self%stepsize = 1.01
    end subroutine paramesh_defaults
end module class_paramesh

! vim: set ft=fortran ff=unix tw=132:
