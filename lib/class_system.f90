! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
! Description:
!
! Library for underlying hardware and operating system, similar to Python os and os.path.
!
module class_system
    use class_string
    implicit none
    private
    public :: system

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Constants.
    !-------------------------------------------------------------------------------------------------------------------------------
    character(len=1), parameter :: OS_SEPERATOR = "/"

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Path manipulations.
    !-------------------------------------------------------------------------------------------------------------------------------
    type :: path_class
    contains
        procedure :: join
        procedure :: dirname
        procedure :: rootname
        procedure :: splitext
    end type path_class

    !-------------------------------------------------------------------------------------------------------------------------------
    ! System manipulations.
    !-------------------------------------------------------------------------------------------------------------------------------
    type :: system_class
        private
        type(path_class), public :: path
    end type system_class

    type(system_class) :: system
contains
    !-------------------------------------------------------------------------------------------------------------------------------
    ! Path manipulation functions.
    !-------------------------------------------------------------------------------------------------------------------------------
    pure function join(self, names)
        class(path_class), intent(in) :: self
        type(string), dimension(:), intent(in) :: names
        character(len=:), allocatable :: join

        join = str%join(names, OS_SEPERATOR)
    end function join

    pure function dirname(self, path)
        class(path_class), intent(in) :: self
        character(len=*), intent(in) :: path
        character(len=:), allocatable :: dirname
        integer :: location

        location = index(path, OS_SEPERATOR, back=.true.)

        if (location == 0) then
            dirname = ''
        else
            dirname = path(:location-1)
        end if
    end function dirname

    pure function rootname(self, path)
        class(path_class), intent(in) :: self
        character(len=*), intent(in) :: path
        character(len=:), allocatable :: rootname
        integer :: location

        location = index(path, OS_SEPERATOR, back=.true.)
        location = index(path(location+1:), ".")+location

        rootname = path(:location-1)
    end function rootname

    pure function splitext(self, path)
        class(path_class), intent(in) :: self
        character(len=*), intent(in) :: path
        type(string) :: splitext(2)
        integer :: location

        location = index(path, OS_SEPERATOR, back=.true.)
        location = index(path(location+1:), ".")+location

        splitext(1) = path(:location-1)
        splitext(2) = path(location:)
    end function splitext
end module class_system

! vim: set ft=fortran ff=unix tw=132:
