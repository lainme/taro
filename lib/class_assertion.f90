! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
! Description:
!
! Assert values are equal for unit tests.
!
module class_assertion
    use iso_fortran_env, only : OUTPUT_UNIT, ERROR_UNIT
    use class_string
    implicit none
    private
    public :: assertion_class

    type :: assertion_class
        private
        character(len=:), allocatable :: identifier ! What's testing?
        logical :: success
    contains
        procedure :: construct
        procedure :: assert
        procedure :: output
    end type assertion_class
contains
    elemental subroutine construct(self, identifier)
        class(assertion_class), intent(inout) :: self
        character(len=*), intent(in) :: identifier

        self%identifier = str%strip(identifier)
        self%success = .true.
    end subroutine construct

    elemental subroutine assert(self, success)
        class(assertion_class), intent(inout) :: self
        logical, intent(in) :: success

        if (.not. success) then
            self%success = success
        end if
    end subroutine assert

    subroutine output(self)
        class(assertion_class), intent(in) :: self

        if (self%success) then
            write(OUTPUT_UNIT, "(A,1X,A)") str%colorize("[PASS]:", "green"), self%identifier
        else
            write(ERROR_UNIT, "(A,1X,A)") str%colorize("[FAIL]:", "red"), self%identifier
        end if
    end subroutine output
end module class_assertion

! vim: set ft=fortran ff=unix tw=132:
