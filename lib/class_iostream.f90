! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
! Description:
!
! Basic input/output streaming functions.
!
module class_iostream
    use iso_fortran_env, only : IOSTAT_END, IOSTAT_EOR
    use class_exception
    use class_string
    implicit none
    private
    public :: iostream

    type :: iostream_class
    contains
        procedure :: readline ! Read a whole line of unknown length.
        procedure :: readlines ! Read whole file.
    end type iostream_class

    type(iostream_class) :: iostream
contains
    function readline(self, funit, stat)
        class(iostream_class), intent(in) :: self
        integer, intent(in) :: funit
        integer, intent(out) :: stat
        character(len=:), allocatable :: readline
        type(exception_class) :: exception
        character(len=128) :: buffer
        integer :: buffer_size

        readline = ''

        do while(.true.)
            read(funit, "(A)", advance="no", iostat=exception%stat, iomsg=exception%message, size=buffer_size) buffer
            call exception%raise()

            readline = readline//buffer(:buffer_size)

            if (exception%stat == IOSTAT_END .or. exception%stat == IOSTAT_EOR) then
                stat = exception%stat
                return
            end if
        end do
    end function readline

    function readlines(self, funit)
        class(iostream_class), intent(in) :: self
        integer, intent(in) :: funit
        type(string), allocatable, dimension(:) :: readlines
        type(exception_class) :: exception
        integer :: nlines
        integer :: stat
        integer :: i

        nlines = 0
        rewind(funit)
        do while(.true.)
            read(funit, *, iostat=exception%stat, iomsg=exception%message)
            call exception%raise()
            if (exception%stat .ne. IOSTAT_END) then
                nlines = nlines+1
            else
                exit
            end if
        end do
        allocate(readlines(nlines))
        rewind(funit)

        do i = 1, nlines
            readlines(i) = self%readline(funit, stat)
        end do
    end function readlines
end module class_iostream

! vim: set ft=fortran ff=unix tw=132:
