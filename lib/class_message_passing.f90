! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2018 lainme <lainme993@gmail.com>
!
! Description:
!
! Class for MPI and OpenMP handling.
!
module class_message_passing
    use iso_fortran_env, only : REAL64
    use omp_lib
    use class_exception
    implicit none
    private :: message_passing_class
    private :: construct, cleanup

    include 'mpif.h'

    type :: message_passing_class
        private
        integer, public :: nprocs = 1
        integer, public :: rank = 1
        integer, public :: nsockets = 1 ! Sockets per process.
        integer, public :: nthreads = 1 ! Total number of omp threads per process.
        integer, public :: inneromp = 1 ! Inner omp threads.
        logical :: init = .false.
        integer :: stat(MPI_STATUS_SIZE)
        integer :: ierr
    contains
        procedure :: construct
        final :: cleanup
    end type message_passing_class

    type(message_passing_class) :: mpi
contains
    subroutine construct(self, nsockets)
        class(message_passing_class), intent(inout) :: self
        type(exception_class) :: exception
        integer, intent(in), optional :: nsockets
        integer :: provided

        if (.not. self%init) then
            call mpi_init_thread(MPI_THREAD_MULTIPLE, provided, self%ierr)
            if (MPI_THREAD_MULTIPLE .ne. provided) then
                call exception%construct("MPI donesn't support MPI_THREAD_MULTIPLE")
                call exception%raise()
            end if
            call mpi_comm_size(MPI_COMM_WORLD, self%nprocs, self%ierr)
            call mpi_comm_rank(MPI_COMM_WORLD, self%rank, self%ierr)
            self%rank = self%rank+1
            self%init = .true.
            if (present(nsockets)) then
                self%nsockets = nsockets
            end if
            self%nthreads = omp_get_max_threads()
            self%inneromp = self%nthreads/self%nsockets
            call omp_set_nested(.true.)
        end if
    end subroutine construct

    subroutine cleanup(self)
        type(message_passing_class), intent(inout) :: self

        call mpi_finalize(self%ierr)
    end subroutine cleanup
end module class_message_passing

! vim: set ft=fortran ff=unix tw=132:
