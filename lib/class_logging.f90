! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2018 lainme <lainme993@gmail.com>
!
! Description:
!
! A simple logging tool. Fortran doesn't support arbitrary argument list, handling all types of logging within this class is
! difficult. So unlike Python/C, this class defines the logging level and unit, and only handle most common logging cases.
!
module class_logging
    use iso_fortran_env, only : INT32, INT64, REAL32, REAL64, OUTPUT_UNIT, ERROR_UNIT
    use class_string
    implicit none
    private
    public :: DEBUG
    public :: INFO
    public :: WARNING
    public :: ERROR
    public :: SUPPRESS
    public :: logger

    ! Logging levels
    integer, parameter :: DEBUG = 1
    integer, parameter :: INFO = 2
    integer, parameter :: WARNING = 3
    integer, parameter :: ERROR = 4
    integer, parameter :: SUPPRESS = 5 ! Use this to suppress the output.

    ! Logging leading strings
    character(len=10), parameter :: PREFIX_STRING(4) = ["[DEBUG]:  ", "[INFO]:   ", "[WARNING]:", "[ERROR]:  "]
    character(len=10), parameter :: PREFIX_COLORS(4) = ["blue      ", "green     ", "yellow    ", "red       "]

    type :: logging_class
        private
        integer, allocatable, public :: funit
        integer, public :: stdout_level = INFO
        integer, public :: stderr_level = ERROR
        integer, public :: fileio_level = SUPPRESS
    contains
        procedure :: construct
        generic :: msg => msg_string, msg_vector
        procedure, private :: msg_string
        procedure, private :: msg_vector
        final :: logging_cleanup
    end type logging_class

    type(logging_class) :: logger
contains
    subroutine construct(self, logfile, stdout_level, stderr_level, fileio_level)
        class(logging_class), intent(inout) :: self
        character(len=*), intent(in), optional :: logfile
        integer, intent(in), optional :: stdout_level
        integer, intent(in), optional :: stderr_level
        integer, intent(in), optional :: fileio_level

        call logging_cleanup(self)

        if (present(logfile)) then
            allocate(self%funit)
            open(newunit=self%funit, file=logfile, status="unknown", action="write")
            if (present(fileio_level)) then
                self%fileio_level = fileio_level
            else
                self%fileio_level = INFO
            end if
        end if

        if (present(stdout_level)) then
            self%stdout_level = stdout_level
        end if

        if (present(stderr_level)) then
            self%stderr_level = stderr_level
        end if
    end subroutine construct

    subroutine msg_string(self, level, fmtstr, message)
        class(logging_class), intent(in) :: self
        integer, intent(in) :: level
        character(len=*), intent(in) :: fmtstr
        character(len=*), intent(in) :: message

        if (level >= self%stderr_level) then
            write(ERROR_UNIT, "(A,1X,"//fmtstr//")") str%colorize(str%strip(PREFIX_STRING(level)), PREFIX_COLORS(level)), message
        else if (level >= self%stdout_level) then
            write(OUTPUT_UNIT, "(A,1X,"//fmtstr//")") str%colorize(str%strip(PREFIX_STRING(level)), PREFIX_COLORS(level)), message
        end if

        if (level >= self%fileio_level) then
            write(self%funit, "(A,1X,"//fmtstr//")") str%strip(PREFIX_STRING(level)), message
        endif
    end subroutine msg_string

    subroutine msg_vector(self, level, fmtstr, message)
        class(logging_class), intent(in) :: self
        integer, intent(in) :: level
        character(len=*), intent(in) :: fmtstr
        class(*), dimension(:), intent(in) :: message

        if (level >= self%stderr_level) then
            call msg_log(ERROR_UNIT, str%colorize(str%strip(PREFIX_STRING(level)), PREFIX_COLORS(level)))
        else if (level >= self%stdout_level) then
            call msg_log(OUTPUT_UNIT, str%colorize(str%strip(PREFIX_STRING(level)), PREFIX_COLORS(level)))
        end if

        if (level >= self%fileio_level) then
            call msg_log(self%funit, str%strip(PREFIX_STRING(level)))
        endif
    contains
        subroutine msg_log(logunit, leading)
            integer, intent(in) :: logunit
            character(len=*), intent(in) :: leading

            select type(message)
            type is(integer(kind=INT32))
                write(logunit, "(A,1X,"//fmtstr//")") leading, message
            type is(integer(kind=INT64))
                write(logunit, "(A,1X,"//fmtstr//")") leading, message
            type is(real(kind=REAL32))
                write(logunit, "(A,1X,"//fmtstr//")") leading, message
            type is(real(kind=REAL64))
                write(logunit, "(A,1X,"//fmtstr//")") leading, message
            type is(logical)
                write(logunit, "(A,1X,"//fmtstr//")") leading, message
            type is(complex)
                write(logunit, "(A,1X,"//fmtstr//")") leading, message
            type is(string)
                write(logunit, "(A,1X,"//fmtstr//")") leading, message
            end select
        end subroutine msg_log
    end subroutine msg_vector

    elemental impure subroutine logging_cleanup(self)
        type(logging_class), intent(inout) :: self
        integer :: ier

        self%stderr_level = ERROR
        self%stdout_level = INFO
        self%fileio_level = SUPPRESS

        if (allocated(self%funit)) then
            close(self%funit, iostat=ier)
            deallocate(self%funit, stat=ier)
        end if
    end subroutine logging_cleanup
end module class_logging

! vim: set ft=fortran ff=unix tw=132:
