! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
! Description:
!
! Properties of geometric entities.
!
module class_geometry
    use iso_fortran_env, only : REAL64
    implicit none
    private
    public :: geometry

    type :: geometry_class
    contains
        procedure :: compute_line
        procedure :: compute_trig
        procedure :: compute_quad
        procedure :: compute_volume
        procedure, private :: compute_transformation
    end type geometry_class

    type(geometry_class) :: geometry
contains
    pure subroutine compute_line(self, nodes, centriod, length, directions)
        class(geometry_class), intent(in) :: self
        real(kind=REAL64), dimension(:,:), intent(in) :: nodes
        real(kind=REAL64), dimension(:), intent(out) :: centriod
        real(kind=REAL64), intent(out) :: length
        real(kind=REAL64), dimension(:,:), intent(out), optional :: directions
        integer :: i

        do i = 1, size(nodes,1)
            centriod(i) = 0.5*(nodes(i,1)+nodes(i,2))
        end do

        length = norm2(nodes(:,2)-nodes(:,1))

        if (present(directions)) then ! Only for 2D.
            directions(:,2) = (nodes(:,2)-nodes(:,1))/length
            directions(1,1) = +directions(2,2)/sum(directions(:,2)**2)
            directions(2,1) = -directions(1,2)/sum(directions(:,2)**2)
        end if
    end subroutine compute_line

    pure subroutine compute_trig(self, nodes, centriod, area, directions)
        class(geometry_class), intent(in) :: self
        real(kind=REAL64), dimension(:,:), intent(in) :: nodes
        real(kind=REAL64), dimension(:), intent(out) :: centriod
        real(kind=REAL64), intent(out) :: area
        real(kind=REAL64), dimension(:,:), intent(out), optional :: directions
        real(kind=REAL64) :: ds(3)
        integer :: i

        do i = 1, size(nodes,1)
            centriod(i) = (nodes(i,1)+nodes(i,2)+nodes(i,3))/3.0
        end do

        ds(3) = 0.5*((nodes(1,1)-nodes(1,2))*(nodes(2,1)+nodes(2,2))&
                    +(nodes(1,2)-nodes(1,3))*(nodes(2,2)+nodes(2,3))&
                    +(nodes(1,3)-nodes(1,1))*(nodes(2,3)+nodes(2,1)))
        if (size(nodes,1) .eq. 3) then
            ds(2) = 0.5*((nodes(3,1)-nodes(3,2))*(nodes(1,1)+nodes(1,2))&
                        +(nodes(3,2)-nodes(3,3))*(nodes(1,2)+nodes(1,3))&
                        +(nodes(3,3)-nodes(3,1))*(nodes(1,3)+nodes(1,1)))
            ds(1) = 0.5*((nodes(2,1)-nodes(2,2))*(nodes(3,1)+nodes(3,2))&
                        +(nodes(2,2)-nodes(2,3))*(nodes(3,2)+nodes(3,3))&
                        +(nodes(2,3)-nodes(2,1))*(nodes(3,3)+nodes(3,1)))
        else
            ds(2) = 0.0
            ds(1) = 0.0
        end if
        area = norm2(ds)

        if (present(directions)) then
            directions(:,1) = ds/area
            directions(:,2) = nodes(:,2)-nodes(:,1)
            directions(:,2) = directions(:,2)/norm2(directions(:,2))
            directions(:,3) = self%compute_transformation(directions(:,1), directions(:,2))
        end if
    end subroutine compute_trig

    pure subroutine compute_quad(self, nodes, centriod, area, directions)
        class(geometry_class), intent(in) :: self
        real(kind=REAL64), dimension(:,:), intent(in) :: nodes
        real(kind=REAL64), dimension(:), intent(out) :: centriod
        real(kind=REAL64), intent(out) :: area
        real(kind=REAL64), dimension(:,:), intent(out), optional :: directions
        real(kind=REAL64) :: trig_centriod(size(centriod),2)
        real(kind=REAL64) :: trig_area(2)
        real(kind=REAL64) :: trig_directions(3,3,2)

        if (.not. present(directions)) then
            call self%compute_trig(reshape([nodes(:,1),nodes(:,2),nodes(:,3)],[size(centriod),3]), trig_centriod(:,1), trig_area(1))
            call self%compute_trig(reshape([nodes(:,1),nodes(:,3),nodes(:,4)],[size(centriod),3]), trig_centriod(:,2), trig_area(2))
            centriod = (trig_centriod(:,1)*trig_area(1)+trig_centriod(:,2)*trig_area(2))/sum(trig_area)
            area = sum(trig_area)
        else
            call self%compute_trig(reshape([nodes(:,1),nodes(:,2),nodes(:,3)],[size(centriod),3]),&
                trig_centriod(:,1), trig_area(1), trig_directions(:,:,1))
            call self%compute_trig(reshape([nodes(:,1),nodes(:,3),nodes(:,4)],[size(centriod),3]),&
                trig_centriod(:,2), trig_area(2), trig_directions(:,:,2))
            centriod = (trig_centriod(:,1)*trig_area(1)+trig_centriod(:,2)*trig_area(2))/sum(trig_area)

            directions(:,1) = trig_directions(:,1,1)*trig_area(1)+trig_directions(:,1,2)*trig_area(2)
            area = norm2(directions(:,1))

            directions(:,1) = directions(:,1)/area
            directions(:,2) = nodes(:,2)-nodes(:,1)
            select case(maxloc(abs(directions(:,1)),1))
            case(1)
                directions(1,2) = (-directions(2,1)*directions(2,2)-directions(3,1)*directions(3,2))/directions(1,1)
            case(2)
                directions(2,2) = (-directions(1,1)*directions(1,2)-directions(3,1)*directions(3,2))/directions(2,1)
            case default
                directions(3,2) = (-directions(1,1)*directions(1,2)-directions(2,1)*directions(2,2))/directions(3,1)
            end select
            directions(:,2) = directions(:,2)/norm2(directions(:,2))
            directions(:,3) = self%compute_transformation(directions(:,1), directions(:,2))
        end if
    end subroutine compute_quad

    pure subroutine compute_volume(self, face_centriods, face_surfaces, centriod, volume)
        class(geometry_class), intent(in) :: self
        real(kind=REAL64), dimension(:,:), intent(in) :: face_centriods
        real(kind=REAL64), dimension(:,:), intent(in) :: face_surfaces ! Surface area vectors.
        real(kind=REAL64), dimension(:), intent(out) :: centriod
        real(kind=REAL64), intent(out) :: volume
        real(kind=REAL64) :: face_product
        integer :: i

        volume = 0.0
        centriod = 0.0

        do i = 1, size(face_centriods,2)
            face_product = dot_product(face_centriods(:,i), face_surfaces(:,i))
            volume = volume+face_product
            centriod = centriod+face_product*face_centriods(:,i)
        end do
        volume = volume/3.0
        centriod = centriod/volume/4.0
    end subroutine compute_volume

    pure function compute_transformation(self, norm, tanA) result(tanB)
        class(geometry_class), intent(in) :: self
        real(kind=REAL64), dimension(:), intent(in) :: norm ! Normal vector.
        real(kind=REAL64), dimension(:), intent(in) :: tanA ! Tangent vector (first).
        real(kind=REAL64) :: tanB(3)

        ! Calculate second tangent vector by cross product.
        tanB(1) = norm(2)*tanA(3)-norm(3)*tanA(2)
        tanB(2) = norm(3)*tanA(1)-norm(1)*tanA(3)
        tanB(3) = norm(1)*tanA(2)-norm(2)*tanA(1)
    end function compute_transformation
end module class_geometry

! vim: set ft=fortran ff=unix tw=132:
