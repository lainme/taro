! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
! Description:
!
! Mapping implemented via hash table, chained with linked list. Pointers are dangerous.
!
module class_hashmap
    use iso_fortran_env, only : REAL64
    implicit none
    private
    public :: hashmap_pair
    public :: hashmap_class

    type, abstract :: hashmap_pair
    contains
        procedure(has_samekey), deferred :: has_samekey ! Compare whether keys are equal.
        procedure(copy), deferred :: copy ! Copy the value.
        procedure(hash), deferred :: hash
    end type hashmap_pair

    abstract interface
        elemental function has_samekey(self, pair)
            import :: hashmap_pair
            class(hashmap_pair), intent(in) :: self
            class(hashmap_pair), intent(in) :: pair
            logical :: has_samekey
        end function has_samekey

        subroutine copy(self, object)
            import :: hashmap_pair
            class(hashmap_pair), intent(in) :: self
            class(*), intent(out) :: object
        end subroutine copy

        elemental function hash(self)
            import :: hashmap_pair
            class(hashmap_pair), intent(in) :: self
            integer :: hash
        end function hash
    end interface

    type :: hashmap_bucket ! Linked list.
        private
        type(hashmap_bucket), pointer :: next => null()
        class(hashmap_pair), pointer :: pair => null()
    contains
        procedure :: haskey => bucket_haskey
        procedure :: remove => bucket_remove
        procedure :: set => bucket_set
        procedure :: find => bucket_find ! Find pointer to the pair.
        generic :: pick => bucket_pick_compact, bucket_pick_verbose ! Pick object of the pair.
        procedure, private :: bucket_pick_compact
        procedure, private :: bucket_pick_verbose
        final :: bucket_cleanup
    end type hashmap_bucket

    type :: hashmap_iterator
        private
        integer :: current_index
        integer :: maximum_index
        type(hashmap_bucket), pointer, contiguous, dimension(:) :: buckets => null()
        type(hashmap_bucket), pointer :: bucket => null()
    contains
        procedure :: construct => iterator_construct
        procedure :: hasnext => iterator_hasnext
        procedure :: find => iterator_find ! Find pointer to the pair.
        procedure :: pick => iterator_pick ! Pick object of the pair.
    end type hashmap_iterator

    type :: hashmap_class
        private
        integer :: nbuckets ! Size of the table.
        integer :: npairs ! Number of pairs.
        real(kind=REAL64) :: maxload ! Maximum load allowed.
        type(hashmap_bucket), pointer, dimension(:) :: buckets => null()
        type(hashmap_iterator), public :: iterator
    contains
        procedure :: construct => hashmap_construct
        procedure :: haskey => hashmap_haskey
        procedure :: remove => hashmap_remove
        procedure :: set => hashmap_set
        procedure :: find => hashmap_find ! Find pointer to the pair.
        generic :: pick => hashmap_pick_compact, hashmap_pick_verbose ! Pick object of the pair.
        procedure, private :: hashmap_pick_compact
        procedure, private :: hashmap_pick_verbose
        procedure, private :: resize => hashmap_resize
        final :: hashmap_cleanup
    end type hashmap_class
contains
    !-------------------------------------------------------------------------------------------------------------------------------
    ! Hashmap bucket methods.
    !-------------------------------------------------------------------------------------------------------------------------------
    pure recursive function bucket_haskey(self, pair) result(haskey)
        class(hashmap_bucket), intent(in) :: self
        class(hashmap_pair), intent(in) :: pair
        logical :: haskey

        haskey = .false.

        if (associated(self%pair)) then
            if (self%pair%has_samekey(pair)) then
                haskey = .true.
                return
            end if
        end if

        if (associated(self%next)) then
            haskey = self%next%haskey(pair)
        end if
    end function bucket_haskey

    recursive function bucket_remove(self, pair) result(success)
        class(hashmap_bucket), intent(inout) :: self
        class(hashmap_pair), intent(in) :: pair
        logical :: success
        class(hashmap_bucket), pointer :: next

        success = .false.

        if (associated(self%pair)) then
            if (self%pair%has_samekey(pair)) then
                deallocate(self%pair)

                next => self%next
                if (associated(next)) then
                    self%pair => next%pair
                    self%next => next%next

                    next%pair => null()
                    next%next => null()
                    deallocate(next)
                end if

                success = .true.
                return
            end if
        end if

        if (associated(self%next)) then
            success = self%next%remove(pair)
        end if
    end function bucket_remove

    pure recursive subroutine bucket_set(self, pair)
        class(hashmap_bucket), intent(inout) :: self
        class(hashmap_pair), intent(in) :: pair

        if (associated(self%pair)) then
            if (self%pair%has_samekey(pair)) then
                deallocate(self%pair)
                allocate(self%pair, source=pair)
                return
            end if
        else
            allocate(self%pair, source=pair)
            return
        end if

        if (associated(self%next)) then
            call self%next%set(pair)
        else
            allocate(self%next)
            allocate(self%next%pair, source=pair)
        end if
    end subroutine bucket_set

    recursive function bucket_find(self, key, content) result(success)
        class(hashmap_bucket), target, intent(in) :: self
        class(hashmap_pair), intent(in) :: key
        class(hashmap_pair), pointer, intent(out) :: content
        logical :: success

        success = .false.

        if (associated(self%pair)) then
            if (self%pair%has_samekey(key)) then
                success = .true.
                content => self%pair
                return
            end if
        end if

        if (associated(self%next)) then
            success = self%next%find(key, content)
        end if
    end function bucket_find

    recursive function bucket_pick_compact(self, pair) result(success)
        class(hashmap_bucket), intent(in) :: self
        class(hashmap_pair), intent(inout) :: pair
        logical :: success

        success = .false.

        if (associated(self%pair)) then
            if (self%pair%has_samekey(pair)) then
                call self%pair%copy(pair)
                success = .true.
                return
            end if
        end if

        if (associated(self%next)) then
            success = self%next%pick(pair)
        end if
    end function bucket_pick_compact

    recursive function bucket_pick_verbose(self, key, content) result(success)
        class(hashmap_bucket), intent(in) :: self
        class(hashmap_pair), intent(in) :: key
        class(*), intent(out) :: content
        logical :: success

        success = .false.

        if (associated(self%pair)) then
            if (self%pair%has_samekey(key)) then
                call self%pair%copy(content)
                success = .true.
                return
            end if
        end if

        if (associated(self%next)) then
            success = self%next%pick(key, content)
        end if
    end function bucket_pick_verbose

    elemental subroutine bucket_cleanup(self)
        type(hashmap_bucket), intent(inout) :: self

        if (associated(self%pair)) then
            deallocate(self%pair)
        end if

        if (associated(self%next)) then
            deallocate(self%next)
        end if
    end subroutine bucket_cleanup

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Hashmap iterator methods.
    !-------------------------------------------------------------------------------------------------------------------------------
    subroutine iterator_construct(self, buckets)
        class(hashmap_iterator), intent(inout) :: self
        type(hashmap_bucket), target, contiguous, dimension(:), intent(in) :: buckets

        self%current_index = 0
        self%maximum_index = size(buckets)
        self%buckets => buckets
        self%bucket => null()
    end subroutine iterator_construct

    recursive function iterator_hasnext(self) result(hasnext)
        class(hashmap_iterator), intent(inout) :: self
        logical :: hasnext

        if (associated(self%bucket)) then
            self%bucket => self%bucket%next
        end if

        if (.not. associated(self%bucket)) then
            if (self%current_index < self%maximum_index) then
                self%current_index = self%current_index+1
                self%bucket => self%buckets(self%current_index)
            else ! Destroy.
                self%bucket => null()
            end if
        end if

        if (associated(self%bucket)) then
            if (associated(self%bucket%pair)) then
                hasnext = .true.
                return
            end if
            hasnext = self%hasnext()
        else
            hasnext = .false.
            self%current_index = 0 ! Reset.
        end if
    end function iterator_hasnext

    function iterator_find(self, pair) result(hasnext)
        class(hashmap_iterator), intent(inout) :: self
        class(hashmap_pair), pointer, intent(out) :: pair
        logical :: hasnext

        hasnext = self%hasnext()

        if (hasnext) then
            pair => self%bucket%pair
        end if
    end function iterator_find

    function iterator_pick(self, pair) result(hasnext)
        class(hashmap_iterator), intent(inout) :: self
        class(hashmap_pair), intent(out) :: pair
        logical :: hasnext

        hasnext = self%hasnext()

        if (hasnext) then
            call self%bucket%pair%copy(pair)
        end if
    end function iterator_pick

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Hashmap methods.
    !-------------------------------------------------------------------------------------------------------------------------------
    subroutine hashmap_construct(self, nbuckets, maxload)
        class(hashmap_class), intent(inout) :: self
        integer, intent(in), optional :: nbuckets
        real(kind=REAL64), intent(in), optional :: maxload

        self%npairs = 0

        if (associated(self%buckets)) then
            deallocate(self%buckets)
        end if

        if (present(nbuckets)) then
            self%nbuckets = nbuckets
        else
            self%nbuckets = 11
        end if

        if (present(maxload)) then
            self%maxload = maxload
        else
            self%maxload = 0.75
        end if

        allocate(self%buckets(self%nbuckets))

        call self%iterator%construct(self%buckets)
    end subroutine hashmap_construct

    elemental function hashmap_haskey(self, pair) result(haskey)
        class(hashmap_class), intent(in) :: self
        class(hashmap_pair), intent(in) :: pair
        logical :: haskey
        integer :: hash

        hash = mod(pair%hash(), self%nbuckets)+1

        haskey = self%buckets(hash)%haskey(pair)
    end function hashmap_haskey

    subroutine hashmap_remove(self, pair)
        class(hashmap_class), intent(inout) :: self
        class(hashmap_pair), intent(in) :: pair
        integer :: hash

        hash = mod(pair%hash(), self%nbuckets)+1

        if (self%buckets(hash)%remove(pair)) then
            self%npairs = self%npairs-1
        end if
    end subroutine hashmap_remove

    recursive subroutine hashmap_set(self, pair)
        class(hashmap_class), intent(inout) :: self
        class(hashmap_pair), intent(in) :: pair
        integer :: hash

        if (float(self%npairs)/float(self%nbuckets) >= self%maxload) then
            call self%resize()
        end if

        self%npairs = self%npairs+1

        hash = mod(pair%hash(), self%nbuckets)+1
        call self%buckets(hash)%set(pair)
    end subroutine hashmap_set

    function hashmap_find(self, key, content) result(success)
        class(hashmap_class), intent(in) :: self
        class(hashmap_pair), intent(in) :: key
        class(hashmap_pair), pointer, intent(out) :: content
        logical :: success
        integer :: hash

        hash = mod(key%hash(), self%nbuckets)+1

        success = self%buckets(hash)%find(key, content)
    end function hashmap_find

    function hashmap_pick_compact(self, pair) result(success)
        class(hashmap_class), intent(in) :: self
        class(hashmap_pair), intent(inout) :: pair
        logical :: success
        integer :: hash

        hash = mod(pair%hash(), self%nbuckets)+1

        success = self%buckets(hash)%pick(pair)
    end function hashmap_pick_compact

    function hashmap_pick_verbose(self, key, content) result(success)
        class(hashmap_class), intent(in) :: self
        class(hashmap_pair), intent(in) :: key
        class(*), intent(out) :: content
        logical :: success
        integer :: hash

        hash = mod(key%hash(), self%nbuckets)+1

        success = self%buckets(hash)%pick(key, content)
    end function hashmap_pick_verbose

    subroutine hashmap_resize(self)
        class(hashmap_class), target, intent(inout) :: self
        type(hashmap_bucket), pointer, dimension(:) :: buckets
        type(hashmap_bucket), pointer :: bucket
        integer :: hash
        integer :: i

        self%nbuckets = 2*self%nbuckets+1
        self%npairs = 0

        allocate(buckets(self%nbuckets))

        do i = 1, size(self%buckets)
            bucket => self%buckets(i)
            do while(associated(bucket))
                if (associated(bucket%pair)) then
                    hash = mod(bucket%pair%hash(), self%nbuckets)+1
                    call buckets(hash)%set(bucket%pair)
                end if
                bucket => bucket%next
            end do
        end do

        deallocate(self%buckets)
        self%buckets => buckets

        call self%iterator%construct(self%buckets)
    end subroutine hashmap_resize

    elemental subroutine hashmap_cleanup(self)
        type(hashmap_class), intent(inout) :: self

        if (associated(self%buckets)) then
            deallocate(self%buckets)
        end if
    end subroutine hashmap_cleanup
end module class_hashmap

! vim: set ft=fortran ff=unix tw=132:
