! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
! Description:
!
! Structured mesh.
!
module class_mesh_structured
    use iso_fortran_env, only : REAL64
    use class_exception
    use class_geometry
    use class_string
    use class_indexmap
    use class_datafmt_cgns
    use class_message_passing
    use class_configparser
    use class_paramesh
    implicit none
    private
    public :: mesh_node_structure
    public :: mesh_cell_structure
    public :: mesh_face_structure
    public :: mesh_boco_structure
    public :: mesh_connection_structure
    public :: mesh_collection_structure
    public :: mesh_zone_structure
    public :: mesh_structured_class

    integer :: nsockets
    integer :: nthreads
    integer :: inneromp

    type :: mesh_node_structure
        real(kind=REAL64), allocatable, dimension(:) :: centriod
    end type mesh_node_structure

    type :: mesh_cell_structure
        real(kind=REAL64) :: volume
        real(kind=REAL64), allocatable, dimension(:) :: centriod
        real(kind=REAL64), allocatable, dimension(:) :: distances ! Distances from cell center to face center.
    end type mesh_cell_structure

    type :: mesh_face_structure
        integer :: axis ! Indicator of the IJK axis this face perpendicular to.
        integer :: outward ! Indicator of positive or negative outward direction.
        real(kind=REAL64) :: area
        real(kind=REAL64), allocatable, dimension(:) :: centriod
        real(kind=REAL64), allocatable, dimension(:,:) :: transform ! Transformation matrix (norm, tangents).
    end type mesh_face_structure

    type :: mesh_boco_structure
        character(len=:), allocatable :: identifier
        integer, allocatable, dimension(:) :: ghostelem ! Index of ghost layers, ordered from those closest to boundary face.
        integer, allocatable, dimension(:) :: boundelem ! Index of boundary layer.
        integer, allocatable, dimension(:) :: innerelem ! Index of inner layers (should have same layers as halos, mirror).
    end type mesh_boco_structure

    type :: mesh_connection_structure
        integer, allocatable, dimension(:) :: ghostelem
        integer, allocatable, dimension(:) :: boundelem
        integer, allocatable, dimension(:) :: innerelem
        integer, allocatable, dimension(:) :: donorelem
        integer, allocatable, dimension(:,:) :: transform ! CGNS zone connectivity transformation matrix.
    end type mesh_connection_structure

    type, extends(cgns_collection_structure) :: mesh_collection_structure
        integer :: nelems ! Total number of elements to exchange (ghostelem/innerelem/donorelem).
        integer :: request(2) ! MPI send/recv status.
    end type mesh_collection_structure

    type :: mesh_zone_structure
        integer :: ncells
        integer, allocatable, dimension(:) :: zonesize
        class(mesh_node_structure), allocatable, dimension(:) :: nodes
        class(mesh_face_structure), allocatable, dimension(:) :: faces
        class(mesh_boco_structure), allocatable, dimension(:) :: bocos
        class(mesh_cell_structure), allocatable, dimension(:) :: cells ! Including halos.
        class(mesh_cell_structure), dimension(:), pointer :: halos => null()
        class(mesh_connection_structure), allocatable, dimension(:) :: connections
        class(mesh_collection_structure), allocatable, dimension(:) :: collections
    end type mesh_zone_structure

    type :: mesh_structured_class
        private
        integer :: nghosts
        integer, public :: nzones
        integer, public :: ndimensions
        type(cgns_reader_class), public :: cgns
        type(mesh_zone_structure), allocatable, dimension(:), public :: zones
    contains
        procedure :: construct
        procedure :: parse
        procedure, private :: parse_zone
    end type mesh_structured_class
contains
    subroutine construct(self, configparser)
        class(mesh_structured_class), intent(inout) :: self
        type(configparser_class), intent(in) :: configparser
        type(exception_class) :: exception
        type(paramesh_class) :: paramesh
        integer :: i

        call mpi%construct()
        nsockets = mpi%nsockets
        nthreads = mpi%nthreads
        inneromp = mpi%inneromp

        call self%cgns%construct(configparser)
        do i = 1, self%cgns%nzones
            if (self%cgns%zones(i)%zonetype .ne. STRUCTURED) then
                call exception%construct("The mesh should be structured.")
                call exception%raise()
            end if
        end do
        self%cgns%phys_dimension = self%cgns%cell_dimension ! 3D surface field is not supported yet.
        call self%cgns%parse()
        call paramesh%construct(self%cgns, configparser)

        self%nzones = size(self%cgns%zonelist)
        self%ndimensions = self%cgns%cell_dimension
    end subroutine construct

    subroutine parse(self, model, nghosts)
        class(mesh_structured_class), intent(inout) :: self
        type(mesh_zone_structure), intent(in), optional :: model
        integer, intent(in), optional :: nghosts
        type(mesh_zone_structure) :: model_selected
        real(kind=REAL64), allocatable, dimension(:,:,:) :: package_send
        real(kind=REAL64), allocatable, dimension(:,:,:) :: package_recv
        integer :: ncolls
        integer :: ncells
        integer :: slices
        integer :: ier
        integer :: i, k

        if (present(model)) then
            model_selected = model
        end if
        if (.not. allocated(model_selected%nodes)) allocate(mesh_node_structure :: model_selected%nodes(1))
        if (.not. allocated(model_selected%faces)) allocate(mesh_face_structure :: model_selected%faces(1))
        if (.not. allocated(model_selected%cells)) allocate(mesh_cell_structure :: model_selected%cells(1))
        if (.not. allocated(model_selected%bocos)) allocate(mesh_boco_structure :: model_selected%bocos(1))
        if (.not. allocated(model_selected%connections)) allocate(mesh_connection_structure :: model_selected%connections(1))
        if (.not. allocated(model_selected%collections)) allocate(mesh_collection_structure :: model_selected%collections(1))

        if (present(nghosts)) then
            self%nghosts = max(1,nghosts)
        else
            self%nghosts = 1
        end if

        deallocate(self%zones, stat=ier)
        allocate(self%zones(self%nzones))

        !$omp parallel do num_threads(nsockets)
        do i = 1, self%nzones
            call self%parse_zone(self%zones(i), self%cgns%zones(self%cgns%zonelist(i)), model_selected)
        end do
        !$omp end parallel do

        ncolls = 0
        ncells = 0
        slices = 1+2*self%ndimensions
        !$omp parallel do num_threads(nsockets) private(i,k) reduction(max:ncolls) reduction(max:ncells)
        do i = 1, self%nzones
            ncolls = max(ncolls, size(self%zones(i)%collections))
            do k = 1, size(self%zones(i)%collections)
                ncells = max(ncells, self%zones(i)%collections(k)%nelems)
            end do
        end do
        !$omp end parallel do
        allocate(package_send(slices*ncells, ncolls, self%nzones))
        allocate(package_recv(slices*ncells, ncolls, self%nzones))

        !$omp parallel num_threads(nsockets)
        !$omp do private(i,k)
        do i = 1, self%nzones
            do k = 1, size(self%zones(i)%collections)
            associate(coll => self%zones(i)%collections(k))
                if (coll%donorproc /= mpi%rank) then
                    call exchange_exter_send(coll, self%zones(i), package_send(:,k,i), package_recv(:,k,i))
                else
                    call exchange_inter(coll, self%zones(i), self%zones(coll%donorzone))
                end if
            end associate
            end do
        end do
        !$omp end do nowait

        !$omp do private(i,k)
        do i = 1, self%nzones
            do k = 1, size(self%zones(i)%collections)
            associate(coll => self%zones(i)%collections(k))
                if (coll%donorproc /= mpi%rank) then
                    call exchange_exter_recv(coll, self%zones(i), package_recv(:,k,i))
                end if
            end associate
            end do
        end do
        !$omp end do nowait
        !$omp end parallel
    contains
        subroutine exchange_exter_send(coll, zone, send, recv)
            type(mesh_collection_structure), intent(inout) :: coll
            type(mesh_zone_structure), intent(in) :: zone
            real(kind=REAL64), dimension(:), intent(inout) :: send
            real(kind=REAL64), dimension(:), intent(inout) :: recv
            integer :: sizes
            integer :: shift
            integer :: ier
            integer :: i, k

            sizes = slices*coll%nelems

            do i = 1, size(coll%connlist)
            associate(conn => zone%connections(coll%connlist(i)))
                do k = 1, size(conn%innerelem)
                    shift = (k-1)*slices
                    send(shift+1) = zone%cells(conn%innerelem(k))%volume
                    send(shift+2:shift+self%ndimensions+1) = zone%cells(conn%innerelem(k))%centriod
                    send(shift+self%ndimensions+2:shift+slices) = matmul(abs(conn%transform),&
                                                                         zone%cells(conn%innerelem(k))%distances)
                end do
            end associate
            end do

            call mpi_isend(send, sizes, MPI_REAL8, coll%donorproc-1, coll%donorzone, MPI_COMM_WORLD, coll%request(1), ier)
            call mpi_irecv(recv, sizes, MPI_REAL8, coll%donorproc-1, coll%localzone, MPI_COMM_WORLD, coll%request(2), ier)
        end subroutine exchange_exter_send

        subroutine exchange_exter_recv(coll, zone, recv)
            type(mesh_collection_structure), intent(in) :: coll
            type(mesh_zone_structure), intent(inout) :: zone
            real(kind=REAL64), dimension(:), intent(in) :: recv
            integer :: shift
            integer :: ier
            integer :: i, k

            call mpi_wait(coll%request(2), MPI_STATUS_IGNORE, ier)
            do i = 1, size(coll%connlist)
            associate(conn => zone%connections(coll%connlist(i)))
                do k = 1, size(conn%ghostelem)
                    shift = (k-1)*slices
                    zone%halos(conn%ghostelem(k))%volume = recv(shift+1)
                    zone%halos(conn%ghostelem(k))%centriod = recv(shift+2:shift+self%ndimensions+1)
                    zone%halos(conn%ghostelem(k))%distances = recv(shift+self%ndimensions+2:shift+slices)
                end do
            end associate
            end do
            call mpi_wait(coll%request(1), MPI_STATUS_IGNORE, ier)
        end subroutine exchange_exter_recv

        subroutine exchange_inter(coll, zone, donorzone)
            type(mesh_collection_structure), intent(in) :: coll
            type(mesh_zone_structure), intent(inout) :: zone
            type(mesh_zone_structure), intent(in) :: donorzone
            integer :: i, k

            do i = 1, size(coll%connlist)
            associate(conn => zone%connections(coll%connlist(i)))
                do k = 1, size(conn%ghostelem)
                    zone%halos(conn%ghostelem(k))%volume = donorzone%cells(conn%donorelem(k))%volume
                    zone%halos(conn%ghostelem(k))%centriod = donorzone%cells(conn%donorelem(k))%centriod
                    zone%halos(conn%ghostelem(k))%distances = matmul(donorzone%cells(conn%donorelem(k))%distances,&
                                                                     abs(conn%transform))
                end do
            end associate
            end do
        end subroutine exchange_inter
    end subroutine parse

    subroutine parse_zone(self, zone, cgns_zone, model)
        class(mesh_structured_class), intent(in) :: self
        type(mesh_zone_structure), target, intent(out) :: zone
        type(cgns_zone_structure), intent(in) :: cgns_zone
        type(mesh_zone_structure), intent(in) :: model
        type(exception_class) :: exception
        integer :: nfaces(self%ndimensions)
        integer :: nhalos(self%ndimensions)
        integer :: nnodes
        integer :: ncells
        integer :: nbocos
        integer :: nconns
        integer :: ncolls
        integer :: i, k

        zone%zonesize = cgns_zone%zonesize-1

        nnodes = product(zone%zonesize+1)
        ncells = product(zone%zonesize)
        nfaces = ncells+ncells/zone%zonesize
        nhalos = 2*self%nghosts*ncells/zone%zonesize
        nbocos = size(cgns_zone%bocos)
        nconns = size(cgns_zone%connections)
        ncolls = size(cgns_zone%collections)
        zone%ncells = ncells
        allocate(zone%nodes(nnodes), source=model%nodes(1))
        allocate(zone%cells(ncells+sum(nhalos)), source=model%cells(1))
        allocate(zone%bocos(nbocos), source=model%bocos(1))
        allocate(zone%faces(sum(nfaces)), source=model%faces(1))
        allocate(zone%connections(nconns), source=model%connections(1))
        allocate(zone%collections(ncolls), source=model%collections(1))
        zone%halos => zone%cells(ncells+1:)

        do i = 1, nbocos
            if ((cgns_zone%bocos(i)%location .ne. VERTEX) .or. (cgns_zone%bocos(i)%itemtype .ne. POINTRANGE)) then
                call exception%construct("Boundary condition should be defined in Vertex as PointRange.")
                call exception%raise()
            end if
            call parse_zone_boco(cgns_zone%bocos(i), zone%bocos(i))
        end do

        do i = 1, nconns
            call parse_zone_conn(cgns_zone%connections(i), zone%connections(i))
        end do

        do i = 1, ncolls
        associate(coll => zone%collections(i))
            coll%connlist = cgns_zone%collections(i)%connlist
            coll%localzone = cgns_zone%collections(i)%localzone
            coll%donorzone = cgns_zone%collections(i)%donorzone
            coll%donorproc = cgns_zone%collections(i)%donorproc
            coll%nelems = 0
            do k = 1, size(coll%connlist)
                coll%nelems = coll%nelems+size(zone%connections(coll%connlist(k))%ghostelem)
            end do
        end associate
        end do

        do i = 1, nnodes
            zone%nodes(i)%centriod = cgns_zone%coords(i,1:self%ndimensions)
        end do

        do i = 1, size(zone%cells)
            allocate(zone%cells(i)%centriod(self%ndimensions))
            allocate(zone%cells(i)%distances(self%ndimensions))
        end do

        do i = 1, sum(nfaces)
            allocate(zone%faces(i)%centriod(self%ndimensions))
            allocate(zone%faces(i)%transform(self%ndimensions,self%ndimensions))
        end do

        if (self%ndimensions .eq. 3) then
            call parse_zone_3D()
        else if (self%ndimensions .eq. 2) then
            call parse_zone_2D()
        else
            call parse_zone_1D()
        end if
    contains
        subroutine parse_zone_3D()
            class(mesh_node_structure), pointer, dimension(:,:,:) :: nodes
            class(mesh_cell_structure), pointer, dimension(:,:,:) :: cells
            class(mesh_face_structure), pointer, dimension(:,:,:) :: xfaces
            class(mesh_face_structure), pointer, dimension(:,:,:) :: yfaces
            class(mesh_face_structure), pointer, dimension(:,:,:) :: zfaces
            class(mesh_cell_structure), pointer, dimension(:,:,:) :: xhalos
            class(mesh_cell_structure), pointer, dimension(:,:,:) :: yhalos
            class(mesh_cell_structure), pointer, dimension(:,:,:) :: zhalos
            real(kind=REAL64) :: points(3,4)
            real(kind=REAL64) :: face_centriods(3,6)
            real(kind=REAL64) :: face_projections(3,6)
            integer :: i, j, k

            ! FIXME: consider data retrive efficiency ?
            cells(1:zone%zonesize(1), 1:zone%zonesize(2), 1:zone%zonesize(3)) => zone%cells(1:zone%ncells)
            nodes(1:zone%zonesize(1)+1, 1:zone%zonesize(2)+1, 1:zone%zonesize(3)+1) => zone%nodes
            xfaces(1:zone%zonesize(1)+1, 1:zone%zonesize(2), 1:zone%zonesize(3)) => zone%faces(1:nfaces(1))
            yfaces(1:zone%zonesize(1), 1:zone%zonesize(2)+1, 1:zone%zonesize(3)) => zone%faces(nfaces(1)+1:nfaces(1)+nfaces(2))
            zfaces(1:zone%zonesize(1), 1:zone%zonesize(2), 1:zone%zonesize(3)+1) => zone%faces(nfaces(1)+nfaces(2)+1:)
            xhalos(1:2*self%nghosts, 1:zone%zonesize(2), 1:zone%zonesize(3)) => zone%halos(1:nhalos(1))
            yhalos(1:zone%zonesize(1), 1:2*self%nghosts, 1:zone%zonesize(3)) => zone%halos(nhalos(1)+1:nhalos(1)+nhalos(2))
            zhalos(1:zone%zonesize(1), 1:zone%zonesize(2), 1:2*self%nghosts) => zone%halos(nhalos(1)+nhalos(2)+1:)

            !$omp parallel num_threads(inneromp)
            !$omp do private(i,j,k,points) collapse(3)
            do k = 1, zone%zonesize(3)
                do j = 1, zone%zonesize(2)
                    do i = 1, zone%zonesize(1)+1
                        points(:,1) = nodes(i,j,k)%centriod
                        points(:,2) = nodes(i,j+1,k)%centriod
                        points(:,3) = nodes(i,j+1,k+1)%centriod
                        points(:,4) = nodes(i,j,k+1)%centriod
                        call geometry%compute_quad(points, xfaces(i,j,k)%centriod, xfaces(i,j,k)%area, xfaces(i,j,k)%transform)
                        xfaces(i,j,k)%outward = sign(1, i-2)
                        xfaces(i,j,k)%axis = 1
                    end do
                end do
            end do
            !$omp end do nowait

            !$omp do private(i,j,k,points) collapse(3)
            do k = 1, zone%zonesize(3)
                do j = 1, zone%zonesize(2)+1
                    do i = 1, zone%zonesize(1)
                        points(:,1) = nodes(i,j,k)%centriod
                        points(:,2) = nodes(i,j,k+1)%centriod
                        points(:,3) = nodes(i+1,j,k+1)%centriod
                        points(:,4) = nodes(i+1,j,k)%centriod
                        call geometry%compute_quad(points, yfaces(i,j,k)%centriod, yfaces(i,j,k)%area, yfaces(i,j,k)%transform)
                        yfaces(i,j,k)%outward = sign(1, j-2)
                        yfaces(i,j,k)%axis = 2
                    end do
                end do
            end do
            !$omp end do nowait

            !$omp do private(i,j,k,points) collapse(3)
            do k = 1, zone%zonesize(3)+1
                do j = 1, zone%zonesize(2)
                    do i = 1, zone%zonesize(1)
                        points(:,1) = nodes(i,j,k)%centriod
                        points(:,2) = nodes(i+1,j,k)%centriod
                        points(:,3) = nodes(i+1,j+1,k)%centriod
                        points(:,4) = nodes(i,j+1,k)%centriod
                        call geometry%compute_quad(points, zfaces(i,j,k)%centriod, zfaces(i,j,k)%area, zfaces(i,j,k)%transform)
                        zfaces(i,j,k)%outward = sign(1, k-2)
                        zfaces(i,j,k)%axis = 3
                    end do
                end do
            end do
            !$omp end do

            !$omp do private(i,j,k,face_centriods,face_projections) collapse(3)
            do k = 1, zone%zonesize(3)
                do j = 1, zone%zonesize(2)
                    do i = 1, zone%zonesize(1)
                        face_centriods(:,1) = xfaces(i,j,k)%centriod
                        face_centriods(:,2) = xfaces(i+1,j,k)%centriod
                        face_centriods(:,3) = yfaces(i,j,k)%centriod
                        face_centriods(:,4) = yfaces(i,j+1,k)%centriod
                        face_centriods(:,5) = zfaces(i,j,k)%centriod
                        face_centriods(:,6) = zfaces(i,j,k+1)%centriod
                        face_projections(:,1) = -xfaces(i,j,k)%area*xfaces(i,j,k)%transform(:,1)
                        face_projections(:,2) = xfaces(i+1,j,k)%area*xfaces(i+1,j,k)%transform(:,1)
                        face_projections(:,3) = -yfaces(i,j,k)%area*yfaces(i,j,k)%transform(:,1)
                        face_projections(:,4) = yfaces(i,j+1,k)%area*yfaces(i,j+1,k)%transform(:,1)
                        face_projections(:,5) = -zfaces(i,j,k)%area*zfaces(i,j,k)%transform(:,1)
                        face_projections(:,6) = zfaces(i,j,k+1)%area*zfaces(i,j,k+1)%transform(:,1)
                        call geometry%compute_volume(face_centriods, face_projections, cells(i,j,k)%centriod, cells(i,j,k)%volume)
                        ! Better approches possible?
                        cells(i,j,k)%distances(1) = 0.5*(norm2(xfaces(i+1,j,k)%centriod-cells(i,j,k)%centriod)&
                                                        +norm2(cells(i,j,k)%centriod-xfaces(i,j,k)%centriod))
                        cells(i,j,k)%distances(2) = 0.5*(norm2(yfaces(i,j+1,k)%centriod-cells(i,j,k)%centriod)&
                                                        +norm2(cells(i,j,k)%centriod-yfaces(i,j,k)%centriod))
                        cells(i,j,k)%distances(3) = 0.5*(norm2(zfaces(i,j,k+1)%centriod-cells(i,j,k)%centriod)&
                                                        +norm2(cells(i,j,k)%centriod-zfaces(i,j,k)%centriod))
                    end do
                end do
            end do
            !$omp end do

            !$omp barrier
            !$omp do private(j,k) collapse(2)
            do k = 1, zone%zonesize(3)
                do j = 1, zone%zonesize(2)
                    call compute_ghost_cell(xhalos(self%nghosts:1:-1,j,k), cells(1,j,k), xfaces(1,j,k), 1, 1)
                    call compute_ghost_cell(xhalos(self%nghosts+1:,j,k), cells(zone%zonesize(1),j,k),&
                                            xfaces(zone%zonesize(1)+1,j,k), 1, -1)
                end do
            end do
            !$omp end do nowait

            !$omp do private(i,k) collapse(2)
            do k = 1, zone%zonesize(3)
                do i = 1, zone%zonesize(1)
                    call compute_ghost_cell(yhalos(i,self%nghosts:1:-1,k), cells(i,1,k), yfaces(i,1,k), 2, 1)
                    call compute_ghost_cell(yhalos(i,self%nghosts+1:,k), cells(i,zone%zonesize(2),k),&
                                            yfaces(i,zone%zonesize(2)+1,k), 2, -1)
                end do
            end do
            !$omp end do nowait

            !$omp do private(i,j) collapse(2)
            do j = 1, zone%zonesize(2)
                do i = 1, zone%zonesize(1)
                    call compute_ghost_cell(zhalos(i,j,self%nghosts:1:-1), cells(i,j,1), zfaces(i,j,1), 3, 1)
                    call compute_ghost_cell(zhalos(i,j,self%nghosts+1:), cells(i,j,zone%zonesize(3)),&
                                            zfaces(i,j,zone%zonesize(3)+1), 3, -1)
                end do
            end do
            !$omp end do nowait
            !$omp end parallel
        end subroutine parse_zone_3D

        subroutine parse_zone_2D()
            class(mesh_cell_structure), pointer, dimension(:,:) :: cells
            class(mesh_node_structure), pointer, dimension(:,:) :: nodes
            class(mesh_face_structure), pointer, dimension(:,:) :: xfaces
            class(mesh_face_structure), pointer, dimension(:,:) :: yfaces
            class(mesh_cell_structure), pointer, dimension(:,:) :: xhalos
            class(mesh_cell_structure), pointer, dimension(:,:) :: yhalos
            real(kind=REAL64) :: points(2,4)
            integer :: i, j

            cells(1:zone%zonesize(1), 1:zone%zonesize(2)) => zone%cells(1:zone%ncells)
            nodes(1:zone%zonesize(1)+1, 1:zone%zonesize(2)+1) => zone%nodes
            xfaces(1:zone%zonesize(1)+1, 1:zone%zonesize(2)) => zone%faces(1:nfaces(1))
            yfaces(1:zone%zonesize(1), 1:zone%zonesize(2)+1) => zone%faces(nfaces(1)+1:)
            xhalos(1:2*self%nghosts, 1:zone%zonesize(2)) => zone%halos(1:nhalos(1))
            yhalos(1:zone%zonesize(1), 1:2*self%nghosts) => zone%halos(nhalos(1)+1:)

            !$omp parallel num_threads(inneromp)
            !$omp do private(i,j,points) collapse(2)
            do j = 1, zone%zonesize(2)
                do i = 1, zone%zonesize(1)+1
                    points(:,1) = nodes(i,j)%centriod
                    points(:,2) = nodes(i,j+1)%centriod
                    call geometry%compute_line(points, xfaces(i,j)%centriod, xfaces(i,j)%area, xfaces(i,j)%transform)
                    xfaces(i,j)%outward = sign(1, i-2)
                    xfaces(i,j)%axis = 1
                end do
            end do
            !$omp end do nowait

            !$omp do private(i,j,points) collapse(2)
            do j = 1, zone%zonesize(2)+1
                do i = 1, zone%zonesize(1)
                    points(:,1) = nodes(i+1,j)%centriod
                    points(:,2) = nodes(i,j)%centriod
                    call geometry%compute_line(points, yfaces(i,j)%centriod, yfaces(i,j)%area, yfaces(i,j)%transform)
                    yfaces(i,j)%outward = sign(1, j-2)
                    yfaces(i,j)%axis = 1
                end do
            end do
            !$omp end do

            !$omp do private(i,j,points) collapse(2)
            do j = 1, zone%zonesize(2)
                do i = 1, zone%zonesize(1)
                    points(:,1) = nodes(i,j)%centriod
                    points(:,2) = nodes(i+1,j)%centriod
                    points(:,3) = nodes(i+1,j+1)%centriod
                    points(:,4) = nodes(i,j+1)%centriod
                    call geometry%compute_quad(points, cells(i,j)%centriod, cells(i,j)%volume)
                    ! Better approches possible?
                    cells(i,j)%distances(1) = 0.5*(norm2(xfaces(i+1,j)%centriod-cells(i,j)%centriod)&
                                                  +norm2(cells(i,j)%centriod-xfaces(i,j)%centriod))
                    cells(i,j)%distances(2) = 0.5*(norm2(yfaces(i,j+1)%centriod-cells(i,j)%centriod)&
                                                  +norm2(cells(i,j)%centriod-yfaces(i,j)%centriod))
                end do
            end do
            !$omp end do

            !$omp do
            do j = 1, zone%zonesize(2)
                call compute_ghost_cell(xhalos(self%nghosts:1:-1,j), cells(1,j), xfaces(1,j), 1, 1)
                call compute_ghost_cell(xhalos(self%nghosts+1:,j), cells(zone%zonesize(1),j), xfaces(zone%zonesize(1)+1,j), 1, -1)
            end do
            !$omp end do nowait

            !$omp do
            do i = 1, zone%zonesize(1)
                call compute_ghost_cell(yhalos(i,self%nghosts:1:-1), cells(i,1), yfaces(i,1), 2, 1)
                call compute_ghost_cell(yhalos(i,self%nghosts+1:), cells(i,zone%zonesize(2)), yfaces(i,zone%zonesize(2)+1), 2, -1)
            end do
            !$omp end do nowait
            !$omp end parallel
        end subroutine parse_zone_2D

        subroutine parse_zone_1D()
            integer :: i

            !$omp parallel num_threads(inneromp)
            !$omp do
            do i = 1, zone%zonesize(1)+1
                zone%faces(i)%centriod = zone%nodes(i)%centriod
                zone%faces(i)%area = 1.0
                zone%faces(i)%transform = 1.0
                zone%faces(i)%outward = sign(1, i-2)
                zone%faces(i)%axis = 1
            end do
            !$omp end do nowait

            !$omp do
            do i = 1, zone%zonesize(1)
                zone%cells(i)%centriod = 0.5*(zone%nodes(i)%centriod+zone%nodes(i+1)%centriod)
                zone%cells(i)%volume = norm2(zone%nodes(i+1)%centriod-zone%nodes(i)%centriod)
                zone%cells(i)%distances = 0.5*zone%cells(i)%volume
            end do
            !$omp end do nowait
            !$omp end parallel

            call compute_ghost_cell(zone%halos(self%nghosts:1:-1), zone%cells(1), zone%faces(1), 1, 1)
            call compute_ghost_cell(zone%halos(self%nghosts+1:), zone%cells(zone%zonesize(1)), zone%faces(zone%zonesize(1)+1),&
                                    1, -1)
        end subroutine parse_zone_1D

        subroutine parse_zone_boco(cgns_boco, boco)
            type(cgns_boco_structure), intent(in) :: cgns_boco
            class(mesh_boco_structure), intent(out) :: boco
            integer, allocatable, dimension(:) :: ghostelem
            integer, allocatable, dimension(:) :: boundelem
            integer, allocatable, dimension(:) :: innerelem

            call compute_index(cgns_boco%scope, zone%zonesize, ghostelem, boundelem, innerelem)
            boco%identifier = cgns_boco%identifier
            boco%ghostelem = ghostelem
            boco%boundelem = boundelem
            boco%innerelem = innerelem
        end subroutine parse_zone_boco

        subroutine parse_zone_conn(cgns_conn, conn)
            type(cgns_connection_structure), intent(in) :: cgns_conn
            class(mesh_connection_structure), intent(out) :: conn
            integer, allocatable, dimension(:) :: ghostelem
            integer, allocatable, dimension(:) :: boundelem
            integer, allocatable, dimension(:) :: innerelem

            conn%transform = cgns_conn%transform

            call compute_index(cgns_conn%localscope, zone%zonesize, ghostelem, boundelem, innerelem)
            conn%ghostelem = ghostelem
            conn%boundelem = boundelem
            conn%innerelem = innerelem

            call compute_index(cgns_conn%donorscope, cgns_conn%donorsize-1, ghostelem, boundelem, innerelem)
            conn%donorelem = innerelem
        end subroutine parse_zone_conn

        pure subroutine compute_index(scope_node, zonesize, ghostelem, boundelem, innerelem)
            integer, dimension(:,:), intent(in) :: scope_node
            integer, dimension(:), intent(in) :: zonesize
            integer, allocatable, dimension(:), intent(out) :: ghostelem
            integer, allocatable, dimension(:), intent(out) :: boundelem
            integer, allocatable, dimension(:), intent(out) :: innerelem
            integer :: scope_halo(self%ndimensions,2)
            integer :: scope_cell(self%ndimensions,2)
            integer :: scope_face(self%ndimensions,2)
            integer :: sizes_halo(self%ndimensions)
            integer :: sizes_face(self%ndimensions)
            integer :: slice_edge(self%ndimensions)
            integer :: slice_face(self%ndimensions)
            integer :: index_edge(self%ndimensions)
            integer :: index_halo(self%ndimensions)
            integer :: index_cell(self%ndimensions)
            integer :: index_face(self%ndimensions)
            integer :: nhalos(self%ndimensions)
            integer :: nfaces(self%ndimensions)
            integer :: ncells
            integer :: axis
            integer :: i

            ! The following code eliminated different number of dimensions and different looping of x/y/z planes, and store all
            ! indexes from those cloest to face.
            ! Example: scope_node is [1,5,6] - [3,5,3], and zonesize = [NX,NY,NZ] (boundary perpendicular to y)
            !   1. compute_scope: node->cell index convertion. Since there are multiple ghost layers (and inner mirror layer), the
            !                     y dimension is also modified to reflect the starting index and ending index of ghost layers
            !                     (always from small to large value, and is negative if the direction is negative in increasing
            !                     direction of xyz).
            !                     scope_halo: [1,NGHOST+1,5] - [2,2*NGHOST,3]
            !                     scope_cell: [1,-NX,5] - [2,-NX+NGHOST-1,3]
            !                     scope_face: [1,5,5] - [2,5,3]
            !   2. sizes_halo = [NX,2*NGHOST,NY]; sizes_face = [NX,NY+1,NZ]. To encode index of yhalos and yfaces.
            !   3. slice_edge = [2,NGHOST,3], then shifted to [NGHOST,2,3]. To decode index when looping.
            !   4. When looping halo/cells, they are starting from those cloest to face, so index_edge is [y,x,z]. It then shifted
            !      to [x,y,z] for computing compressed index.
            !   5. When compute the mirror inner cells, it's possible that NX < NGHOST, and the 'inner cell' actually goes to halo
            !      cell in the other side. This is detected and handled seperately.

            ncells = product(zonesize)
            nhalos = 2*self%nghosts*ncells/zonesize
            nfaces = ncells+ncells/zonesize

            call compute_scope(scope_node, scope_halo, scope_cell, scope_face, axis)

            sizes_halo = [(merge(2*self%nghosts, zonesize(i), i==axis), i=1, self%ndimensions)]
            slice_edge = abs(scope_halo(:,2)-scope_halo(:,1))+1
            slice_edge(1:) = cshift(slice_edge(1:), axis-1)
            slice_edge(2:) = cshift(slice_edge(2:), axis-1)
            allocate(ghostelem(product(slice_edge)))
            allocate(innerelem(product(slice_edge)))
            do i = 1, size(ghostelem)
                index_edge = indexmap%decode(slice_edge, i)
                index_edge(2:) = cshift(index_edge(2:), -axis+1)
                index_edge(1:) = cshift(index_edge(1:), -axis+1)

                index_halo = abs(index_edge+scope_halo(:,1)-1)
                index_cell = abs(index_edge+scope_cell(:,1)-1)

                ghostelem(i) = indexmap%encode(sizes_halo, index_halo)+sum(nhalos(:axis-1))
                if (index_cell(axis) > zonesize(axis) .or. index_cell(axis) < 1) then ! Overflow to ghost cell.
                    index_cell(axis) = index_cell(axis)+scope_cell(axis,2)+merge(0,1,index_cell(axis)<1)
                    innerelem(i) = ncells+indexmap%encode(sizes_halo, index_cell)+sum(nhalos(:axis-1))
                else ! Normal innder cell
                    innerelem(i) = indexmap%encode(zonesize, index_cell)
                end if
            end do

            sizes_face = [(merge(zonesize(i)+1, zonesize(i), i==axis), i=1, self%ndimensions)]
            slice_face = abs(scope_face(:,2)-scope_face(:,1))+1
            allocate(boundelem(product(slice_face)))
            do i = 1, size(boundelem)
                index_face = abs(indexmap%decode(slice_face, i)+scope_face(:,1)-1)
                boundelem(i) = indexmap%encode(sizes_face, index_face)+sum(nfaces(:axis-1))
            end do
        end subroutine compute_index

        pure subroutine compute_scope(scope_node, scope_halo, scope_cell, scope_face, axis)
            integer, dimension(:,:), intent(in) :: scope_node
            integer, dimension(:,:), intent(out) :: scope_halo
            integer, dimension(:,:), intent(out) :: scope_cell
            integer, dimension(:,:), intent(out) :: scope_face
            integer, intent(out) :: axis
            integer :: i

            ! Example: cgns_boco%scope is [1,5,6] - [3,5,3], and zonesize = [NX,NY,NZ] (boundary perpendicular to y)
            !          scope_halo: [1,NGHOST+1,5] - [2,2*NGHOST,3]
            !          scope_cell: [1,-NX,5] - [2,-NX+NGHOST-1,3]
            !          scope_face: [1,5,5] - [2,5,3]
            do i = 1, self%ndimensions
                if (scope_node(i,2) .ne. scope_node(i,1)) then ! Dimension within the boundary plane.
                    if (scope_node(i,2) > scope_node(i,1)) then
                        scope_halo(i,:) = [scope_node(i,1), scope_node(i,2)-1]
                    else
                        scope_halo(i,:) = [scope_node(i,1)-1, scope_node(i,2)]
                    end if
                    scope_cell(i,:) = scope_halo(i,:)
                    scope_face(i,:) = scope_halo(i,:)
                else ! Dimension of the boundary plane perpendicular to.
                    axis = i ! Record this axis.
                    if (scope_node(i,1) == 1) then ! Left boundary.
                        scope_halo(i,:) = -[self%nghosts, 1] ! NGHOST -> 1
                        scope_cell(i,:) = +[1, self%nghosts] ! 1 -> NGHOST
                    else
                        scope_halo(i,:) = +[self%nghosts+1, 2*self%nghosts] ! NGHOST+1 -> 2*NGHOST
                        scope_cell(i,:) = -[scope_node(i,1)-1, scope_node(i,1)-self%nghosts] ! NCELL -> NCELL-NGHOST+1
                    end if
                    scope_face(i,:) = scope_node(i,:)
                end if
            end do
        end subroutine compute_scope

        subroutine compute_ghost_cell(ghost_cells, cell, face, axis, direction)
            class(mesh_cell_structure), dimension(:), intent(inout) :: ghost_cells
            class(mesh_cell_structure), intent(in) :: cell
            class(mesh_face_structure), intent(in) :: face
            integer, intent(in) :: axis ! IJK axis.
            integer, intent(in) :: direction ! Left or right direction.
            integer :: i

            do i = 1, self%nghosts
                ghost_cells(i)%centriod = face%centriod-direction*(2*i-1)*cell%distances(axis)*face%transform(:,1)
                ghost_cells(i)%distances = cell%distances
            end do
            ghost_cells%volume = cell%volume
        end subroutine compute_ghost_cell
    end subroutine parse_zone
end module class_mesh_structured

! vim: set ft=fortran ff=unix tw=132:
