! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
! Description:
!
! CGNS data format.
!
module class_datafmt_cgns
    use iso_fortran_env, only : REAL32, REAL64, INT32, INT64
    use class_string
    use class_hashmap
    use class_indexmap
    use class_exception
    use class_datafmt_csv
    use class_message_passing
    use class_configparser
    use class_system
    use cgns, cnull => null
    implicit none
    private
    public :: STRUCTURED
    public :: CELLCENTER, VERTEX
    public :: POINTRANGE
    public :: cgns_zone_structure
    public :: cgns_boco_structure
    public :: cgns_section_structure
    public :: cgns_connection_structure
    public :: cgns_collection_structure
    public :: cgns_reader_class
    public :: cgns_writer_class

    integer, parameter :: index_base = 1 ! Always assume one base.

    !-------------------------------------------------------------------------------------------------------------------------------
    ! CGNS structure.
    !-------------------------------------------------------------------------------------------------------------------------------
    type :: cgns_section_structure
        integer :: element_type
        integer :: scope(2)
        integer, allocatable, dimension(:,:) :: elements ! Elements -> nodes connection information.
    end type cgns_section_structure

    type :: cgns_boco_structure
        character(len=:), allocatable :: identifier
        integer :: itemtype
        integer :: location
        integer, allocatable, dimension(:,:) :: scope
    end type cgns_boco_structure

    ! One-to-one connection only. Need more samples to implement generic connection.
    type :: cgns_connection_structure
        character(len=:), allocatable :: donorname
        integer :: donorzone ! Index of donorzone in global view.
        integer, allocatable, dimension(:) :: donorsize
        integer, allocatable, dimension(:,:) :: transform ! Transformation matrix.
        integer, allocatable, dimension(:,:) :: localscope
        integer, allocatable, dimension(:,:) :: donorscope
    end type cgns_connection_structure

    ! Connections group by donorzone (for unique tag in MPI).
    type :: cgns_collection_structure
        integer :: localzone ! Index of localzone in global view.
        integer :: donorzone ! Index of donorzone in global view.
        integer :: donorproc ! Processor the donorzone belongs to. For MPI/Coarray.
        integer, allocatable, dimension(:) :: connlist ! List of connection indexes.
    end type cgns_collection_structure

    type, extends(hashmap_pair) :: cgns_zone_structure
        integer :: zonetype
        integer :: zonemark ! Index of zone in global view.
        integer :: zoneproc ! Index of processor.
        integer, allocatable, dimension(:) :: zonesize
        character(len=:), allocatable :: zonename
        real(kind=REAL64), allocatable, dimension(:,:) :: coords
        type(cgns_section_structure), allocatable, dimension(:) :: sections
        type(cgns_boco_structure), allocatable, dimension(:) :: bocos
        type(cgns_connection_structure), allocatable, dimension(:) :: connections
        type(cgns_collection_structure), allocatable, dimension(:) :: collections
    contains
        procedure :: has_samekey ! Compare whether keys are equal.
        procedure :: copy ! Copy the value.
        procedure :: hash
    end type cgns_zone_structure

    !-------------------------------------------------------------------------------------------------------------------------------
    ! CGNS reader and writer class.
    !-------------------------------------------------------------------------------------------------------------------------------
    type :: cgns_reader_class
        private
        logical, public :: parainit ! Whether initialized in parallel.
        integer, allocatable :: funit
        integer, public :: nzones
        integer, public :: phys_dimension
        integer, public :: cell_dimension
        integer, allocatable, dimension(:), public :: zonelist ! Global index of zones belongs to local processor.
        character(len=:), allocatable :: infofile ! Paramesh information file.
        type(cgns_zone_structure), allocatable, dimension(:), public :: zones
    contains
        generic :: construct => construct_common, construct_extern, construct_simple
        procedure, private :: construct_common => reader_construct_common
        procedure, private :: construct_extern => reader_construct_extern ! Construct from external file.
        procedure, private :: construct_simple => reader_construct_simple ! Construct from simple inputs.
        procedure :: parse => reader_parse
        final :: reader_cleanup
    end type cgns_reader_class

    type :: cgns_writer_class
        private
        integer, allocatable :: funit
        type(cgns_reader_class), pointer :: reader => null()
    contains
        procedure :: construct => writer_construct
        procedure :: create_solution => writer_create_solution
        procedure :: output_solution => writer_output_solution
        final :: writer_cleanup
    end type cgns_writer_class
contains
    !-------------------------------------------------------------------------------------------------------------------------------
    ! CGNS hashmap.
    !-------------------------------------------------------------------------------------------------------------------------------
    elemental function has_samekey(self, pair)
        class(cgns_zone_structure), intent(in) :: self
        class(hashmap_pair), intent(in) :: pair
        logical :: has_samekey

        has_samekey = .false.

        select type(pair)
        class is(cgns_zone_structure)
            has_samekey = pair%zonename .eq. self%zonename
        end select
    end function has_samekey

    subroutine copy(self, object)
        class(cgns_zone_structure), intent(in) :: self
        class(*), intent(out) :: object

        select type(object)
        type is(cgns_zone_structure)
            object = self
        end select
    end subroutine copy

    elemental function hash(self)
        class(cgns_zone_structure), intent(in) :: self
        integer :: hash

        hash = str%hash(self%zonename)
    end function hash

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Reader class.
    !-------------------------------------------------------------------------------------------------------------------------------
    subroutine reader_construct_common(self, configparser)
        class(cgns_reader_class), intent(inout) :: self
        type(configparser_class), intent(in) :: configparser
        type(exception_class) :: exception
        type(string) :: dirname
        type(string) :: meshfile
        type(string) :: infofile
        real(kind=REAL64), allocatable, dimension(:) :: geometry
        integer, allocatable, dimension(:) :: numcells
        integer :: celldims
        integer :: stat(5)

        call configparser%get("main.dirname", dirname, ".")
        call configparser%get("cgns_reader.meshfile", meshfile, stat=stat(1))
        call configparser%get("cgns_reader.infofile", infofile, stat=stat(2))
        call configparser%get("cgns_reader.celldims", celldims, stat=stat(3))
        if (stat(3) <= 0) then
            allocate(geometry(celldims))
            allocate(numcells(celldims))
        end if
        call configparser%get("cgns_reader.geometry", geometry, stat=stat(4))
        call configparser%get("cgns_reader.numcells", numcells, stat=stat(5))

        if (all(stat(1:2) <= 0)) then
            call self%construct_extern(system%path%join([dirname, meshfile]), system%path%join([dirname, infofile]))
        else if (stat(1) <= 0 .and. stat(2) > 0) then
            call self%construct_extern(system%path%join([dirname, meshfile]))
        else if (all(stat(4:5) <= 0)) then
            call self%construct_simple(geometry, numcells+1)
        else
            call exception%construct("No mesh is given.")
            call exception%raise()
        end if
    end subroutine reader_construct_common

    subroutine reader_construct_extern(self, meshfile, infofile)
        class(cgns_reader_class), intent(inout) :: self
        character(len=*), intent(in) :: meshfile
        character(len=*), intent(in), optional :: infofile
        type(exception_class) :: exception
        character(len=128) :: basename ! Dummy.
        integer :: ier ! Error code.
        integer :: i

        call reader_cleanup(self)
        call mpi%construct()

        allocate(self%funit)
        call exception%construct("Failed to open file '"//meshfile//"'.")
        call cgp_open_f(meshfile, CG_MODE_READ, self%funit, exception%stat)
        call exception%raise()
        call cg_base_read_f(self%funit, index_base, basename, self%cell_dimension, self%phys_dimension, ier)

        if (present(infofile)) then
            self%infofile = infofile
            self%parainit = .true.
        end if

        call cg_nzones_f(self%funit, index_base, self%nzones, ier)
        allocate(self%zones(self%nzones))
        do i = 1, self%nzones
            call cg_zone_type_f(self%funit, index_base, i, self%zones(i)%zonetype, ier)
        end do
    end subroutine reader_construct_extern

    subroutine reader_construct_simple(self, geometry, zonesize)
        class(cgns_reader_class), intent(inout) :: self
        real(kind=REAL64), dimension(:), intent(in) :: geometry
        integer, dimension(:), intent(in) :: zonesize
        type(string), allocatable, dimension(:) :: identifiers
        real(kind=REAL64), allocatable, dimension(:) :: delta
        integer, allocatable, dimension(:) :: idx
        integer :: nnodes
        integer :: nbocos
        integer :: i, k

        call reader_cleanup(self)
        call mpi%construct()

        self%nzones = 1
        self%phys_dimension = size(zonesize)
        self%cell_dimension = size(zonesize)

        delta = geometry/(zonesize-1)
        nnodes = product(zonesize)
        nbocos = 2*self%cell_dimension

        allocate(self%zones(self%nzones))
        allocate(self%zones(1)%coords(nnodes,self%cell_dimension))
        allocate(self%zones(1)%bocos(nbocos))
        allocate(self%zones(1)%sections(0))
        allocate(self%zones(1)%connections(0))
        allocate(self%zones(1)%collections(0))
        self%zones(1)%zonetype = STRUCTURED
        self%zones(1)%zonesize = zonesize
        self%zones(1)%zonename = "zone-1"

        do i = 1, nnodes
            idx = indexmap%decode(zonesize, i)
            self%zones(1)%coords(i,:) = (idx-1)*delta
        end do

        identifiers = [string("west"), string("east"), string("south"), string("north"), string("lower"), string("upper")]
        do i = 1, nbocos
        associate(boco => self%zones(1)%bocos(i))
            boco%identifier = identifiers(i)
            boco%location = VERTEX
            boco%itemtype = POINTRANGE
            allocate(boco%scope(self%cell_dimension,2))
            boco%scope(:,1) = [(merge(zonesize(k),1,i==2*k+0), k=1,self%cell_dimension)]
            boco%scope(:,2) = [(merge(1,zonesize(k),i==2*k-1), k=1,self%cell_dimension)]
        end associate
        end do

        self%zonelist = [1]
    end subroutine reader_construct_simple

    subroutine reader_parse(self)
        class(cgns_reader_class), intent(inout) :: self
        type(hashmap_class) :: zonemap
        integer :: ier
        integer :: i

        if (.not. allocated(self%funit)) return

        call parse_common() ! Read general information, initialize zonemap and zonelist.

        ! Parse structure information for all zones.
        do i = 1, self%nzones
            call parse_zone_meta(self%zones(i), i)
        end do

        ! Parse zone data for zones in zonelist.
        do i = 1, size(self%zonelist)
            call parse_zone_data(self%zones(self%zonelist(i)), self%zonelist(i))
        end do

        call cgp_close_f(self%funit, ier)
        deallocate(self%funit)
    contains
        subroutine parse_common()
            type(cgns_zone_structure) :: zoneinfo
            type(exception_class) :: exception
            type(csv_reader_class) :: csv
            character(len=128) :: zonename
            logical :: success
            integer :: zonesize(9)
            integer :: i, k

            ! Hashmap of zone information for search.
            call zonemap%construct()

            ! Initialize zone information, zonemap, zonelist without considering MPI information.
            allocate(self%zonelist(self%nzones))
            do i = 1, self%nzones
                call cg_zone_read_f(self%funit, index_base, i, zonename, zonesize, ier)
                self%zones(i)%zonename = str%strip(zonename)
                select case(self%zones(i)%zonetype)
                case(STRUCTURED)
                    self%zones(i)%zonesize = zonesize(1:self%cell_dimension)
                case(UNSTRUCTURED)
                    self%zones(i)%zonesize = zonesize(1:3)
                end select
                self%zones(i)%zoneproc = mpi%rank
                self%zones(i)%zonemark = i
                call zonemap%set(self%zones(i))
                self%zonelist(i) = i
            end do

            ! Read partition information and correct zone information, zonemap if needed.
            if (allocated(self%infofile)) then
                call csv%construct(self%infofile)
                if (size(csv%lines) .ne. mpi%nprocs) then
                    call exception%construct("Partition information donesn't equal to the nprocs")
                    call exception%raise()
                end if

                ! Correct zone information and zonemap.
                do i = 1, mpi%nprocs
                associate(line => csv%lines(i)%line)
                    do k = 1, size(line(3:))
                        zoneinfo%zonename = line(k+2)
                        success = zonemap%pick(zoneinfo)
                        zoneinfo%zoneproc = i
                        self%zones(zoneinfo%zonemark)%zoneproc = i
                        call zonemap%set(zoneinfo)
                    end do
                end associate
                end do

                ! Correct zonelist.
                associate(line => csv%lines(mpi%rank)%line(3:))
                deallocate(self%zonelist, stat=ier)
                allocate(self%zonelist(size(line)))
                do i = 1, size(self%zonelist)
                    zoneinfo%zonename = line(i)
                    success = zonemap%pick(zoneinfo)
                    self%zonelist(i) = zoneinfo%zonemark
                end do
                end associate
            end if
        end subroutine parse_common

        subroutine parse_zone_meta(zone, index_zone)
            type(cgns_zone_structure), intent(inout) :: zone
            integer, intent(in) :: index_zone
            type(cgns_zone_structure) :: zoneinfo
            type(exception_class) :: exception
            character(len=128) :: sectionname ! Dummy.
            character(len=128) :: connectname ! Dummy.
            character(len=128) :: donorname
            character(len=128) :: boconame
            logical :: success
            integer, allocatable, dimension(:) :: connmarks
            integer :: transform(3)
            integer :: norm_index(3) ! Dummy.
            integer :: norm_sizes ! Dummy.
            integer :: norm_dtype ! Dummy.
            integer :: bocotype ! Dummy.
            integer :: ndataset ! Dummy.
            integer :: hasparent ! Dummy.
            integer :: nbndry ! Dummy.
            integer :: index_dimension
            integer :: nsections
            integer :: npoints
            integer :: nbocos
            integer :: nconns
            integer :: ier ! Error code.
            integer :: i, k

            call cg_index_dim_f(self%funit, index_base, index_zone, index_dimension, ier)

            call cg_nsections_f(self%funit, index_base, index_zone, nsections, ier)
            allocate(zone%sections(nsections))
            do i = 1, nsections
            associate(section => zone%sections(i))
                call cg_section_read_f(self%funit, index_base, index_zone, i, sectionname, section%element_type,&
                                       section%scope(1), section%scope(2), nbndry, hasparent, ier)
            end associate
            end do

            ! Different software may have different ways to write boundary condition, current implementation only tested for Pointwise.
            call cg_nbocos_f(self%funit, index_base, index_zone, nbocos, ier)
            allocate(zone%bocos(nbocos))
            do i = 1, nbocos
            associate(boco => zone%bocos(i))
                call cg_boco_info_f(self%funit, index_base, index_zone, i, boconame, bocotype, boco%itemtype, npoints, norm_index,&
                    norm_sizes, norm_dtype, ndataset, ier)
                boco%identifier = str%strip(boconame, "1234567890") ! Some software may need to get family name.

                allocate(boco%scope(index_dimension, npoints))
                call cg_boco_read_f(self%funit, index_base, index_zone, i, boco%scope, cnull, ier)
                call cg_boco_gridlocation_read_f(self%funit, index_base, index_zone, i, boco%location, ier)
            end associate
            end do

            ! Markers for gathering connections.
            allocate(connmarks(self%nzones))
            connmarks = 0

            ! One-to-One connection. Need more samples for generic connection.
            call cg_nconns_f(self%funit, index_base, index_zone, nconns, ier)
            if (nconns .ne. 0) then
                call exception%construct("CGNS generic grid conectivity is not supported yet.")
                call exception%raise()
            end if
            call cg_n1to1_f(self%funit, index_base, index_zone, nconns, ier)
            allocate(zone%connections(nconns))
            do i = 1, nconns
            associate(connection => zone%connections(i))
                allocate(connection%localscope(index_dimension, 2))
                allocate(connection%donorscope(index_dimension, 2))
                call cg_1to1_read_f(self%funit, index_base, index_zone, i, connectname, donorname, connection%localscope,&
                    connection%donorscope, transform, ier)
                connection%donorname = str%strip(donorname)
                connection%transform = decode_transform(transform(1:index_dimension))
                ! Donorzone information.
                zoneinfo%zonename = str%strip(donorname)
                success = zonemap%pick(zoneinfo)
                connection%donorzone = zoneinfo%zonemark
                connection%donorsize = zoneinfo%zonesize
                ! Mark this connection.
                connmarks(connection%donorzone) = connection%donorzone
                ! Scope ordering.
                call scope_ordering(connection%localscope, connection%donorscope, index_zone < zoneinfo%zonemark)
            end associate
            end do

            ! Connection collections.
            connmarks = pack(connmarks, connmarks/=0)
            allocate(zone%collections(size(connmarks)))
            do i = 1, size(connmarks)
                zone%collections(i)%connlist = pack([(k, k=1, nconns)], zone%connections%donorzone==connmarks(i))
                zone%collections(i)%donorzone = connmarks(i)
                zone%collections(i)%localzone = index_zone
                zone%collections(i)%donorproc = self%zones(connmarks(i))%zoneproc
            end do
        end subroutine parse_zone_meta

        subroutine parse_zone_data(zone, index_zone)
            type(cgns_zone_structure), intent(inout) :: zone
            integer, intent(in) :: index_zone
            character(len=128) :: coordname
            integer, allocatable, dimension(:) :: range_min
            integer, allocatable, dimension(:) :: range_max
            integer :: datatype ! Dummy.
            integer :: index_dimension
            integer :: nnodes
            integer :: ier ! Error code.
            integer :: i

            call cg_index_dim_f(self%funit, index_base, index_zone, index_dimension, ier)

            allocate(range_min(index_dimension))
            allocate(range_max(index_dimension))
            range_min = 1
            range_max = zone%zonesize(1:index_dimension)
            allocate(zone%coords(product(range_max), self%phys_dimension))
            do i = 1, self%phys_dimension
                call cg_coord_info_f(self%funit, index_base, index_zone, i, datatype, coordname, ier)
                call cg_coord_read_f(self%funit, index_base, index_zone, str%strip(coordname), REALDOUBLE, range_min,&
                                     range_max, zone%coords(:,i), ier)
            end do

            do i = 1, size(zone%sections)
            associate(section => zone%sections(i))
                call cg_npe_f(section%element_type, nnodes, ier)
                allocate(section%elements(nnodes, section%scope(2)-section%scope(1)+1))
                call cg_elements_read_f(self%funit, index_base, index_zone, i, section%elements, cnull, ier)
            end associate
            end do
        end subroutine parse_zone_data

        subroutine scope_ordering(localscope, donorscope, compare)
            integer, dimension(:,:), intent(inout) :: localscope
            integer, dimension(:,:), intent(inout) :: donorscope
            logical, intent(in) :: compare
            integer, allocatable, dimension(:) :: scope
            if ((sum(localscope(:,1)) > sum(localscope(:,2)) .and. compare) .or. &
                (sum(donorscope(:,1)) > sum(donorscope(:,2)) .and. (.not. compare))) then
                scope = localscope(:,1)
                localscope(:,1) = localscope(:,2)
                localscope(:,2) = scope
                scope = donorscope(:,1)
                donorscope(:,1) = donorscope(:,2)
                donorscope(:,2) = scope
            end if
        end subroutine scope_ordering
    end subroutine reader_parse

    elemental impure subroutine reader_cleanup(self)
        type(cgns_reader_class), intent(inout) :: self
        integer :: ier

        self%parainit = .false.

        if (allocated(self%funit)) then
            call cgp_close_f(self%funit, ier)
            deallocate(self%funit)
        end if

        deallocate(self%infofile, stat=ier)
        deallocate(self%zonelist, stat=ier)
        deallocate(self%zones, stat=ier)
    end subroutine reader_cleanup

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Writer class.
    !-------------------------------------------------------------------------------------------------------------------------------
    subroutine writer_construct(self, filename, reader)
        class(cgns_writer_class), intent(inout) :: self
        character(len=*), intent(in) :: filename
        type(cgns_reader_class), intent(in), target :: reader
        type(exception_class) :: exception
        integer :: index_base
        integer :: ier ! Error code.
        integer :: i

        call writer_cleanup(self)
        call mpi%construct()

        self%reader => reader

        allocate(self%funit)
        call exception%construct("Failed to open file '"//filename//"'.")
        call cgp_open_f(filename, CG_MODE_WRITE, self%funit, exception%stat)
        call exception%raise()

        ! Output structure of all zones.
        call cg_base_write_f(self%funit, "base-1", reader%cell_dimension, reader%phys_dimension, index_base, ier)
        do i = 1, reader%nzones
            call output_zone_meta(reader%zones(i))
        end do

        ! Output data for zones in zonelist.
        do i = 1, size(reader%zonelist)
            call output_zone_data(reader%zones(reader%zonelist(i)), reader%zonelist(i))
        end do
    contains
        subroutine output_zone_meta(zone)
            type(cgns_zone_structure), intent(in) :: zone
            character(len=128) :: coordnames(3)
            character(len=128) :: sectionname
            character(len=128) :: connectname
            integer, allocatable, dimension(:) :: zonesize
            integer :: index_zone
            integer :: index_boco
            integer :: index_item ! Dummy.
            integer :: i

            select case(zone%zonetype)
            case(STRUCTURED)
                deallocate(zonesize, stat=ier)
                allocate(zonesize(3*reader%cell_dimension))
                zonesize = 0
                zonesize(1:reader%cell_dimension) = zone%zonesize
                zonesize(reader%cell_dimension+1:reader%cell_dimension+reader%cell_dimension) = zone%zonesize-1
            case(UNSTRUCTURED)
                zonesize = zone%zonesize
            end select
            call cg_zone_write_f(self%funit, index_base, zone%zonename, zonesize, zone%zonetype, index_zone, ier)

            coordnames = ["CoordinateX", "CoordinateY", "CoordinateZ"]
            do i = 1, reader%phys_dimension
                call cgp_coord_write_f(self%funit, index_base, index_zone, REALDOUBLE, str%strip(coordnames(i)), index_item, ier)
            end do

            do i = 1, size(zone%sections)
            associate(section => zone%sections(i))
                write(sectionname, "(A,I0)") "section-", i
                call cgp_section_write_f(self%funit, index_base, index_zone, sectionname, section%element_type,&
                    section%scope(1), section%scope(2), 0, index_item, ier)
            end associate
            end do

            do i = 1, size(zone%bocos)
            associate(boco => zone%bocos(i))
                call cg_boco_write_f(self%funit, index_base, index_zone, boco%identifier, BCGENERAL, boco%itemtype,&
                                     size(boco%scope,2), boco%scope, index_boco, ier)
                call cg_boco_gridlocation_write_f(self%funit, index_base, index_zone, index_boco, boco%location, ier)
            end associate
            end do

            do i = 1, size(zone%connections)
            associate(conn => zone%connections(i))
                write(connectname, "(A,I0)") "connection-", i
                call cg_1to1_write_f(self%funit, index_base, index_zone, str%strip(connectname), conn%donorname,&
                                     conn%localscope, conn%donorscope, encode_transform(conn%transform),&
                                     index_item, ier)
            end associate
            end do
        end subroutine output_zone_meta

        subroutine output_zone_data(zone, index_zone)
            type(cgns_zone_structure), intent(in) :: zone
            integer, intent(in) :: index_zone
            integer, allocatable, dimension(:) :: range_min
            integer, allocatable, dimension(:) :: range_max
            integer :: index_dimension
            integer :: i

            select case(zone%zonetype)
            case(STRUCTURED)
                index_dimension = reader%cell_dimension
            case(UNSTRUCTURED)
                index_dimension = 1
            end select
            allocate(range_min(index_dimension))
            allocate(range_max(index_dimension))
            range_min = 1
            range_max = zone%zonesize(1:index_dimension)

            do i = 1, reader%phys_dimension
                call cgp_coord_write_data_f(self%funit, index_base, index_zone, i, range_min, range_max, zone%coords(:,i), ier)
            end do

            do i = 1, size(zone%sections)
            associate(section => zone%sections(i))
                call cgp_elements_write_data_f(self%funit, index_base, index_zone, i, section%scope(1), section%scope(2),&
                                               section%elements, ier)
            end associate
            end do
        end subroutine output_zone_data
    end subroutine writer_construct

    subroutine writer_create_solution(self, solname, location, fieldnames, index_sols)
        class(cgns_writer_class), intent(inout) :: self
        character(len=*), intent(in) :: solname
        integer, intent(in) :: location
        type(string), dimension(:), intent(in) :: fieldnames
        integer, intent(out) :: index_sols
        integer :: index_field
        integer :: ier
        integer :: i, k

        do i = 1, self%reader%nzones
            call cg_sol_write_f(self%funit, index_base, i, str%strip(solname), location, index_sols, ier)
            do k = 1, size(fieldnames)
                call cgp_field_write_f(self%funit, index_base, i, index_sols, REALDOUBLE, fieldnames(k)%s, index_field, ier)
            end do
        end do
    end subroutine writer_create_solution

    subroutine writer_output_solution(self, zonerank, index_sols, index_field, fieldname, range_min, range_max, solution)
        class(cgns_writer_class), intent(inout) :: self
        integer, intent(in) :: zonerank
        integer, intent(in) :: index_sols
        integer, intent(in) :: index_field
        character(len=*), intent(in) :: fieldname
        integer, dimension(:), intent(in) :: range_min
        integer, dimension(:), intent(in) :: range_max
        real(kind=REAL64), dimension(:), intent(in) :: solution
        integer :: index_zone
        integer :: ier

        index_zone = self%reader%zonelist(zonerank)

        call cgp_field_write_data_f(self%funit, index_base, index_zone, index_sols, index_field,&
                                    range_min, range_max, solution, ier)
    end subroutine writer_output_solution

    elemental impure subroutine writer_cleanup(self)
        type(cgns_writer_class), intent(inout) :: self
        integer :: ier

        if (allocated(self%funit)) then
            call cgp_close_f(self%funit, ier)
            deallocate(self%funit)
        end if
    end subroutine writer_cleanup

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Helper functions.
    !-------------------------------------------------------------------------------------------------------------------------------
    pure function encode_transform(decoded) result(encoded)
        integer, dimension(:,:), intent(in) :: decoded
        integer, allocatable, dimension(:) :: encoded
        integer :: ndimensions
        integer :: i

        ndimensions = size(decoded,1)
        allocate(encoded(ndimensions))
        do i = 1, ndimensions
            encoded(i) = maxloc(abs(decoded(:,i)),1)*sum(decoded(:,i))
        end do
    end function encode_transform

    pure function decode_transform(encoded) result(decoded)
        integer, dimension(:), intent(in) :: encoded
        integer, allocatable, dimension(:,:) :: decoded
        integer :: ndimensions
        integer :: i, k

        ndimensions = size(encoded)
        allocate(decoded(ndimensions, ndimensions))
        do k = 1, ndimensions
            do i = 1, ndimensions
                decoded(i,k) = sign(1,encoded(k))*delta(encoded(k),i)
            end do
        end do
    contains
        elemental function delta(x, y)
            integer, intent(in) :: x, y
            integer :: delta
            if (abs(x) .eq. abs(y)) then
                delta = 1
            else
                delta = 0
            end if
        end function delta
    end function decode_transform
end module class_datafmt_cgns

! vim: set ft=fortran ff=unix tw=132:
