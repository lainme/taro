! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2018 lainme <lainme993@gmail.com>
!
! Description:
!
! Compute mathematical expression with shunting-yard algorithm.
!
module class_calculator
    use iso_fortran_env, only : REAL64
    use class_exception
    use class_string
    implicit none
    private
    public :: calculator

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Token definitions.
    !-------------------------------------------------------------------------------------------------------------------------------
    enum, bind(c)
        ! Classification of token.
        enumerator :: CLASS_OPERATOR
        enumerator :: CLASS_OPERANDS
        enumerator :: CLASS_FUNCTION
        enumerator :: CLASS_BRACKETS_LEFT
        enumerator :: CLASS_BRACKETS_RIGHT
        ! Associativity of token.
        enumerator :: ASSOC_LEFT
        enumerator :: ASSOC_RIGHT
    end enum

    type :: token_structure
        integer :: classification ! Whether is operator, operands, math function or brackets.
        integer :: associativity ! Left or right associate.
        integer :: precedence ! Precedence of operator.
        integer :: minoperands ! Minimum operands required.
        integer :: maxoperands ! Maximum operands required.
        real(kind=REAL64) :: solution ! Value of the operands.
        procedure(token_compute), pointer, nopass :: compute => null() ! Function for operator or math functions.
    end type token_structure

    interface token_structure
        procedure :: token_create_object_string ! Create object from string.
        procedure :: token_create_object_number ! Create object from number.
    end interface token_structure

    interface
        function token_compute(operands) result(solution)
            import :: REAL64
            real(kind=REAL64), dimension(:), intent(in) :: operands
            real(kind=REAL64) :: solution
        end function token_compute
    end interface

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Token stack.
    !-------------------------------------------------------------------------------------------------------------------------------
    type :: stack_class
        private
        type(token_structure), pointer :: token => null()
        type(stack_class), pointer :: next => null()
    contains
        procedure :: push => stack_push
        procedure :: pop => stack_pop
        final :: stack_cleanup
    end type stack_class

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Calculator.
    !-------------------------------------------------------------------------------------------------------------------------------
    type :: calculator_class
    contains
        procedure :: eval => calculator_eval
    end type calculator_class

    type(calculator_class) :: calculator
contains
    !-------------------------------------------------------------------------------------------------------------------------------
    ! Token definitions.
    !-------------------------------------------------------------------------------------------------------------------------------
    function token_create_object_string(token_string) result(token)
        character(len=*), intent(in) :: token_string
        type(token_structure) :: token
        type(exception_class) :: exception

        if (str%isnumeric(token_string)) then
            token%classification = CLASS_OPERANDS
            read(token_string, *) token%solution
            return
        end if

        select case(str%lower(token_string))
        case("+")
            call create_operator(token, ASSOC_LEFT, 1, 1, 2, token_compute_plus)
        case("-")
            call create_operator(token, ASSOC_LEFT, 1, 1, 2, token_compute_minus)
        case("*")
            call create_operator(token, ASSOC_LEFT, 2, 2, 2, token_compute_multiply)
        case("/")
            call create_operator(token, ASSOC_LEFT, 2, 2, 2, token_compute_division)
        case("^")
            call create_operator(token, ASSOC_RIGHT, 3, 2, 2, token_compute_power)
        case("sqrt(")
            call create_function(token, token_compute_sqrt)
        case("exp(")
            call create_function(token, token_compute_exp)
        case("cos(")
            call create_function(token, token_compute_cos)
        case("sin(")
            call create_function(token, token_compute_sin)
        case("tan(")
            call create_function(token, token_compute_tan)
        case("arccos(")
            call create_function(token, token_compute_arccos)
        case("arcsin(")
            call create_function(token, token_compute_arcsin)
        case("arctan(")
            call create_function(token, token_compute_arctan)
        case("(")
            token%classification = CLASS_BRACKETS_LEFT
        case(")")
            token%classification = CLASS_BRACKETS_RIGHT
        case("pi")
            token%classification = CLASS_OPERANDS
            token%solution = 4.0*atan(1.0)
        case("e")
            token%classification = CLASS_OPERANDS
            token%solution = exp(1.0)
        case default
            call exception%construct("Expression contains invalid characters.")
            call exception%raise()
        end select
    contains
        subroutine create_operator(token, associativity, precedence, minoperands, maxoperands, compute)
            type(token_structure), intent(out) :: token
            integer, intent(in) :: associativity
            integer, intent(in) :: precedence
            integer, intent(in) :: minoperands
            integer, intent(in) :: maxoperands
            procedure(token_compute) :: compute

            token%classification = CLASS_OPERATOR
            token%associativity = associativity
            token%precedence = precedence
            token%minoperands = minoperands
            token%maxoperands = maxoperands
            token%compute => compute
        end subroutine create_operator

        subroutine create_function(token, compute)
            type(token_structure), intent(out) :: token
            procedure(token_compute) :: compute

            token%classification = CLASS_FUNCTION
            token%minoperands = 1
            token%maxoperands = 1
            token%compute => compute
        end subroutine create_function
    end function token_create_object_string

    function token_create_object_number(token_number) result(token)
        real(kind=REAL64), intent(in) :: token_number
        type(token_structure) :: token

        token%classification = CLASS_OPERANDS
        token%solution = token_number
    end function token_create_object_number

    pure function token_compute_plus(operands) result(solution)
        real(kind=REAL64), dimension(:), intent(in) :: operands
        real(kind=REAL64) :: solution

        solution = sum(operands)
    end function token_compute_plus

    pure function token_compute_minus(operands) result(solution)
        real(kind=REAL64), dimension(:), intent(in) :: operands
        real(kind=REAL64) :: solution

        solution = sum(operands)-2*operands(1)
    end function token_compute_minus

    pure function token_compute_multiply(operands) result(solution)
        real(kind=REAL64), dimension(:), intent(in) :: operands
        real(kind=REAL64) :: solution

        solution = product(operands)
    end function token_compute_multiply

    pure function token_compute_division(operands) result(solution)
        real(kind=REAL64), dimension(:), intent(in) :: operands
        real(kind=REAL64) :: solution

        solution = operands(2)/operands(1)
    end function token_compute_division

    pure function token_compute_power(operands) result(solution)
        real(kind=REAL64), dimension(:), intent(in) :: operands
        real(kind=REAL64) :: solution

        solution = operands(2)**operands(1)
    end function token_compute_power

    pure function token_compute_sqrt(operands) result(solution)
        real(kind=REAL64), dimension(:), intent(in) :: operands
        real(kind=REAL64) :: solution

        solution = sqrt(operands(1))
    end function token_compute_sqrt

    pure function token_compute_exp(operands) result(solution)
        real(kind=REAL64), dimension(:), intent(in) :: operands
        real(kind=REAL64) :: solution

        solution = exp(operands(1))
    end function token_compute_exp

    pure function token_compute_cos(operands) result(solution)
        real(kind=REAL64), dimension(:), intent(in) :: operands
        real(kind=REAL64) :: solution

        solution = cos(operands(1))
    end function token_compute_cos

    pure function token_compute_sin(operands) result(solution)
        real(kind=REAL64), dimension(:), intent(in) :: operands
        real(kind=REAL64) :: solution

        solution = sin(operands(1))
    end function token_compute_sin

    pure function token_compute_tan(operands) result(solution)
        real(kind=REAL64), dimension(:), intent(in) :: operands
        real(kind=REAL64) :: solution

        solution = tan(operands(1))
    end function token_compute_tan

    pure function token_compute_arccos(operands) result(solution)
        real(kind=REAL64), dimension(:), intent(in) :: operands
        real(kind=REAL64) :: solution

        solution = acos(operands(1))
    end function token_compute_arccos

    pure function token_compute_arcsin(operands) result(solution)
        real(kind=REAL64), dimension(:), intent(in) :: operands
        real(kind=REAL64) :: solution

        solution = asin(operands(1))
    end function token_compute_arcsin

    pure function token_compute_arctan(operands) result(solution)
        real(kind=REAL64), dimension(:), intent(in) :: operands
        real(kind=REAL64) :: solution

        solution = atan(operands(1))
    end function token_compute_arctan

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Token stack.
    !-------------------------------------------------------------------------------------------------------------------------------
    subroutine stack_push(self, token)
        class(stack_class), intent(inout), target :: self
        type(token_structure), intent(in) :: token
        class(stack_class), pointer :: next

        if (associated(self%token)) then
            next => self%next
            allocate(self%next)
            self%next%token => self%token
            self%next%next => next
        end if

        allocate(self%token, source=token)
    end subroutine stack_push

    function stack_pop(self, token) result(success)
        class(stack_class), intent(inout), target :: self
        type(token_structure), intent(out) :: token
        logical :: success
        class(stack_class), pointer :: next

        if (.not. associated(self%token)) then
            success = .false.
        else
            success = .true.
            token = self%token
            deallocate(self%token)
        end if

        if (associated(self%next)) then
            next => self%next

            self%token => next%token
            self%next => next%next

            next%token => null()
            next%next => null()
            deallocate(next)
        end if
    end function stack_pop

    elemental subroutine stack_cleanup(self)
        type(stack_class), intent(inout) :: self

        if (associated(self%token)) then
            deallocate(self%token)
        end if

        if (associated(self%next)) then
            deallocate(self%next)
        end if
    end subroutine stack_cleanup

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Calculator.
    !-------------------------------------------------------------------------------------------------------------------------------
    function calculator_eval(self, expression, stat) result(solution)
        class(calculator_class), intent(in) :: self
        character(len=*), intent(in), target :: expression
        integer, intent(out), optional :: stat
        real(kind=REAL64) :: solution
        character(len=:), pointer :: expression_unparsed
        type(exception_class) :: exception
        type(stack_class) :: operands_stack
        type(stack_class) :: operator_stack
        type(token_structure) :: operator_token
        type(token_structure) :: token
        integer :: cursor
        integer :: i

        expression_unparsed => expression

        do while(parse_token(expression_unparsed, token))
            select case(token%classification)
            case(CLASS_OPERANDS)
                call operands_stack%push(token)
            case(CLASS_OPERATOR)
                do while(operator_stack%pop(operator_token))
                    if (((token%precedence < operator_token%precedence) .or. &
                         (token%precedence .eq. operator_token%precedence .and. token%associativity .eq. ASSOC_LEFT)) .and. &
                        (operator_token%classification .ne. CLASS_BRACKETS_LEFT .and. &
                         operator_token%classification .ne. CLASS_FUNCTION)) then
                        call execute_operation(operator_token, operands_stack)
                    else
                        call operator_stack%push(operator_token)
                        exit
                    end if
                end do
                call operator_stack%push(token)
            case(CLASS_BRACKETS_LEFT, CLASS_FUNCTION)
                call operator_stack%push(token)
            case(CLASS_BRACKETS_RIGHT)
                do while(operator_stack%pop(operator_token))
                    if (operator_token%classification .ne. CLASS_BRACKETS_LEFT) then
                        call execute_operation(operator_token, operands_stack)
                    end if
                    if (operator_token%classification .eq. CLASS_BRACKETS_LEFT .or. &
                        operator_token%classification .eq. CLASS_FUNCTION) then
                        exit
                    end if
                end do
            end select
        end do

        do while(operator_stack%pop(operator_token))
            call execute_operation(operator_token, operands_stack)
        end do

        if (operands_stack%pop(token)) then
            solution = token%solution
        else
            call exception%construct("Couldn't evalute the expression.")
        end if

        if (present(stat)) then
            stat = exception%stat
        else
            call exception%raise()
        end if
    contains
        function parse_token(expression, token) result(success)
            character(len=:), pointer, intent(inout) :: expression
            type(token_structure), intent(out) :: token
            logical :: success
            integer :: i

            ! Parse expression from right to left until the leading string is recognized.
            success = .false.
            do i = 1, len(expression)
            associate(location => len(expression)+1-i)
                if (istoken(expression(1:location))) then
                    token = token_structure(expression(1:location))
                    expression => expression(location+1:)
                    success = .true.
                    return
                end if
            end associate
            end do
        end function parse_token

        function istoken(characters)
            character(len=*), intent(in) :: characters
            logical :: istoken

            ! Fast check potential valid tokens.
            istoken = str%isnumeric(characters) .or. &
                      str%isalpha(characters) .or. & ! Possible math constants.
                      (str%isalpha(characters(:len(characters)-1)) .and. characters(len(characters):) == "(") .or. & ! Functions.
                      characters == "(" .or. &
                      characters == ")" .or. &
                      scan(characters, DIGITSDOT//UPPERCASE//LOWERCASE//"()") == 0 ! Special chars (possible operators).
        end function istoken

        subroutine execute_operation(operator_token, operands_stack)
            type(token_structure), intent(in) :: operator_token
            type(stack_class), intent(inout) :: operands_stack
            type(token_structure) :: operands_token(2)
            type(exception_class) :: exception
            integer :: i

            do i = 1, operator_token%maxoperands
                if (.not. operands_stack%pop(operands_token(i))) then
                    if (i < operator_token%minoperands) then
                        call exception%construct("No enough operands in the expression.")
                        call exception%raise()
                    end if
                    exit
                end if
            end do
            call operands_stack%push(token_structure(operator_token%compute(operands_token(:i-1)%solution)))
        end subroutine execute_operation
    end function calculator_eval
end module class_calculator

! vim: set ft=fortran ff=unix tw=132:
