! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2018 lainme <lainme993@gmail.com>
!
! Description:
!
! Numerical quadrature algorithms.
!
module class_quadrature
    use iso_fortran_env, only : REAL64
    implicit none
    private
    public :: quadrature

    real(kind=REAL64), parameter :: PI = 4.0*atan(1.0)

    type :: quadrature_class
    contains
        procedure :: newton_cotes_boole
        procedure :: gaussian_maxwell ! Half-space Maxwell polynomials exp(-x^2)*x^p (p=0 only).
    end type quadrature_class

    type(quadrature_class) :: quadrature
contains
    pure subroutine newton_cotes_boole(self, npoints, scope, abscissas, weight)
        class(quadrature_class), intent(in) :: self
        integer, intent(inout) :: npoints
        real(kind=REAL64), intent(in) :: scope(2) ! Range of the variables to integrate.
        real(kind=REAL64), allocatable, dimension(:), intent(out) :: abscissas
        real(kind=REAL64), allocatable, dimension(:), intent(out) :: weight
        real(kind=REAL64) :: dx
        integer :: i

        ! Correct number of points.
        npoints = (npoints/4)*4+1

        allocate(abscissas(npoints))
        allocate(weight(npoints))

        ! This dimension is reduced.
        if (npoints == 1) then
            abscissas(1) = 0.0
            weight(1) = 1.0
            return
        end if

        dx = (scope(2)-scope(1))/(npoints-1)

        forall(i=1:npoints)
            abscissas(i) = scope(1)+(i-1)*dx
            weight(i) = coefficient(i)*dx
        end forall
    contains
        elemental function coefficient(i)
            integer, intent(in) :: i
            real(kind=REAL64) :: coefficient

            if (i==1 .or. i==npoints) then
                coefficient = 14.0/45.0
            else if (mod(i-5,4)==0) then
                coefficient = 28.0/45.0
            else if (mod(i-3,4)==0) then
                coefficient = 24.0/45.0
            else
                coefficient = 64.0/45.0
            end if
        end function coefficient
    end subroutine newton_cotes_boole

    subroutine gaussian_maxwell(self, npoints, abscissas, weight, expand)
        class(quadrature_class), intent(in) :: self
        integer, intent(inout) :: npoints ! Number of points.
        real(kind=REAL64), allocatable, dimension(:), intent(out) :: abscissas
        real(kind=REAL64), allocatable, dimension(:), intent(out) :: weight
        logical, intent(in), optional :: expand ! Whether expand to full space, default to false.
        real(kind=REAL64), allocatable, dimension(:,:) :: eigenvector
        real(kind=REAL64), allocatable, dimension(:) :: workspace
        real(kind=REAL64), allocatable, dimension(:) :: alpha
        real(kind=REAL64), allocatable, dimension(:) :: beta
        real(kind=REAL64), allocatable, dimension(:) :: gn
        real(kind=REAL64) :: scale_factor ! Integrate exp(-x^2)*x^p from 0 to Infinity.
        real(kind=REAL64) :: residual
        real(kind=REAL64) :: gn_old
        real(kind=REAL64) :: y
        integer :: iter
        integer :: err
        integer :: p
        integer :: i

        ! Constant.
        p = 0

        allocate(alpha(npoints))
        allocate(beta(npoints))
        allocate(gn(max(51,npoints+1)))
        allocate(eigenvector(npoints, npoints))
        allocate(workspace(max(1,2*npoints-2)))

        !---------------------------------------------------------------------------------------------------------------------------
        ! Compute alpha and beta in the tridiagonal matrix.
        ! Ref: Half-Range Generalized Hermite Polynomials and the Related Gaussian Quadratures
        ! (https://doi.org/10.1137/S0036142900370939)
        !---------------------------------------------------------------------------------------------------------------------------
        ! Starting values.
        alpha(1) = 1.0/sqrt(PI)
        alpha(2) = 2.0/sqrt(PI)/(PI-2.0)
        beta(1) = 0.0
        beta(2) = 0.0+(p+1)/2.0-alpha(1)**2-beta(1)
        gn(1) = beta(1)-(0.0+p/2.0)/6.0
        gn(2) = beta(2)-(1.0+p/2.0)/6.0

        ! Recurrence calculation of gn.
        do i = 3, min(npoints, 9)
            y = 2*(i-2)+p
            gn(i) = (y+1)/3.0-gn(i-1)-((y/6.0-gn(i-1))**2-p**2/16.0)**2/((y-1)/3.0-gn(i-1)-gn(i-2))/(y/12.0+gn(i-1))**2
        end do
        do i = min(npoints, 9)+1, size(gn)
            gn(i) = compute_gn(i-1)
        end do

        ! Iterative calculation of gn (accuracy improvement).
        iter = 0
        do while(.true.)
            residual = 0.0
            do i = 3, size(gn)-1
                gn_old = gn(i)
                gn(i) = iterate_gn(i)
                residual = max(residual, abs(gn_old-gn(i)))
            end do
            iter = iter+1
            if (residual <= 1.0E-16 .or. iter >= 50) then
                exit
            end if
        end do

        ! Compute alpha and sqrt(beta).
        do i = 1, npoints
            alpha(i) = sqrt((2*(i-1)+p+1)/3.0-gn(i+1)-gn(i))
            beta(i) = sqrt(gn(i+1)+(i+p/2.0)/6.0)
        end do

        !---------------------------------------------------------------------------------------------------------------------------
        ! Compute abscissas and weights.
        ! Ref: A Gaussian quadrature procedure for use in the solution of the Boltzmann equation and related problems
        ! (https://doi.org/10.1016/0021-9991(81)90099-1)
        !---------------------------------------------------------------------------------------------------------------------------
        call dstev('V', npoints, alpha, beta, eigenvector, npoints, workspace, err)

        scale_factor = gamma((p+1)/2.0)/2.0
        if (present(expand) .and. expand) then
            allocate(abscissas(2*npoints))
            allocate(weight(2*npoints))
            abscissas(npoints+1:) = alpha
            abscissas(npoints:1:-1) = -alpha
            weight(npoints+1:) = scale_factor*eigenvector(1,:)**2
            weight(npoints:1:-1) = weight(npoints+1:)
            npoints = 2*npoints
        else
            allocate(abscissas(npoints))
            allocate(weight(npoints))
            abscissas = alpha
            weight = scale_factor*eigenvector(1,:)**2
        end if
    contains
        elemental function compute_gn(n) result(g)
            integer, intent(in) :: n
            real(kind=REAL64) :: g
            real(kind=REAL64) :: c0, c1, c2, c3, y

            c0 = 1.0/36.0-p**2/8.0
            c1 = 23.0/432.0-11.0/48.0*p**2+3.0/32.0*p**4
            c2 = 1189.0/2592.0-409.0/192.0*p**2+75.0/64.0*p**4+9.0/64.0*p**6
            c3 = 196057.0/20736.0-153559.0/3456.0*p**2+7111.0/256.0*p**4+639.0/128.0*p**6+135.0/512.0*p**8
            y = 2*n+p
            g = c0/y+c1/y**3+c2/y**5+c3/y**7
        end function compute_gn

        elemental function iterate_gn(n) result(g)
            integer, intent(in) :: n
            real(kind=REAL64) :: g
            real(kind=REAL64) :: y, a, b, c, d, e

            y = 2*(n-1)+p
            a = gn(n+1)-(y+1)/3.0
            b = gn(n-1)-(y-1)/3.0
            c = y/12.0
            d = y/6.0
            e = p**2/16.0
            g = gn(n)**3*(-4*d-2*c-(a+b))+gn(i)**2*(6*d**2-2*e**2-c**2-(a+b)*2*c-a*b)-2*(e*d)**2+e**4+d**4-a*b*c**2
            g = g/((a+b)*c**2+2*a*b*c+4*d**3-4*e**2*d)
        end function iterate_gn
    end subroutine gaussian_maxwell
end module class_quadrature

! vim: set ft=fortran ff=unix tw=132:
