! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
! Description:
!
! Better string type and manipulate strings, inspired by Python standard string methods and sawlibf
! (https://github.com/ScottWales/sawlibf/blob/master/src/lib/sawlibf/string.f90).
!
module class_string
    implicit none
    private
    public :: UPPERCASE
    public :: LOWERCASE
    public :: DIGITSDOT
    public :: string
    public :: assignment(=)
    public :: operator(//)
    public :: operator(.eq.)
    public :: operator(.ne.)
    public :: operator(.lt.)
    public :: operator(.le.)
    public :: operator(.gt.)
    public :: operator(.ge.)
    public :: operator(.in.)
    public :: str

    character(len=26), parameter :: UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    character(len=26), parameter :: LOWERCASE = "abcdefghijklmnopqrstuvwxyz"
    character(len=11), parameter :: DIGITSDOT = "0123456789."

    !-------------------------------------------------------------------------------------------------------------------------------
    ! String type sine vary length characters in fortran sucks!
    !-------------------------------------------------------------------------------------------------------------------------------
    type :: string
        character(len=:), allocatable :: s
    contains
        generic :: write(formatted) => formatted_write
        procedure, private :: formatted_write => string_formatted_write
    end type string

    interface assignment(=)
        procedure :: copy
        procedure :: copy_lhs_characters
        procedure :: copy_rhs_characters
    end interface

    interface operator(//)
        procedure :: append
        procedure :: append_lhs_characters
        procedure :: append_rhs_characters
    end interface

    interface operator(.eq.)
        procedure :: equiv
        procedure :: equiv_lhs_characters
        procedure :: equiv_rhs_characters
    end interface

    interface operator(.ne.)
        procedure :: nequiv
        procedure :: nequiv_lhs_characters
        procedure :: nequiv_rhs_characters
    end interface

    interface operator(.lt.)
        procedure :: smaller
        procedure :: smaller_lhs_characters
        procedure :: smaller_rhs_characters
    end interface

    interface operator(.le.)
        procedure :: smaller_or_equiv
        procedure :: smaller_or_equiv_lhs_characters
        procedure :: smaller_or_equiv_rhs_characters
    end interface

    interface operator(.gt.)
        procedure :: greater
        procedure :: greater_lhs_characters
        procedure :: greater_rhs_characters
    end interface

    interface operator(.ge.)
        procedure :: greater_or_equiv
        procedure :: greater_or_equiv_lhs_characters
        procedure :: greater_or_equiv_rhs_characters
    end interface

    interface operator(.in.)
        procedure :: inarray
        procedure :: inarray_characters
    end interface

    !-------------------------------------------------------------------------------------------------------------------------------
    ! String class to manipulate characters and strings.
    !-------------------------------------------------------------------------------------------------------------------------------
    type :: string_class
    contains
        procedure :: colorize ! Generate a colored terminal string with standard colors (WARNING: Linux only).
        procedure :: hash ! Compute hash of the string.
        procedure :: upper
        procedure :: lower
        procedure :: strip
        procedure :: split
        procedure :: join
        procedure :: startswith
        procedure :: endswith
        procedure :: isnumeric
        procedure :: isalpha
    end type string_class

    type(string_class) :: str
contains
    !-------------------------------------------------------------------------------------------------------------------------------
    ! Operator overloads.
    !-------------------------------------------------------------------------------------------------------------------------------
    pure subroutine copy(this, source)
        type(string), intent(out) :: this
        type(string), intent(in) :: source

        this%s = source%s
    end subroutine copy

    pure subroutine copy_lhs_characters(this, source)
        character(len=:), allocatable, intent(out) :: this
        type(string), intent(in) :: source

        this = source%s
    end subroutine copy_lhs_characters

    pure subroutine copy_rhs_characters(this, source)
        type(string), intent(out) :: this
        character(len=*), intent(in) :: source

        this%s = source
    end subroutine copy_rhs_characters

    pure function append(this, other) result(characters)
        type(string), intent(in) :: this
        type(string), intent(in) :: other
        character(len=:), allocatable :: characters

        characters = this%s//other%s
    end function append

    pure function append_lhs_characters(this, other) result(characters)
        character(len=*), intent(in) :: this
        type(string), intent(in) :: other
        character(len=:), allocatable :: characters

        characters = this//other%s
    end function append_lhs_characters

    pure function append_rhs_characters(this, other) result(characters)
        type(string), intent(in) :: this
        character(len=*), intent(in) :: other
        character(len=:), allocatable :: characters

        characters = this%s//other
    end function append_rhs_characters

    elemental function equiv(this, other)
        type(string), intent(in) :: this
        type(string), intent(in) :: other
        logical :: equiv

        equiv = this%s .eq. other%s
    end function equiv

    elemental function equiv_lhs_characters(this, other) result(equiv)
        character(len=*), intent(in) :: this
        type(string), intent(in) :: other
        logical :: equiv

        equiv = this .eq. other%s
    end function equiv_lhs_characters

    elemental function equiv_rhs_characters(this, other) result(equiv)
        type(string), intent(in) :: this
        character(len=*), intent(in) :: other
        logical :: equiv

        equiv = this%s .eq. other
    end function equiv_rhs_characters

    elemental function nequiv(this, other)
        type(string), intent(in) :: this
        type(string), intent(in) :: other
        logical :: nequiv

        nequiv = this%s .ne. other%s
    end function nequiv

    elemental function nequiv_lhs_characters(this, other) result(nequiv)
        character(len=*), intent(in) :: this
        type(string), intent(in) :: other
        logical :: nequiv

        nequiv = this .ne. other%s
    end function nequiv_lhs_characters

    elemental function nequiv_rhs_characters(this, other) result(nequiv)
        type(string), intent(in) :: this
        character(len=*), intent(in) :: other
        logical :: nequiv

        nequiv = this%s .ne. other
    end function nequiv_rhs_characters

    elemental function smaller(this, other)
        type(string), intent(in) :: this
        type(string), intent(in) :: other
        logical :: smaller

        smaller = this%s .lt. other%s
    end function smaller

    elemental function smaller_lhs_characters(this, other) result(smaller)
        character(len=*), intent(in) :: this
        type(string), intent(in) :: other
        logical :: smaller

        smaller = this .lt. other%s
    end function smaller_lhs_characters

    elemental function smaller_rhs_characters(this, other) result(smaller)
        type(string), intent(in) :: this
        character(len=*), intent(in) :: other
        logical :: smaller

        smaller = this%s .lt. other
    end function smaller_rhs_characters

    elemental function smaller_or_equiv(this, other)
        type(string), intent(in) :: this
        type(string), intent(in) :: other
        logical :: smaller_or_equiv

        smaller_or_equiv = this%s .le. other%s
    end function smaller_or_equiv

    elemental function smaller_or_equiv_lhs_characters(this, other) result(smaller_or_equiv)
        character(len=*), intent(in) :: this
        type(string), intent(in) :: other
        logical :: smaller_or_equiv

        smaller_or_equiv = this .le. other%s
    end function smaller_or_equiv_lhs_characters

    elemental function smaller_or_equiv_rhs_characters(this, other) result(smaller_or_equiv)
        type(string), intent(in) :: this
        character(len=*), intent(in) :: other
        logical :: smaller_or_equiv

        smaller_or_equiv = this%s .le. other
    end function smaller_or_equiv_rhs_characters

    elemental function greater(this, other)
        type(string), intent(in) :: this
        type(string), intent(in) :: other
        logical :: greater

        greater = this%s .gt. other%s
    end function greater

    elemental function greater_lhs_characters(this, other) result(greater)
        character(len=*), intent(in) :: this
        type(string), intent(in) :: other
        logical :: greater

        greater = this .gt. other%s
    end function greater_lhs_characters

    elemental function greater_rhs_characters(this, other) result(greater)
        type(string), intent(in) :: this
        character(len=*), intent(in) :: other
        logical :: greater

        greater = this%s .gt. other
    end function greater_rhs_characters

    elemental function greater_or_equiv(this, other)
        type(string), intent(in) :: this
        type(string), intent(in) :: other
        logical :: greater_or_equiv

        greater_or_equiv = this%s .ge. other%s
    end function greater_or_equiv

    elemental function greater_or_equiv_lhs_characters(this, other) result(greater_or_equiv)
        character(len=*), intent(in) :: this
        type(string), intent(in) :: other
        logical :: greater_or_equiv

        greater_or_equiv = this .ge. other%s
    end function greater_or_equiv_lhs_characters

    elemental function greater_or_equiv_rhs_characters(this, other) result(greater_or_equiv)
        type(string), intent(in) :: this
        character(len=*), intent(in) :: other
        logical :: greater_or_equiv

        greater_or_equiv = this%s .ge. other
    end function greater_or_equiv_rhs_characters

    pure function inarray(this, array)
        type(string), intent(in) :: this
        type(string), dimension(:), intent(in) :: array
        logical :: inarray
        integer :: i

        inarray = .false.

        do i = 1, size(array)
            if (this .eq. array(i)) then
                inarray = .true.
                exit
            end if
        end do
    end function inarray

    pure function inarray_characters(this, array) result(inarray)
        character(len=*), intent(in) :: this
        type(string), dimension(:), intent(in) :: array
        logical :: inarray
        integer :: i

        inarray = .false.

        do i = 1, size(array)
            if (this .eq. array(i)%s) then
                inarray = .true.
                exit
            end if
        end do
    end function inarray_characters

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Methods for the string class.
    !-------------------------------------------------------------------------------------------------------------------------------
    pure function colorize(self, characters, color) result(colored)
        class(string_class), intent(in) :: self
        character(len=*), intent(in) :: characters
        character(len=*), intent(in) :: color
        character(len=:), allocatable :: colored
        character(len=:), allocatable :: color_sequence

        select case(self%strip(color))
        case("black")
            color_sequence = "[30;1m"
        case("red")
            color_sequence = "[31;1m"
        case("green")
            color_sequence = "[32;1m"
        case("yellow")
            color_sequence = "[33;1m"
        case("blue")
            color_sequence = "[34;1m"
        case("magenta")
            color_sequence = "[35;1m"
        case("cyan")
            color_sequence = "[36;1m"
        case("white")
            color_sequence = "[37;1m"
        end select

        colored = achar(27)//color_sequence//characters//achar(27)//"[0m"
    end function colorize

    elemental function hash(self, characters)
        class(string_class), intent(in) :: self
        character(len=*), intent(in) :: characters
        integer :: hash
        integer :: i

        ! DJB2 by Daniel J. Bernstein. It's intresting to implement murmurhash3, but it looks difficult.
        hash = 5381

        do i = 1, len(characters)
            hash = (ishft(hash,5)+hash)+ichar(characters(i:i))
        end do

        hash = abs(hash)
    end function hash

    pure function upper(self, characters) result(uppercased)
        class(string_class), intent(in) :: self
        character(len=*), intent(in) :: characters
        character(len=:), allocatable :: uppercased
        integer :: location
        integer :: i

        uppercased = characters

        do i = 1, len(uppercased)
            location = index(LOWERCASE, uppercased(i:i))
            if (location > 0) then
                uppercased(i:i) = UPPERCASE(location:location)
            end if
        end do
    end function upper

    pure function lower(self, characters) result(lowercased)
        class(string_class), intent(in) :: self
        character(len=*), intent(in) :: characters
        character(len=:), allocatable :: lowercased
        integer :: location
        integer :: i

        lowercased = characters

        do i = 1, len(lowercased)
            location = index(UPPERCASE, lowercased(i:i))
            if (location > 0) then
                lowercased(i:i) = lowerCASE(location:location)
            end if
        end do
    end function lower

    pure function strip(self, characters, discards) result(stripped)
        class(string_class), intent(in) :: self
        character(len=*), intent(in) :: characters
        character(len=*), intent(in), optional :: discards
        character(len=:), allocatable :: stripped
        integer :: strlen

        stripped = trim(adjustl(characters))

        if (present(discards)) then
            do while(.true.)
                strlen = len(stripped)
                if (scan(discards, stripped(1:1)) > 0) then
                    stripped = stripped(2:)
                else if (scan(discards, stripped(strlen:strlen)) > 0) then
                    stripped = stripped(:strlen-1)
                else
                    return
                end if
            end do
        end if
    end function strip

    pure function split(self, characters, seperator) result(splitted)
        class(string_class), intent(in) :: self
        character(len=*), intent(in) :: characters
        character(len=*), intent(in), optional :: seperator ! Default to a white space.
        character(len=:), allocatable :: seperator_choosen
        type(string), allocatable, dimension(:) :: splitted
        type(string), allocatable, dimension(:) :: splitted_expanded
        character(len=:), allocatable :: substring
        integer :: prev, next

        prev = 1
        next = 1

        if (present(seperator)) then
            seperator_choosen = seperator
        else
            seperator_choosen = " "
        end if

        do while(next /= 0)
            next = index(characters(prev:), seperator_choosen)
            prev = prev+next

            ! Determine string before and after the seperator.
            if (next == 1 .and. seperator_choosen == " ") then ! Duplicate, discard.
                cycle
            else if (next == 1) then
                substring = ''
            else if (next == 0) then ! Not found.
                substring = characters(prev-next:)
            else ! Found.
                substring = characters(prev-next:prev-2)
            end if

            ! Expand the string array.
            if (.not. allocated(splitted)) then
                allocate(splitted(1))
            else
                allocate(splitted_expanded(size(splitted)+1))
                splitted_expanded(1:size(splitted)) = splitted
                call move_alloc(splitted_expanded, splitted)
            end if

            splitted(ubound(splitted,1)) = self%strip(substring)
        end do
    end function split

    pure function join(self, array, seperator) result(joined)
        class(string_class), intent(in) :: self
        type(string), dimension(:), intent(in) :: array
        character(len=*), intent(in), optional :: seperator ! Default to empty.
        character(len=:), allocatable :: seperator_choosen
        character(len=:), allocatable :: joined
        integer :: i

        if (present(seperator)) then
            seperator_choosen = seperator
        else
            seperator_choosen = ''
        end if

        joined = array(1)%s

        do i = 2, size(array)
            joined = joined//seperator_choosen//array(i)%s
        end do
    end function join

    elemental function startswith(self, characters, substring)
        class(string_class), intent(in) :: self
        character(len=*), intent(in) :: characters
        character(len=*), intent(in) :: substring
        logical :: startswith
        integer :: location

        location = index(characters, substring)

        if (location .ne. 1) then
            startswith = .false.
        else
            startswith = .true.
        end if
    end function startswith

    elemental function endswith(self, characters, substring)
        class(string_class), intent(in) :: self
        character(len=*), intent(in) :: characters
        character(len=*), intent(in) :: substring
        logical :: endswith
        integer :: location

        location = index(characters, substring, .true.)

        if (location .ne. len(characters)-len(substring)+1) then
            endswith = .false.
        else
            endswith = .true.
        end if
    end function endswith

    elemental function isnumeric(self, characters)
        class(string_class), intent(in) :: self
        character(len=*), intent(in) :: characters
        logical :: isnumeric
        integer :: location

        isnumeric = .false.

        location = index(str%upper(characters), "E+")
        if (location > 0 .and. location < len(characters)-1) then
            if (isdigits(characters(:location-1)).and. isdigits(characters(location+2:))) then
                isnumeric = .true.
            end if
        end if

        location = index(str%upper(characters), "E-")
        if (location > 0 .and. location < len(characters)-1) then
            if (isdigits(characters(:location-1)).and. isdigits(characters(location+2:))) then
                isnumeric = .true.
            end if
        end if

        if (isdigits(characters)) then
            isnumeric = .true.
        end if
    contains
        elemental function isdigits(characters)
            character(len=*), intent(in) :: characters
            logical :: isdigits

            isdigits = verify(characters, DIGITSDOT) == 0
        end function
    end function isnumeric

    elemental function isalpha(self, characters)
        class(string_class), intent(in) :: self
        character(len=*), intent(in) :: characters
        logical :: isalpha

        isalpha = verify(characters, UPPERCASE//LOWERCASE) == 0
    end function isalpha

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Methods for the string type.
    !-------------------------------------------------------------------------------------------------------------------------------
    subroutine string_formatted_write(self, unit, iotype, vlist, iostat, iomsg)
        class(string), intent(in) :: self
        integer, intent(in) :: unit
        character(len=*), intent(in) :: iotype
        integer, dimension(:), intent(in) :: vlist
        integer, intent(out) :: iostat
        character(len=*), intent(inout) :: iomsg

        write(unit, "(A)", advance="no", iostat=iostat, iomsg=iomsg) self%s
    end subroutine string_formatted_write
end module class_string

! vim: set ft=fortran ff=unix tw=132:
