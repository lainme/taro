! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
! Description:
!
! Exception definitions and handlers.
!
module class_exception
    use iso_fortran_env, only : ERROR_UNIT
    use class_string
    implicit none
    private
    public :: exception_class

    type :: exception_class
        private
        integer, public :: stat = 0
        character(len=:), allocatable, public :: message
    contains
        procedure :: construct
        procedure :: raise
    end type exception_class
contains
    elemental subroutine construct(self, message)
        class(exception_class), intent(inout) :: self
        character(len=*), intent(in) :: message

        self%stat = 1
        self%message = str%strip(message)
    end subroutine construct

    subroutine raise(self)
        class(exception_class), intent(in) :: self

        if (self%stat > 0) then
            write(ERROR_UNIT, "(A,1X,A)") str%colorize("[ERROR]:", "red"), self%message
            stop
        end if
    end subroutine raise
end module class_exception

! vim: set ft=fortran ff=unix tw=132:
