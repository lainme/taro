! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2018 lainme <lainme993@gmail.com>
!
! Description:
!
! Encoding/decoding array indexes.
!
module class_indexmap
    implicit none
    private
    public :: indexmap

    type :: indexmap_class
    contains
        procedure :: encode
        procedure :: decode
    end type indexmap_class

    type(indexmap_class) :: indexmap
contains
    pure function encode(self, scope, decoded) result(encoded)
        class(indexmap_class), intent(in) :: self
        integer, dimension(:), intent(in) :: scope
        integer, dimension(:), intent(in) :: decoded
        integer :: encoded
        integer :: i

        encoded = decoded(1)
        do i = 2, size(scope)
            encoded = encoded+(decoded(i)-1)*product(scope(1:i-1))
        end do
    end function encode

    pure function decode(self, scope, encoded) result(decoded)
        class(indexmap_class), intent(in) :: self
        integer, dimension(:), intent(in) :: scope
        integer, intent(in) :: encoded
        integer :: decoded(size(scope))
        integer :: remains
        integer :: i

        decoded(1) = mod(encoded-1, scope(1))+1
        remains = encoded-decoded(1)
        do i = 2, size(scope)
            decoded(i) = mod(remains/product(scope(1:i-1)), scope(i))+1
            remains = remains-(decoded(i)-1)*product(scope(1:i-1))
        end do
    end function decode
end module class_indexmap

! vim: set ft=fortran ff=unix tw=132:
