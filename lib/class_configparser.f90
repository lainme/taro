! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
! Description:
!
! A simple INI parser inspired by Python standard library.
!
module class_configparser
    use iso_fortran_env, only : IOSTAT_END, INT32, INT64, REAL32, REAL64
    use class_string
    use class_hashmap
    use class_iostream
    use class_exception
    use class_calculator
    implicit none
    private
    public :: configparser_class

    type, extends(hashmap_pair) :: config_structure
        character(len=:), allocatable :: key ! Name of the element.
        character(len=:), allocatable :: content ! Parsed content of the element.
    contains
        procedure :: has_samekey => config_has_samekey
        procedure :: copy => config_copy
        procedure :: hash => config_hash
    end type config_structure

    interface config_structure
        procedure :: config_create_object
    end interface config_structure

    type :: configparser_class
        private
        integer :: config_unit
        character(len=:), allocatable :: config_file
        type(hashmap_class) :: configurations
    contains
        procedure :: construct => configparser_construct
        procedure :: parse => configparser_parse
        procedure :: set => configparser_set
        generic :: get => configparser_get_scalar, configparser_get_vector
        procedure, private :: configparser_get_scalar
        procedure, private :: configparser_get_vector
    end type configparser_class
contains
    !-------------------------------------------------------------------------------------------------------------------------------
    ! Config structure methods.
    !-------------------------------------------------------------------------------------------------------------------------------
    elemental function config_create_object(key, content) result(config)
        character(len=*), intent(in) :: key
        character(len=*), intent(in), optional :: content
        type(config_structure) :: config

        config%key = str%strip(key)
        if (present(content)) then
            config%content = str%strip(content)
        end if
    end function config_create_object

    elemental function config_has_samekey(self, pair) result(has_samekey)
        class(config_structure), intent(in) :: self
        class(hashmap_pair), intent(in) :: pair
        logical :: has_samekey

        has_samekey = .false.

        select type(pair)
        class is(config_structure)
            has_samekey = pair%key .eq. self%key
        end select
    end function config_has_samekey

    subroutine config_copy(self, object)
        class(config_structure), intent(in) :: self
        class(*), intent(out) :: object

        select type(object)
        class is(config_structure)
            object%key = self%key
            if (allocated(self%content)) then
                object%content = self%content
            end if
        class is(string)
            if (allocated(self%content)) then
                object = self%content
            end if
        end select
    end subroutine config_copy

    elemental function config_hash(self) result(hash)
        class(config_structure), intent(in) :: self
        integer :: hash

        hash = str%hash(self%key)
    end function config_hash

    !-------------------------------------------------------------------------------------------------------------------------------
    ! Config class methods.
    !-------------------------------------------------------------------------------------------------------------------------------
    subroutine configparser_construct(self, config_file, config_unit)
        class(configparser_class), intent(inout) :: self
        character(len=*), intent(in), optional :: config_file
        integer, intent(in), optional :: config_unit
        integer :: stat

        deallocate(self%config_file, stat=stat)

        if (present(config_file)) then
            self%config_file = config_file
        end if

        if (present(config_unit)) then
            self%config_unit = config_unit
        end if

        call self%configurations%construct()
    end subroutine configparser_construct

    subroutine configparser_parse(self)
        class(configparser_class), intent(inout) :: self
        type(exception_class) :: exception
        type(string), allocatable, dimension(:) :: element
        type(string) :: section
        character(len=:), allocatable :: line
        character(len=:), allocatable :: key
        character(len=:), allocatable :: content
        integer :: stat

        stat = 0
        section = ''

        if (allocated(self%config_file)) then
            open(newunit=self%config_unit, file=self%config_file, status="old", action="read", iostat=exception%stat,&
                 iomsg=exception%message)
            call exception%raise()
        end if

        do while(stat .ne. IOSTAT_END)
            line = str%strip(iostream%readline(self%config_unit, stat))

            if (str%startswith(line, "[") .and. str%endswith(line, "]")) then ! New section.
                section = str%strip(line(2:len(line)-1))
            else if ((.not. str%startswith(line, ";")) .and. &
                     (.not. str%startswith(line, "#")) .and. (line .ne. "")) then ! New element.
                element = str%split(line, "=")
                if (size(element) > 1 .and. str%strip(str%join(element(2:))) .ne. "") then
                    key = section//"."//str%strip(element(1)%s)
                    content = str%join(element(2:))
                    call self%configurations%set(config_structure(key, content))
                else
                    call exception%construct("Invalid configuration ["//line//"].")
                    call exception%raise()
                end if
            end if
        end do

        if (allocated(self%config_file)) then
            close(self%config_unit)
        end if
    end subroutine configparser_parse

    subroutine configparser_set(self, key, content)
        class(configparser_class), intent(inout) :: self
        character(len=*), intent(in) :: key
        character(len=*), intent(in) :: content

        call self%configurations%set(config_structure(key, content))
    end subroutine configparser_set

    subroutine configparser_get_scalar(self, key, content, defaults, stat)
        class(configparser_class), intent(in) :: self
        character(len=*), intent(in) :: key ! Key to search.
        class(*), intent(out) :: content ! Found content.
        character(len=*), intent(in), optional :: defaults ! Default values.
        integer, intent(out), optional :: stat
        type(exception_class) :: exception
        type(string) :: charvalue
        logical :: success

        ! Find content.
        success = self%configurations%pick(config_structure(key), charvalue)

        ! Get content.
        if (.not. allocated(charvalue%s)) then
            if (.not. present(defaults)) then
                call exception%construct("Required value not found for config ["//str%strip(key)//"].")
            else
                charvalue%s = defaults
            end if
        end if

        if (allocated(charvalue%s)) then
            call exception%construct("Data type error occurred for value of config ["//str%strip(key)//"].")
            select type(content)
            type is(integer(kind=INT32))
                read(charvalue%s, *, iostat=exception%stat) content
            type is(integer(kind=INT64))
                read(charvalue%s, *, iostat=exception%stat) content
            type is(real(kind=REAL32))
                content = calculator%eval(charvalue%s, exception%stat)
            type is(real(kind=REAL64))
                content = calculator%eval(charvalue%s, exception%stat)
            type is(logical)
                read(charvalue%s, *, iostat=exception%stat) content
            type is(complex)
                read(charvalue%s, *, iostat=exception%stat) content
            type is(string)
                exception%stat = 0
                content = charvalue
            end select
        end if

        ! Error handling.
        if (present(stat)) then
            stat = exception%stat
        else
            call exception%raise()
        end if
    end subroutine configparser_get_scalar

    subroutine configparser_get_vector(self, key, content, defaults, stat)
        class(configparser_class), intent(in) :: self
        character(len=*), intent(in) :: key ! Key to search.
        class(*), dimension(:), intent(out) :: content ! Found content.
        character(len=*), intent(in), optional :: defaults ! Default values.
        integer, intent(out), optional :: stat
        type(exception_class) :: exception
        type(string), allocatable, dimension(:) :: vector
        type(string) :: charvalue
        logical :: success
        integer :: i

        ! Find content.
        success = self%configurations%pick(config_structure(key), charvalue)

        ! Get content.
        if (.not. allocated(charvalue%s)) then
            if (.not. present(defaults)) then
                call exception%construct("Required value not found for config ["//str%strip(key)//"].")
            else
                charvalue%s = defaults
            end if
        end if

        if (allocated(charvalue%s)) then
            call exception%construct("Data type error occurred for value of config ["//str%strip(key)//"].")
            vector = str%split(charvalue%s, ",")
            select type(content)
            type is(integer(kind=INT32))
                read(charvalue%s, *, iostat=exception%stat) content
            type is(integer(kind=INT64))
                read(charvalue%s, *, iostat=exception%stat) content
            type is(real(kind=REAL32))
                do i = 1, min(size(vector), size(content))
                    content(i) = calculator%eval(vector(i)%s, stat=exception%stat)
                end do
            type is(real(kind=REAL64))
                do i = 1, min(size(vector), size(content))
                    content(i) = calculator%eval(vector(i)%s, stat=exception%stat)
                end do
            type is(logical)
                read(charvalue%s, *, iostat=exception%stat) content
            type is(complex)
                read(charvalue%s, *, iostat=exception%stat) content
            type is(string)
                exception%stat = 0
                content = vector
            end select
        end if

        ! Error handling.
        if (present(stat)) then
            stat = exception%stat
        else
            call exception%raise()
        end if
    end subroutine configparser_get_vector
end module class_configparser

! vim: set ft=fortran ff=unix tw=132:
