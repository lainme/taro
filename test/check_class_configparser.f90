! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
subroutine check_class_configparser()
    use iso_fortran_env, only : REAL64
    use class_assertion
    use class_configparser
    use class_string
    implicit none

    type(assertion_class) :: assertion
    type(configparser_class) :: configparser
    type(string) :: chartype
    character(len=:), allocatable :: message
    real(kind=REAL64) :: calc_vector(2)
    real(kind=REAL64) :: calc_scalar
    integer :: inttype
    integer :: funit
    integer :: i

    message = "[main]"//new_line("a")//"area=10"
    open(newunit=funit, status="scratch")
    write(funit, *) message
    rewind(funit)

    call configparser%construct(config_unit=funit)
    call configparser%parse()
    call configparser%set("main.node", "20")
    call configparser%set("main.calc_scalar", "4.0/2.0")
    call configparser%set("main.calc_vector", "3.0/2.0, 9.0/3.0")
    call configparser%get("main.area", chartype)
    call configparser%get("main.node", inttype)
    call configparser%get("main.calc_scalar", calc_scalar)
    call configparser%get("main.calc_vector", calc_vector)

    call assertion%construct("configparser_class :: get")
    call assertion%assert(chartype .eq. "10")
    call assertion%assert(inttype .eq. 20)
    call assertion%assert(abs(calc_scalar-2.0) <= 1E-6)
    call assertion%assert(all(abs(calc_vector-[1.5,3.0]) <= 1E-6))
    call assertion%output()

    close(funit)
end subroutine check_class_configparser

! vim: set ft=fortran ff=unix tw=132:
