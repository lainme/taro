! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
subroutine check_class_string()
    use class_assertion
    use class_string
    implicit none

    type(assertion_class) :: assertion

    call assertion%construct("string_class :: upper")
    call assertion%assert(str%upper("test") .eq. "TEST")
    call assertion%output()

    call assertion%construct("string_class :: split")
    call assertion%assert(all(str%split("arg1 arg2") .eq. [string("arg1"), string("arg2")]))
    call assertion%assert(all(str%split("arg1  arg2") .eq. [string("arg1"), string("arg2")]))
    call assertion%assert(all(str%split("arg1/arg2", "/") .eq. [string("arg1"), string("arg2")]))
    call assertion%assert(all(str%split("/arg1/arg2", "/") .eq. [string(""), string("arg1"), string("arg2")]))
    call assertion%assert(all(str%split("arg1//arg2", "/") .eq. [string("arg1"), string(""), string("arg2")]))
    call assertion%output()

    call assertion%construct("string_class :: strip")
    call assertion%assert(str%strip(" test ") .eq. "test")
    call assertion%assert(str%strip("12345we2st67890","1234567890") .eq. "we2st")
    call assertion%output()

    call assertion%construct("string_class :: join")
    call assertion%assert(str%join([string("hello"), string("world")]) .eq. "helloworld")
    call assertion%output()
end subroutine check_class_string

! vim: set ft=fortran ff=unix tw=132:
