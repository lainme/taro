! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
subroutine check_class_datafmt_cgns
    use iso_fortran_env, only : REAL64
    use class_string
    use class_assertion
    use class_indexmap
    use class_datafmt_cgns
    use cgns, only : STRUCTURED, UNSTRUCTURED, HEXA_8, QUAD_4
    implicit none

    call check_mesh_simple()
    call check_mesh_structured()
    call check_mesh_unstructured()
contains
    subroutine check_mesh_simple()
        type(assertion_class) :: assertion
        type(cgns_reader_class) :: cgns
        real(kind=REAL64) :: lengths(2)
        integer :: num(2)
        integer :: idx(2)
        integer :: i

        lengths = [1.0,1.0]
        num = [2,2]

        call cgns%construct(lengths, num+1)
        call assertion%construct("cgns_reader_class :: construct (simple)")
        call assertion%assert(cgns%phys_dimension .eq. 2)
        call assertion%assert(cgns%cell_dimension .eq. 2)
        call assertion%output()

        call cgns%parse()
        call assertion%construct("cgns_reader_class :: parse (simple)")
        call assertion%assert(cgns%nzones .eq. 1)
        call assertion%assert(cgns%zones(1)%zonetype .eq. STRUCTURED)
        call assertion%assert(size(cgns%zones(1)%coords) .eq. product(num+1)*2)
        call assertion%assert(size(cgns%zones(1)%bocos) .eq. 4)

        do i = 1, product(num+1)
            idx = indexmap%decode(num+1, i)
            call assertion%assert(abs(cgns%zones(1)%coords(i,1)-(idx(1)-1.0)/num(1)) .le. 1E-6)
            call assertion%assert(abs(cgns%zones(1)%coords(i,2)-(idx(2)-1.0)/num(2)) .le. 1E-6)
        end do

        do i = 1, 4
        associate(boco => cgns%zones(1)%bocos(i))
            select case(str%upper(boco%identifier))
            case("WEST")
                call assertion%assert(all(boco%scope .eq. reshape([1,1,1,num(2)+1], [2,2])))
            case("EAST")
                call assertion%assert(all(boco%scope .eq. reshape([num(1)+1,1,num(1)+1,num(2)+1], [2,2])))
            case("SOUTH")
                call assertion%assert(all(boco%scope .eq. reshape([1,1,num(1)+1,1], [2,2])))
            case("NORTH")
                call assertion%assert(all(boco%scope .eq. reshape([1,num(2)+1,num(1)+1,num(2)+1], [2,2])))
            end select
        end associate
        end do

        call assertion%output()
    end subroutine check_mesh_simple

    subroutine check_mesh_structured()
        type(assertion_class) :: assertion
        type(cgns_reader_class) :: cgns
        integer :: num(3)
        integer :: idx(3)
        integer :: i, k

        call cgns%construct("../test/resource/mesh_structured_3D.cgns")
        call assertion%construct("cgns_reader_class :: construct (structured)")
        call assertion%assert(cgns%phys_dimension .eq. 3)
        call assertion%assert(cgns%cell_dimension .eq. 3)
        call assertion%output()

        call cgns%parse()
        call assertion%construct("cgns_reader_class :: parse (structured)")
        call assertion%assert(cgns%nzones .eq. 2)

        num = [3,2,1]

        do k = 1, 2
            call assertion%assert(cgns%zones(k)%zonetype .eq. STRUCTURED)
            call assertion%assert(size(cgns%zones(k)%coords) .eq. product(num+1)*3)
            call assertion%assert(size(cgns%zones(k)%bocos) .eq. 5)
            call assertion%assert(size(cgns%zones(k)%connections) .eq. 1)

            do i = 1, product(num+1)
                idx = indexmap%decode(num+1, i)
                call assertion%assert(abs(cgns%zones(k)%coords(i,1)-(idx(1)-1.0)/num(1)) .le. 1E-6)
                call assertion%assert(abs(cgns%zones(k)%coords(i,2)-(idx(2)-1.0)/num(2)) .le. 1E-6)
            end do

            do i = 1, 5
            associate(boco => cgns%zones(k)%bocos(i))
                select case(str%upper(boco%identifier))
                case("WEST")
                    call assertion%assert(all(boco%scope .eq. reshape([1, 1, 1, 1, num(2)+1, num(3)+1], [3,2])))
                case("EAST")
                    call assertion%assert(all(boco%scope .eq. reshape([num(1)+1, 1, 1, num(1)+1, num(2)+1, num(3)+1], [3,2])))
                case("SOUTH")
                    call assertion%assert(all(boco%scope .eq. reshape([1, 1, 1, num(1)+1, 1, num(3)+1], [3,2])))
                case("NORTH")
                    call assertion%assert(all(boco%scope .eq. reshape([1, num(2)+1, 1, num(1)+1, num(2)+1, num(3)+1], [3,2])))
                case("BELOW")
                    call assertion%assert(all(boco%scope .eq. reshape([1, 1, 1, num(1)+1, num(2)+1, 1], [3,2])))
                case("ABOVE")
                    call assertion%assert(all(boco%scope .eq. reshape([1, 1, num(3)+1, num(1)+1, num(2)+1, num(3)+1], [3,2])))
                end select
            end associate
            end do
        end do

        associate(connection => cgns%zones(1)%connections(1))
            call assertion%assert(connection%donorzone .eq. 2)
            call assertion%assert(all(connection%localscope .eq. reshape([1, 1, 1, num(1)+1, num(2)+1, 1], [3,2])))
            call assertion%assert(all(connection%donorscope .eq. reshape([1, 1, num(3)+1, num(1)+1, num(2)+1, num(3)+1], [3,2])))
        end associate

        associate(connection => cgns%zones(2)%connections(1))
            call assertion%assert(connection%donorzone .eq. 1)
            call assertion%assert(all(connection%localscope .eq. reshape([1, 1, num(3)+1, num(1)+1, num(2)+1, num(3)+1], [3,2])))
            call assertion%assert(all(connection%donorscope .eq. reshape([1, 1, 1, num(1)+1, num(2)+1, 1], [3,2])))
        end associate

        call assertion%assert(size(cgns%zones(1)%collections) .eq. 1)
        call assertion%assert(cgns%zones(1)%collections(1)%donorzone .eq. 2)
        call assertion%assert(cgns%zones(1)%collections(1)%localzone .eq. 1)
        call assertion%assert(cgns%zones(1)%collections(1)%donorproc .eq. 1)
        call assertion%assert(all(cgns%zones(1)%collections(1)%connlist .eq. [1]))

        call assertion%assert(size(cgns%zones(2)%collections) .eq. 1)
        call assertion%assert(cgns%zones(2)%collections(1)%donorzone .eq. 1)
        call assertion%assert(cgns%zones(2)%collections(1)%localzone .eq. 2)
        call assertion%assert(cgns%zones(2)%collections(1)%donorproc .eq. 1)
        call assertion%assert(all(cgns%zones(2)%collections(1)%connlist .eq. [1]))

        call assertion%output()
    end subroutine check_mesh_structured

    subroutine check_mesh_unstructured()
        type(assertion_class) :: assertion
        type(cgns_reader_class) :: cgns
        integer :: num(3)
        integer :: idx(3)
        integer :: i

        call cgns%construct("../test/resource/mesh_unstructured_3D.cgns")
        call assertion%construct("cgns_reader_class :: construct (unstructured)")
        call assertion%assert(cgns%phys_dimension .eq. 3)
        call assertion%assert(cgns%cell_dimension .eq. 3)
        call assertion%output()

        call cgns%parse()
        call assertion%construct("cgns_reader_class :: parse (unstructured)")
        call assertion%assert(cgns%nzones .eq. 1)

        num = [3,2,1]

        call assertion%assert(cgns%zones(1)%zonetype .eq. UNSTRUCTURED)
        call assertion%assert(size(cgns%zones(1)%coords) .eq. product(num+1)*3)
        call assertion%assert(size(cgns%zones(1)%sections) .eq. 7)
        call assertion%assert(size(cgns%zones(1)%bocos) .eq. 6)

        do i = 1, product(num+1)
            idx = indexmap%decode(num+1, i)
            call assertion%assert(abs(cgns%zones(1)%coords(i,1)-(idx(1)-1.0)/num(1)) .le. 1E-6)
            call assertion%assert(abs(cgns%zones(1)%coords(i,2)-(idx(2)-1.0)/num(2)) .le. 1E-6)
            call assertion%assert(abs(cgns%zones(1)%coords(i,3)-(idx(3)-1.0)/num(3)) .le. 1E-6)
        end do

        associate(section => cgns%zones(1)%sections(1))
            call assertion%assert(section%element_type .eq. HEXA_8)
            call assertion%assert(all(section%scope .eq. [1,6]))
            call assertion%assert(all(section%elements(:,1) .eq. [1,2,6,5,13,14,18,17]))
            call assertion%assert(all(section%elements(:,2) .eq. [2,3,7,6,14,15,19,18]))
            call assertion%assert(all(section%elements(:,3) .eq. [3,4,8,7,15,16,20,19]))
            call assertion%assert(all(section%elements(:,4) .eq. [5,6,10,9,17,18,22,21]))
            call assertion%assert(all(section%elements(:,5) .eq. [6,7,11,10,18,19,23,22]))
            call assertion%assert(all(section%elements(:,6) .eq. [7,8,12,11,19,20,24,23]))
        end associate

        associate(section => cgns%zones(1)%sections(2))
            call assertion%assert(section%element_type .eq. QUAD_4)
            call assertion%assert(all(section%scope .eq. [7,12]))
            call assertion%assert(all(section%elements(:,1) .eq. [13,14,18,17]))
            call assertion%assert(all(section%elements(:,2) .eq. [14,15,19,18]))
            call assertion%assert(all(section%elements(:,3) .eq. [15,16,20,19]))
            call assertion%assert(all(section%elements(:,4) .eq. [17,18,22,21]))
            call assertion%assert(all(section%elements(:,5) .eq. [18,19,23,22]))
            call assertion%assert(all(section%elements(:,6) .eq. [19,20,24,23]))
        end associate

        associate(section => cgns%zones(1)%sections(3))
            call assertion%assert(section%element_type .eq. QUAD_4)
            call assertion%assert(all(section%scope .eq. [13,18]))
            call assertion%assert(all(section%elements(:,1) .eq. [1,2,6,5]))
            call assertion%assert(all(section%elements(:,2) .eq. [2,3,7,6]))
            call assertion%assert(all(section%elements(:,3) .eq. [3,4,8,7]))
            call assertion%assert(all(section%elements(:,4) .eq. [5,6,10,9]))
            call assertion%assert(all(section%elements(:,5) .eq. [6,7,11,10]))
            call assertion%assert(all(section%elements(:,6) .eq. [7,8,12,11]))
        end associate

        associate(section => cgns%zones(1)%sections(4))
            call assertion%assert(section%element_type .eq. QUAD_4)
            call assertion%assert(all(section%scope .eq. [19,20]))
            call assertion%assert(all(section%elements(:,1) .eq. [4,8,20,16]))
            call assertion%assert(all(section%elements(:,2) .eq. [8,12,24,20]))
        end associate

        associate(section => cgns%zones(1)%sections(5))
            call assertion%assert(section%element_type .eq. QUAD_4)
            call assertion%assert(all(section%scope .eq. [21,23]))
            call assertion%assert(all(section%elements(:,1) .eq. [12,11,23,24]))
            call assertion%assert(all(section%elements(:,2) .eq. [11,10,22,23]))
            call assertion%assert(all(section%elements(:,3) .eq. [10,9,21,22]))
        end associate

        associate(section => cgns%zones(1)%sections(6))
            call assertion%assert(section%element_type .eq. QUAD_4)
            call assertion%assert(all(section%scope .eq. [24,26]))
            call assertion%assert(all(section%elements(:,1) .eq. [1,2,14,13]))
            call assertion%assert(all(section%elements(:,2) .eq. [2,3,15,14]))
            call assertion%assert(all(section%elements(:,3) .eq. [3,4,16,15]))
        end associate

        associate(section => cgns%zones(1)%sections(7))
            call assertion%assert(section%element_type .eq. QUAD_4)
            call assertion%assert(all(section%scope .eq. [27,28]))
            call assertion%assert(all(section%elements(:,1) .eq. [9,5,17,21]))
            call assertion%assert(all(section%elements(:,2) .eq. [5,1,13,17]))
        end associate

        do i = 1, 6
        associate(boco => cgns%zones(1)%bocos(i))
            select case(str%upper(boco%identifier))
            case("WEST")
                call assertion%assert(all(boco%scope .eq. reshape([27,28], [1,2])))
            case("EAST")
                call assertion%assert(all(boco%scope .eq. reshape([19,20], [1,2])))
            case("SOUTH")
                call assertion%assert(all(boco%scope .eq. reshape([24,26], [1,2])))
            case("NORTH")
                call assertion%assert(all(boco%scope .eq. reshape([21,23], [1,2])))
            case("BELOW")
                call assertion%assert(all(boco%scope .eq. reshape([13,18], [1,2])))
            case("ABOVE")
                call assertion%assert(all(boco%scope .eq. reshape([7,12], [1,2])))
            end select
        end associate
        end do

        call assertion%output()
    end subroutine check_mesh_unstructured
end subroutine check_class_datafmt_cgns

! vim: set ft=fortran ff=unix tw=132:
