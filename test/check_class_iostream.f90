! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
subroutine check_class_iostream()
    use iso_fortran_env, only : IOSTAT_END
    use class_assertion
    use class_string
    use class_iostream
    implicit none

    type(assertion_class) :: assertion
    character(len=:), allocatable :: message
    integer :: funit
    integer :: stat

    message = "[main]"//new_line("a")//"area=10"
    open(newunit=funit, status="scratch")
    write(funit, *) message
    rewind(funit)

    call assertion%construct("iostream_class :: readline")
    call assertion%assert(str%strip(iostream%readline(funit, stat)) .eq. "[main]")
    call assertion%assert(str%strip(iostream%readline(funit, stat)) .eq. "area=10")
    call assertion%output()
end subroutine check_class_iostream

! vim: set ft=fortran ff=unix tw=132:
