! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
! Description:
!
! Main program for unit test.
!
program check
    use class_logging
    implicit none

    call logger%construct(stdout_level=SUPPRESS)

    call check_class_string()
    call check_class_system()
    call check_class_iostream()
    call check_class_quadrature()
    call check_class_hashmap()
    call check_class_argparser()
    call check_class_configparser()
    call check_class_indexmap()
    call check_class_geometry()
    call check_class_datafmt_cgns()
    call check_class_mesh_structured()
    call check_class_calculator()
end program check

! vim: set ft=fortran ff=unix tw=132:
