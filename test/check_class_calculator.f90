! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2018 lainme <lainme993@gmail.com>
!
subroutine check_class_calculator
    use class_assertion
    use class_calculator
    implicit none

    type(assertion_class) :: assertion

    call assertion%construct("calculator_class :: eval")
    call assertion%assert(abs(calculator%eval("-2+3*4/2+2^3")-(-2.0+3.0*4.0/2.0+2**3)) <= 1E-6)
    call assertion%assert(abs(calculator%eval("sqrt(4)+sin(pi/2)")-(sqrt(4.0)+sin(4.0*atan(1.0)/2.0))) <= 1E-6)
    call assertion%output()
end subroutine check_class_calculator

! vim: set ft=fortran ff=unix tw=132:
