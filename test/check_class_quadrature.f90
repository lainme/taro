! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2018 lainme <lainme993@gmail.com>
!
subroutine check_class_quadrature
    use iso_fortran_env, only : REAL64
    use class_assertion
    use class_quadrature
    implicit none

    type(assertion_class) :: assertion
    integer :: npoints
    real(kind=REAL64), allocatable, dimension(:) :: abscissas, weight

    npoints = 9
    call assertion%construct("quadrature_class :: newton_cotes_boole")
    call quadrature%newton_cotes_boole(npoints, [-4.0_REAL64, 4.0_REAL64], abscissas, weight)
    call assertion%assert(all(abs(abscissas-[-4.0,-3.0,-2.0,-1.0,0.0,1.0,2.0,3.0,4.0]) .le. 1E-6))
    call assertion%assert(all(abs(weight-[14,64,24,64,28,64,24,64,14]/45.0) .le. 1E-6))
    call assertion%output()

    npoints = 2
    call assertion%construct("quadrature_class :: gaussian_maxwell")
    call quadrature%gaussian_maxwell(npoints, abscissas, weight)
    call assertion%assert(all(abs(abscissas-[0.30019393,1.252421045]) .le. 1E-6))
    call assertion%assert(all(abs(weight-[0.640529179,0.245697745]) .le. 1E-6))
    call assertion%output()
end subroutine check_class_quadrature

! vim: set ft=fortran ff=unix tw=132:
