! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
subroutine check_class_geometry
    use iso_fortran_env, only : REAL64
    use class_assertion
    use class_geometry
    implicit none

    type(assertion_class) :: assertion
    real(kind=REAL64), allocatable, dimension(:,:) :: directions
    real(kind=REAL64), allocatable, dimension(:,:) :: nodes
    real(kind=REAL64), allocatable, dimension(:) :: centriod
    real(kind=REAL64) :: length
    real(kind=REAL64) :: area

    ! Line in 1D.
    nodes = reshape([2,3], [1,2])
    centriod = [0]
    call geometry%compute_line(nodes, centriod, length)
    call assertion%construct("geometry_class :: compute_line (1D)")
    call assertion%assert(abs(centriod(1)-2.5) .lt. 1E-6)
    call assertion%assert(abs(length-1.0) .lt. 1E-6)
    call assertion%output()

    ! Line in 2D.
    nodes = reshape([1,1,2,2], [2,2])
    centriod = [0,0]
    directions = reshape([0,0,0,0], [2,2])
    call geometry%compute_line(nodes, centriod, length, directions)
    call assertion%construct("geometry_class :: compute_line (2D)")
    call assertion%assert(all(abs(centriod-[1.5,1.5]) .lt. 1E-6))
    call assertion%assert(abs(length-sqrt(2.0)) .lt. 1E-6)
    call assertion%assert(all(abs(directions-1.0/reshape([length,-length,length,length],[2,2])) .lt. 1E-6))
    call assertion%output()

    ! Triangle in 2D.
    nodes = reshape([1,1,2,1,2,2], [2,3])
    centriod = [0,0]
    call geometry%compute_trig(nodes, centriod, area)
    call assertion%construct("geometry_class :: compute_trig (2D)")
    call assertion%assert(all(abs(centriod-[5.0/3.0,4.0/3.0]) .lt. 1E-6))
    call assertion%assert(abs(area-0.5) .lt. 1E-6)
    call assertion%output()

    ! Triangle in 3D.
    nodes = reshape([1,1,1,2,1,1,2,2,1], [3,3])
    centriod = [0,0,0]
    directions = reshape([0,0,0,0,0,0,0,0,0],[3,3])
    call geometry%compute_trig(nodes, centriod, area, directions)
    call assertion%construct("geometry_class :: compute_trig (3D)")
    call assertion%assert(all(abs(centriod-[5.0/3.0,4.0/3.0,1.0]) .lt. 1E-6))
    call assertion%assert(abs(area-0.5) .lt. 1E-6)
    call assertion%assert(all(abs(directions(:,1)-[0,0,1]) .lt. 1E-6))
    call assertion%assert(all(abs(directions(:,2)-[1,0,0]) .lt. 1E-6))
    call assertion%assert(all(abs(directions(:,3)-[0,1,0]) .lt. 1E-6))
    call assertion%output()

    ! Quadrature in 2D.
    nodes = reshape([0,0,1,0,1,1,0,1], [2,4])
    centriod = [0,0]
    call geometry%compute_quad(nodes, centriod, area)
    call assertion%construct("geometry_class :: compute_quad (2D)")
    call assertion%assert(all(abs(centriod-[0.5,0.5]) .lt. 1E-6))
    call assertion%assert(abs(area-1.0) .lt. 1E-6)
    call assertion%output()

    ! Quadrature in 3D.
    nodes = reshape([0,0,0,1,0,0,1,1,0,0,1,0], [3,4])
    centriod = [0,0,0]
    directions = reshape([0,0,0,0,0,0,0,0,0],[3,3])
    call geometry%compute_quad(nodes, centriod, area, directions)
    call assertion%construct("geometry_class :: compute_quad (3D)")
    call assertion%assert(all(abs(centriod-[0.5,0.5,0.0]) .lt. 1E-6))
    call assertion%assert(abs(area-1.0) .lt. 1E-6)
    call assertion%assert(all(abs(directions(:,1)-[0,0,1]) .lt. 1E-6))
    call assertion%assert(all(abs(directions(:,2)-[1,0,0]) .lt. 1E-6))
    call assertion%assert(all(abs(directions(:,3)-[0,1,0]) .lt. 1E-6))
    call assertion%output()
end subroutine check_class_geometry

! vim: set ft=fortran ff=unix tw=132:
