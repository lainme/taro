! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
subroutine check_class_mesh_structured
    use iso_fortran_env, only : REAL64
    use class_assertion
    use class_string
    use class_indexmap
    use class_mesh_structured
    use class_configparser
    implicit none

    type(assertion_class) :: assertion
    type(configparser_class) :: configparser
    type(mesh_structured_class) :: mesh
    real(kind=REAL64) :: length(2)
    real(kind=REAL64) :: del(2)
    integer :: nxfaces, nyfaces
    integer :: nxhalos, nyhalos
    integer :: nghosts
    integer :: num(2)
    integer :: i

    num = [2,2]
    length = [0.5, 1.0]
    del = length/num
    nghosts = 2
    nxfaces = (num(1)+1)*num(2)
    nyfaces = num(1)*(num(2)+1)
    nxhalos = 2*nghosts*num(2)
    nyhalos = 2*nghosts*num(1)

    ! Mesh from CGNS.
    call configparser%construct()
    call configparser%set("cgns_reader.meshfile", "../test/resource/mesh_structured_2D.cgns")
    call mesh%construct(configparser)
    call assertion%construct("mesh_structured_class :: construct (cgns)")
    call assertion%assert(mesh%ndimensions .eq. 2)
    call assertion%output()

    call mesh%parse(nghosts=nghosts)
    call assertion%construct("mesh_structured_class :: parse (cgns)")
    call assertion%assert(mesh%nzones .eq. 2)
    do i = 1, 2
        call check_zone(mesh%zones(i), i, 3, 1)
    end do
    call assertion%output()

    ! Mesh from simple inputs.
    call configparser%construct()
    call configparser%set("cgns_reader.celldims", "2")
    call configparser%set("cgns_reader.geometry", "0.5, 1.0")
    call configparser%set("cgns_reader.numcells", "2, 2")
    call mesh%construct(configparser)
    call assertion%construct("mesh_structured_class :: construct (simple)")
    call assertion%assert(mesh%ndimensions .eq. 2)
    call assertion%output()

    call mesh%parse(nghosts=nghosts)
    call assertion%construct("mesh_structured_class :: parse (simple)")
    call assertion%assert(mesh%nzones .eq. 1)
    call check_zone(mesh%zones(1), 1, 4, 0)
    call assertion%output()
contains
    subroutine check_zone(zone, index_zone, nbocos, nconns)
        type(mesh_zone_structure), intent(in) :: zone
        integer, intent(in) :: index_zone
        integer, intent(in) :: nbocos
        integer, intent(in) :: nconns
        real(kind=REAL64) :: shift(2)
        integer :: idx(2)
        integer :: i, j

        shift = [0.5*(index_zone-1), 0.0]

        call assertion%assert(all(zone%zonesize .eq. num))
        call assertion%assert(size(zone%nodes) .eq. product(num+1))
        call assertion%assert(size(zone%faces) .eq. nxfaces+nyfaces)
        call assertion%assert(size(zone%cells) .eq. product(num)+2*nghosts*(num(1)+num(2)))
        call assertion%assert(size(zone%halos) .eq. 2*nghosts*(num(1)+num(2)))
        call assertion%assert(size(zone%bocos) .eq. nbocos)
        if (allocated(zone%connections)) then
            call assertion%assert(size(zone%connections) .eq. nconns)
        end if

        do i = 1, size(zone%nodes)
            idx = indexmap%decode(num+1, i)
            call assertion%assert(all(abs(zone%nodes(i)%centriod-shift-(idx-1.0)*del) .le. 1E-6))
        end do

        do i = 1, zone%ncells
            idx = indexmap%decode(num, i)
            call assertion%assert(all(abs(zone%cells(i)%centriod-shift-(idx-0.5)*del) .le. 1E-6))
            call assertion%assert(abs(zone%cells(i)%volume-product(del)) .le. 1E-6)
            call assertion%assert(all(abs(zone%cells(i)%distances-0.5*del) .le. 1E-6))
        end do

        do i = 1, nxfaces
            idx = indexmap%decode([num(1)+1,num(2)], i)
            call assertion%assert(abs(zone%faces(i)%centriod(1)-shift(1)-(idx(1)-1.0)*del(1)) .le. 1E-6)
            call assertion%assert(abs(zone%faces(i)%centriod(2)-shift(2)-(idx(2)-0.5)*del(2)) .le. 1E-6)
            call assertion%assert(abs(zone%faces(i)%area-del(2)) .le. 1E-6)
            call assertion%assert(all(abs(zone%faces(i)%transform-reshape([1.0,0.0,0.0,1.0],[2,2])) .le. 1E-6))
        end do

        do i = nxfaces+1, nxfaces+nyfaces
            idx = indexmap%decode([num(1),num(2)+1], i-nxfaces)
            call assertion%assert(abs(zone%faces(i)%centriod(1)-shift(1)-(idx(1)-0.5)*del(1)) .le. 1E-6)
            call assertion%assert(abs(zone%faces(i)%centriod(2)-shift(2)-(idx(2)-1.0)*del(2)) .le. 1E-6)
            call assertion%assert(abs(zone%faces(i)%area-del(1)) .le. 1E-6)
            call assertion%assert(all(abs(zone%faces(i)%transform-reshape([0.0,1.0,-1.0,0.0],[2,2])) .le. 1E-6))
        end do

        do i = 1, nxhalos
            idx = indexmap%decode([2*nghosts, num(2)], i)
            if (idx(1) > nghosts) then
                idx(1) = num(1)+idx(1)-nghosts
            else
                idx(1) = idx(1)-nghosts
            end if
            call assertion%assert(all(abs(zone%halos(i)%centriod-shift-(idx-0.5)*del) .le. 1E-6))
            call assertion%assert(abs(zone%halos(i)%volume-product(del)) .le. 1E-6)
            call assertion%assert(all(abs(zone%halos(i)%distances-0.5*del) .le. 1E-6))
        end do

        do i = nxhalos+1, nxhalos+nyhalos
            idx = indexmap%decode([num(1), 2*nghosts], i-nxhalos)
            if (idx(2) > nghosts) then
                idx(2) = num(2)+idx(2)-nghosts
            else
                idx(2) = idx(2)-nghosts
            end if
            call assertion%assert(all(abs(zone%halos(i)%centriod-shift-(idx-0.5)*del) .le. 1E-6))
            call assertion%assert(abs(zone%halos(i)%volume-product(del)) .le. 1E-6)
            call assertion%assert(all(abs(zone%halos(i)%distances-0.5*del) .le. 1E-6))
        end do

        do i = 1, size(zone%bocos)
            select case(str%upper(zone%bocos(i)%identifier))
            case("west")
                call assertion%assert(all(zone%bocos(i)%ghostelem .eq. [2,1,6,5]))
                call assertion%assert(all(zone%bocos(i)%boundelem .eq. [1,4]))
                call assertion%assert(all(zone%bocos(i)%innerelem .eq. [1,2,3,4]))
            case("east")
                call assertion%assert(all(zone%bocos(i)%ghostelem .eq. [3,4,7,8]))
                call assertion%assert(all(zone%bocos(i)%boundelem .eq. [3,6]))
                call assertion%assert(all(zone%bocos(i)%innerelem .eq. [2,1,4,3]))
            case("south")
                call assertion%assert(all(zone%bocos(i)%ghostelem .eq. [11,9,12,10]))
                call assertion%assert(all(zone%bocos(i)%boundelem .eq. [7,8]))
                call assertion%assert(all(zone%bocos(i)%innerelem .eq. [1,3,2,4]))
            case("north")
                call assertion%assert(all(zone%bocos(i)%ghostelem .eq. [13,15,14,16]))
                call assertion%assert(all(zone%bocos(i)%boundelem .eq. [11,12]))
                call assertion%assert(all(zone%bocos(i)%innerelem .eq. [3,1,4,2]))
            end select
        end do

        if (nconns .ne. 0) then
            if (index_zone == 1) then
                call assertion%assert(zone%collections(1)%donorzone .eq. 2)
                call assertion%assert(all(zone%connections(1)%ghostelem .eq. [3,4,7,8]))
                call assertion%assert(all(zone%connections(1)%donorelem .eq. [1,2,3,4]))
                call assertion%assert(all(zone%connections(1)%boundelem .eq. [3,6]))
                call assertion%assert(all(zone%connections(1)%innerelem .eq. [2,1,4,3]))
            else
                call assertion%assert(zone%collections(1)%donorzone .eq. 1)
                call assertion%assert(all(zone%connections(1)%ghostelem .eq. [2,1,6,5]))
                call assertion%assert(all(zone%connections(1)%donorelem .eq. [2,1,4,3]))
                call assertion%assert(all(zone%connections(1)%boundelem .eq. [1,4]))
                call assertion%assert(all(zone%connections(1)%innerelem .eq. [1,2,3,4]))
            end if
        end if
    end subroutine check_zone
end subroutine check_class_mesh_structured

! vim: set ft=fortran ff=unix tw=132:
