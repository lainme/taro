! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
module mod_check_class_hashmap
    use class_hashmap
    implicit none

    type, extends(hashmap_pair) :: pair_structure
        integer :: key
        integer :: content
    contains
        procedure :: has_samekey
        procedure :: copy
        procedure :: hash
    end type pair_structure

    interface pair_structure
        procedure :: pair_object
    end interface
contains
    pure function pair_object(key, content) result(pair)
        integer, intent(in) :: key
        integer, intent(in), optional :: content
        type(pair_structure) :: pair

        pair%key = key
        if (present(content)) then
            pair%content = content
        end if
    end function pair_object

    elemental function has_samekey(self, pair)
        class(pair_structure), intent(in) :: self
        class(hashmap_pair), intent(in) :: pair
        logical :: has_samekey

        has_samekey = .false.

        select type(pair)
        class is(pair_structure)
            has_samekey = self%key .eq. pair%key
        end select
    end function has_samekey

    subroutine copy(self, object)
        class(pair_structure), intent(in) :: self
        class(*), intent(out) :: object

        select type(object)
        type is(integer)
            object = self%content
        class is(pair_structure)
            object%key = self%key
            object%content = self%content
        end select
    end subroutine copy

    elemental function hash(self)
        class(pair_structure), intent(in) :: self
        integer :: hash

        hash = self%key
    end function hash
end module mod_check_class_hashmap

subroutine check_class_hashmap()
    use class_assertion
    use mod_check_class_hashmap
    implicit none

    type(assertion_class) :: assertion
    type(hashmap_class) :: hashmap
    class(hashmap_pair), pointer :: pair
    integer :: content
    logical :: success

    call hashmap%construct()
    call hashmap%set(pair_structure(1,10))
    call hashmap%set(pair_structure(5,50))
    call hashmap%set(pair_structure(22,20))

    call assertion%construct("hashmap_class :: haskey")
    call assertion%assert(hashmap%haskey(pair_structure(5)) .eqv. .true.)
    call assertion%assert(hashmap%haskey(pair_structure(8)) .eqv. .false.)
    call assertion%output()

    call hashmap%remove(pair_structure(5))
    call assertion%construct("hashmap_class :: remove")
    call assertion%assert(hashmap%haskey(pair_structure(5)) .eqv. .false.)
    call assertion%output()

    success = hashmap%pick(pair_structure(22), content)
    call assertion%construct("hashmap_class :: pick")
    call assertion%assert(content .eq. 20)
    call assertion%output()

    success = hashmap%find(pair_structure(22), pair)
    call assertion%construct("hashmap_class :: find")
    select type(pair)
    class is(pair_structure)
        call assertion%assert(pair%content .eq. 20)
    end select
    call assertion%output()
end subroutine check_class_hashmap

! vim: set ft=fortran ff=unix tw=132:
