! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
subroutine check_class_argparser
    use class_string
    use class_assertion
    use class_argparser
    implicit none

    type(assertion_class) :: assertion
    type(argparser_class) :: argparser
    type(string) :: chartype
    integer :: inttype

    call argparser%construct(description="Unit test.", version="0.0.1")
    call argparser%add_argument("-c", meta="CHARACTER", required=.true.)
    call argparser%add_argument("--int", content="128", meta="INT")
    call argparser%parse("bin/taro -c character")
    call argparser%get("-c", chartype)
    call argparser%get("--int", inttype)

    call assertion%construct("argparser_class :: get")
    call assertion%assert(chartype%s .eq. "character")
    call assertion%assert(inttype .eq. 128)
    call assertion%output()
end subroutine check_class_argparser

! vim: set ft=fortran ff=unix tw=132:
