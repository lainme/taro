! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2016 lainme <lainme993@gmail.com>
!
subroutine check_class_system
    use class_string
    use class_assertion
    use class_system
    implicit none

    type(assertion_class) :: assertion
    type(string) :: splitext(2)

    call assertion%construct("system_class :: path%join")
    call assertion%assert(system%path%join([string("/home/lainme"), string("test.f90")]) .eq. "/home/lainme/test.f90")
    call assertion%assert(system%path%join([string("/home/lainme/"), string("test.f90")]) .eq.  "/home/lainme//test.f90")
    call assertion%output()

    call assertion%construct("system_class :: path%splitext")
    call assertion%assert(all(system%path%splitext("/home/lainme/channel.cgns") .eq. &
                              [string("/home/lainme/channel"), string(".cgns")]))
    call assertion%output()
end subroutine check_class_system

! vim: set ft=fortran ff=unix tw=132:
