! License: MIT (https://opensource.org/licenses/MIT)
! Copyright (C) 2018 lainme <lainme993@gmail.com>
!
subroutine check_class_indexmap
    use class_indexmap
    use class_assertion
    implicit none

    type(assertion_class) :: assertion
    integer :: scope(3)
    integer :: decoded(3)
    integer :: encoded

    scope = [2,3,4]
    decoded = scope-1
    encoded = decoded(1)+(decoded(2)-1)*scope(1)+(decoded(3)-1)*scope(1)*scope(2)

    call assertion%construct("indexmap_class :: encode")
    call assertion%assert(indexmap%encode(scope, decoded) .eq. encoded)
    call assertion%output()

    call assertion%construct("indexmap_class :: decode")
    call assertion%assert(all(indexmap%decode(scope, encoded) .eq. decoded))
    call assertion%output()
end subroutine check_class_indexmap

! vim: set ft=fortran ff=unix tw=132:
